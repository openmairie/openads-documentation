.. _authentification:

################
Authentification
################

.. _auth_db:

Basique (*db*)
##############

Sans configuration particulière openADS authentifie les utilisateurs via les informations présentes
dans sa base de données, et utilise un identifiant et un mot de passe.
Le type d'authentification par défaut est donc nommé *db*.


.. _auth_ldap:

Annuaire LDAP
#############

Si les utilisateurs doivent être authentifiés via un annuaire *LDAP* il faut créer un fichier de
configuration spécifique **dyn/directory.inc.php**, contenant les informations suivantes:

.. code-block:: php

   <?php
   $directory = array();
   $directory["ldap-default"] = array(
       'ldap_proto' => 'ldaps', // Le protocol de connexion
       'ldap_server' => 'monserveurldap.local', // le domaine du serveur
       'ldap_server_port' => '6636', // le port de connexion
       'ldap_admin_login' => 'openads@monserveurldap', // l'identifiant de l'utilisateur administrateur
       'ldap_admin_passwd' => '#supersecret#', // mot de passe de l'administrateur
       'ldap_proto_vers' => 3, // version du protocole
       'ldap_proto_vers_no_setup' => true, // 'true' pour utiliser la version par défaut
       'ldap_base' => 'dc=myorg,dc=ad,dc=mydep,dc=fr', // un filtre utilisateur pour l'administrateur
       'ldap_base_users' => 'dc=mcc,dc=ad,dc=culture,dc=fr',  // un filtre utilisateur de base
       'ldap_user_filter' => '(&(ObjectClass=*)(memberof:1.2.800.126547.1.4.2145:=CN=openads,OU=openads,OU=Applications,DC=myorg,DC=ad,DC=mydep,DC=fr))',
       'ldap_login_attrib' => 'samaccountname', // quel attribut des utilisateurs est utilisé pour les authentifier
       'ldap_more_attrib' => array( // plus d'infos utilisateurs
           'email' => 'mail',
           'nom' => 'displayname',
       ),
       'default_om_profil' => '1', // l'identifiant (dans openADS) du profil par défaut des utilisateurs
       "ldap_login_dn" => "userprincipalname", // si renseigné, utilise la valeur requêtée de cet attribut en tant que login
   );

Par ailleurs il faut également modifier le fichier **dyn/database.inc.php** pour que la 14ème valeur
de configuration spécifie la clé utilsée dans le tableau dans le bloc de code ci-dessus, ici
**ldap-default**, soit:

.. code-block:: php

    <?php
    $conn[1] = array(
        "openADS",
        "pgsql",
        "pgsql",
        "postgres",
        "postgres",
        "tcp",
        "localhost",
        "5432",
        "",
        "openads",
        "AAAA-MM-JJ",
        "openads",
        "",
        "ldap-default", // Ici, paramétrage pour l'annuaire LDAP
        "mail-default",
        "filestorage-default",
    );


.. _auth_sso:

OpenID Connect (SSO)
####################

Si les utilisateurs peuvent également s'authentifier via un *SSO* (*Single Sign On*), openADS
supporte le protocol *OpenID Connect*. Ce protocole vient en supplément d'un compte utilisateur
dans openADS, qui a déjà une authentification de type *db* (basique) ou *ldap*.

Pour permettre l'authentification *SSO*, il faut créer un fichier de configuration spécifique
**dyn/openid.inc.php**, contenant les informations suivantes:

.. code-block:: php

   <?php
   return array(
       'redirect_url' => 'https://mon-openads/app/index.php?module=login&login_callback=true', // l'URL openADS de redirection
       'provider_url' => 'https://application-fournisseur-sso', // l'URL de l'application tierce réalisant l'authentification
       'application_id' => 'identifiant-application', // l'identifiant de l'application openADS auprès de l'application tierce
       'application_secret' => 'supersecret', // le mot de passe de l'application openADS auprès de l'application tierce
       'user_attribute_for_login' => 'login' // l'attribut de l'utilisateur qui correspond à son login
   );

Par ailleurs il faut également modifier le fichier **dyn/config.inc.php** pour que la valeur de la
variable de configuration **enable_openid_login** vale **true**, telle que:

.. code-block:: php

   <?php
   $config = array();
   $config['enable_openid_login'] = true;
