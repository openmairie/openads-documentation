.. _compteurs:

#############
Les compteurs
#############

.. toctree::


Les compteurs permettent de compter incrémentalement toutes sorte d'opérations, ainsi que
de définir des seuils d'alerte et de quota.

Pour ce faire il faut définir un compteur via l'interface d'administration dans le menu
*Compteur & Quota*, mais aussi implémenter l'incrémentation du compteur dans le code source
d'openADS en ajoutant un appel à la fonction *compteur::increment* à l'endroit désiré.

.. Note: Il existe déjà de base dans le code source d'openADS l'appel à un compteur dont le
  *code* est **signatures** pour compter les documents signés. C'est détaillé ci-dessous.

Les différents champs de la table/classe compteur
#################################################

- **compteur**: l'identifiant technique du compteur
- **code**: un code pour pouvoir gérer facilement les compteurs, orienté métier
- **description**: la description du compteur (ce qui est compté)
- **unité**: l'unité de ce qui est compté (par exemple: nombre, clics, requêtes, etc.)
- **quantité**: la valeur actuelle du compteur (quantité de ce qui a été compté)
- **quota**: seuil à partir duquel il y aura une notion métier de quota (par exemple:
  affichage d'un message pour renouveller une souscription)
- **alerte**: seuil à partir duquel il y aura une notion métier d'avertissement (par
  exemple un message indiquant le nombre restant d'unités sera affiché à l'utilisateur)
- **date_modification**: champ permettant d'enregistrer la date de la dernière modification
  de la valeur du compteur
- **date_debut_validite**: date de début de validité du compteur
- **date_fin_validite**: date de fin de validité du compteur
- **collectivité**: indique à quelle collectivité est rattaché ce compteur


.. Note: openADS utilise un code spécifique *signatures* qui permet de compter le nombre
  de documents signés via un parapheur électronique (plugin openADS) et un widget est
  aussi fourni pour afficher l'état de ce compteur ainsi que des message d'alerte et de
  quota;


Le cas du compteur de signature, implémenté de base dans openADS
################################################################

Si un compteur avec le *code* **signatures** est défini et valide alors openADS l'utilisera
pour compter le nombre de documents signés électroniquement (via plugin parapheur électronique)
pour la collectivité à laquelle il est rattaché.

Dans ce cas un widget sera également disponible pour afficher l'état de ce compteur, ainsi que
des messages d'alerte et de quota (renouvellement de la souscription).
Pour plus d'information sur le widget, voir :ref:`ici<widget_compteur_signatures>` et pour son
paramétrage :ref:`ici<administration_widget_compteur_signatures>`.


Détails d'implémentation des compteurs
######################################


*****************************************
Rendre une classe "comptable/dénombrable"
*****************************************

Utiliser le trait *incrementable*
*********************************

Dans openADS les compteurs sont implémentés via la classe *compteur*, qui utilise un *trait*
PHP **incrementable**, c'est à dire qu'il est possible de donner la capacité à être compté
à n'importe quelle (autre) classe grâce à ce composant.

Pour ce faire, la classe qui nécessite d'être comptée doit utiliser (*use*) le *trait*, via
le code :

.. code:: php

   require_once __DIR__ . '/trait_incrementable.php';

   class XXX extends XXX_gen {
      use incrementable;


Par exemple la classe compteur commence comme ceci :

.. code:: php

   require_once "../gen/obj/compteur.class.php";

   require_once __DIR__ . '/trait_date_valid.php';
   require_once __DIR__ . '/trait_incrementable.php';

   class compteur extends compteur_gen {
      use date_valid;
      use incrementable;


Ensuite il est nécessaire de définir plusieurs aspects pour que le comptage ait lieu :

- **incrementGetProperty** (optionel): méthode qui retourne le nom du champ qui contient la valeur
  du compteur. Par défaut vaut 'quantite'.

- **incrementGetStep** (optionel): méthode qui retourne le pas de l'incrément. Par défaut vaut 1.0.

- **incrementUpdateLastMod** (optionel): méthode qui retourne un booléen qui indique, s'il vaut *true*
  qu'il faut mettre à jour la date de dernière modification.

- **incrementGetLastModProp** (optionel): méthode qui retourne le nom du champ qui contient la date
  de dernière modification.

- **incrementGetSQLCondition** (obligatoire): méthode qui retourne une chaine de caractères correspondant
  à une clause WHERE de la requête SQL servant à identifiter l'enregistrement de l'objet
  "cournat" à incrémenter par la fonction *incrementable::increment()*


.. Note: La classe compteur ne surcharge que la méthode *incrementGetSQLCondition* et utilise les
   autres méthodes définies dans le *trait*.


Appeler la fonction *increment* à l'endroit désiré
**************************************************

Tous ces éléments sont effectivements utilisés dans la fonction *incrementable::increment()*
qui va réaliser le comptage lorsqu'elle sera appellée dans le code.

Par exemple la classe *compteur* est actuellement utilisée dans le webservice de maintenance
d'openADS qui récupère les documents signés et met à jour les instructions correspondantes :

.. code:: php

   class MaintenanceManager extends MetierManager {

      ...

      public function update_parapheur_datas() {

         ...

         $compteur_signature = $this->f->findObjectByCondition(
            'compteur',
            "code = 'signatures'
            AND om_collectivite = $collectivite_id
            AND (
                  om_validite_debut IS NULL
                  OR om_validite_debut <= CURRENT_DATE
            )
            AND (
                  om_validite_fin IS NULL
                  OR om_validite_fin > CURRENT_DATE
            )",
            'compteur DESC');
         if (! empty($compteur_signature)) {
            try {
               $res = $compteur_signature->increment();
            }
            catch(Exception $e) {
               // ...
            }

