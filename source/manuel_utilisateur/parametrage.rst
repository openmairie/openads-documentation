.. _parametrage:

###########
Paramétrage
###########

Libellés
########

.. _parametrage_civilite:

=============
Les civilités
=============

(:menuselection:`Paramétrage --> Civilité`)

Édition des civilités présentes dans l'application.

.. _parametrage_arrondissement:

===================
Les arrondissements
===================

(:menuselection:`Paramétrage --> Arrondissement`)

Édition des arrondissements disponibles dans l'application.


.. _parametrage_quartier:

=============
Les quartiers
=============

(:menuselection:`Paramétrage --> Quartier`)

Édition des quartiers disponibles dans l'application.


Organisation
############


.. _parametrage_genre:

==========
Les genres
==========

(:menuselection:`Paramétrage --> Organisation --> Genre`)

Édition des genres de dossiers (ERP, URBA).

.. _parametrage_groupe:

===========
Les groupes
===========

(:menuselection:`Paramétrage --> Organisation --> Groupe`)

Édition des groupes de dossiers (ERP, ADS, Contentieux, Changement d'usage, ...).

.. _parametrage_direction:

============
La direction
============

(:menuselection:`Paramétrage --> Organisation --> Direction`)

Édition des informations concernant la direction du service ADS.

.. _parametrage_division:

=============
Les divisions
=============

(:menuselection:`Paramétrage --> Organisation --> Division`)

Éditions des différentes divisions traitant les demandes.

.. _parametrage_instructeur_qualite:

==========================
Les qualités d'instructeur
==========================

(:menuselection:`Paramétrage --> Organisation --> Qualité d'instructeur`)

Éditions des qualités d'instructeur.

.. _parametrage_instructeur:

================
Les instructeurs
================

(:menuselection:`Paramétrage --> Organisation --> Instructeur`)

Éditions des différents instructeurs traitant les demandes.

.. _parametrage_signataire_habilitation:

=================================
Les habilitations des signataires
=================================

(:menuselection:`Paramétrage --> Organisation --> Signataire Habilitation`)

Éditions des habilitations des signataires.

.. _parametrage_signataire_arrete:

===============
Les signataires
===============

(:menuselection:`Paramétrage --> Organisation --> Signataire Arrêté`)

Éditions des signataires d'arrêtés.
En cochant la case "défaut" le signataire sera préselectionné lors de l'ajout d'une instruction.

Le choix de l'habilitation du signataire se fait via le champs "signataire habilitation". Le libellé
de l'habilitation du signataire sera associé à son nom dans la liste des signataires des évènements d'instructions.

En remplissant le champs "description" la description sera visible, en plus du nom du signataire
et de son habilitation, dans la liste des signataires des évènements d'instructions.

Si un parapheur est relié à openADS, un champ nommé "Paramètre du parapheur" permet de spécifier des paramètres supplémentaires au parapheur.
Le champ doit contenir du json valide.

Exemple de contenu du champ "Paramètre du parapheur" dans le cas d'un connecteur Pastell iParapheur :

.. code-block:: json

      {"circuit" : "SIGNATURE"}

Un bouton "Relancer les demandes de signature" permet, pour le signataire courant, de relancer
(annuler puis renvoyer) toutes les demandes de signature qu'il a effectué et qui ne sont pas encore
signées. À n'utiliser que si vraiment nécessaire car cela peut avoir des effets de bords.
En l'occurence l'un des effets de ce traitement est que les demandes de signatures seront associées
à l'utilisateur administrateur qui effectue la relance, plutôt qu'à l'utilisateur intial qui
avait réalisé l'envoi en signature.


.. _parametrage_taxe_amenagement:

=========
Les taxes
=========

(:menuselection:`Paramétrage --> Organisation --> Taxes`)

Ce menu de paramétrage des taxes permet de renseigner les valeurs forfaitaires, le taux communale, le taux départementale, le taux régionale et les taux des exonérations facultatives de la taxe d'aménagement. Il permet également de définir le taux de la redevance d'archéologie préventive.
Il ne peut y avoir qu'un seul paramétrage de taxe par collectivité. Sans ce paramétrage il n'est pas possible d'estimer les montants des taxes sur un dossier d'instruction. Les collectivités de niveau 1 récupère le paramètrage des taxes associé à la collectivité de niveau 2 si celui-ci existe.

.. image:: a_taxe_amenagement_form.png


Gestion des commissions
#######################

.. _parametrage_type_commission:

========================
Les types de commissions
========================

(:menuselection:`Paramétrage --> Gestion des commissions --> Type De Commission`)

Éditions des différents types de commissions (Commission technique d'urbanisme).

.. image:: a_type_commission_parametrage.png

Le champ 'Liste de diffusion' est la liste d'adresses email pour lesquelles un courriel va être envoyé à chaque diffusion.
Elles doivent être séparées par un retour à la ligne.

Le champ 'Participants' est est de type texte libre, les participants ne sont pas obligés d'avoir un compte sur l'application.

Les champs qui peuvent être repris pour une édition sont le 'libellé', la 'salle',  l' 'adresse' et son 'complement'.

Le champ 'Collectivité' permet de saisir des collectivités de niveau mono uniquement : les commissions intercommunales ne sont pas gérées.

Gestion des consultations
#########################

.. _parametrage_avis_consultation:

=========================
Les avis de consultations
=========================

(:menuselection:`Paramétrage --> Gestion des consultations --> Avis Consultation`)

Éditions des différents avis possibles en réponse aux consultations de services

.. _parametrage_service:

============
Les services
============

(:menuselection:`Paramétrage --> Gestion des consultations --> Service`)

Ce menu sert au paramétrage des services consultés de l'application.

.. image:: a_service_parametrage.png

Les champs de 'Abrégé' à 'Liste de diffusion' permettent de saisir les coordonnées du service.

Le champ 'Liste de diffusion' peut contenir plusieurs adresses e-mail séparées par des sauts de ligne.

Le champ 'notification par mail' sert à indiquer si le service souhaite être 
notifié par mail lors de l'ajout d'une nouvelle demande de consultation. Le mail envoyé
au service consulté contient 2 liens d'accès à openADS, qui sont :ref:`paramétrables <administration_parametrage_service_consulte>`.

Le champ 'type de délai' spécifie le type du délai, c'est-à-dire si le calcul de la date limite doit être fait en mois ou en jours.

Le champ 'délai' indique le temps dont dispose le service pour répondre à une 
demande de consultation.

Le champ 'consultation papier' indique si un PDF doit être généré pour 
l'instructeur au moment de la demande de consultation.

Les champs 'Date de début de validité' et 'Date de fin de validité' permettent d'indiquer les dates pour lesquelles une 
demande de consultation à ces services est possible.

Le champ 'type de consultation' spécifie le type de la consultation. Le type 
choisi a un impact sur le logiciel :

- "Pour information", qui permet à l'instructeur de signaler à un service l'existence d'une opération en cours. Elle est strictement « informative » et n'implique pas de retour d'avis de la part du service concerné.
- "Avec avis attendu", que l'instructeur déclenche lorsqu'il attend un retour d'avis de la part du service consulté. Elles s'afficheront avec un fond jaune dans le tableau listant les demandes de consultation du dossier d'instruction
- "Pour conformité", similaire à la précédente, mais qui n'intervient pas au même moment au cours du processus métier : le contenu de la demande de consultation et le délai associé différent.

Le champ 'type d'édition de la consultation' sert à indiquer le type d'édition
lié à la demande de consultation. Ce select est populé grâce aux états. 

Le champ 'peut recevoir une notification d'instruction ' sert à identifier les services qui pourront être notifiés via :ref:`l'action de notification des services<notification_services_instruction>` depuis une instruction.

Pour qu'un état apparaisse dans la liste des types d'édition possibles, il faut 
que le libellé de l'état soit préfixé par 'consultation\_'.

.. _parametrage_thematique_services:

===========================
Les thématiques de services
===========================

(:menuselection:`Paramétrage --> Gestion des consultations --> Thématique Des Services`)

Éditions des groupes de services.

.. _parametrage_lien_service_thematique:

===============================================
Les liens entre les services et les thématiques
===============================================

(:menuselection:`Paramétrage --> Gestion des consultations --> Lien Service / Thématique`)

Liaison des services aux différents groupes de services.

.. _parametrage_categorie_tiers_consultes:

=================================
Les catégorie des tiers consultés
=================================

(:menuselection:`Paramétrage --> Gestion des consultations --> Catégorie Des Tiers Consultés`)

Éditions des catégorie des tiers consultés.

.. _parametrage_tiers_consultes:

===================
Les tiers consultés
===================

(:menuselection:`Paramétrage --> Gestion des consultations --> Tiers Consultés`)

Ce menu sert au paramétrage des tiers consultés de l'application.

Filtrage du listing des tiers consultés
=======================================

.. image:: a_listing_tiers.png

Le listing des tiers est filtré selon la collectivité de la catégorie des tiers.
Ainsi un utilisateur de la collectivité *Marseille* ne verra que les tiers consulté dont la catégorie est liée à la collectivité *Marseille*.
Au niveau de la recherche avancée cet utilisateur ne pourra filtrer que par catégories liées à sa collectivité.

Pour les utilisateurs de la collectivité de niveau 2 tous les tiers consulté sont visible, une colonne supplémentaire est affichée dans le listing
et un filtre supplémentaire "collectivité" est accessible dans la recherche avancée.
La colonne supplémentaire indique la collectivité à laquelle le tiers est rattaché.
En mode service consulté cette colonne s'intitule "service" sinon elle s'intitule "collectivité".

.. note::

  Pour les utilisateurs de la collectivité de niveau 2, dans le cas où une catégorie de tiers est liée à plusieurs collectivités,
  un même tiers sera affiché autant de fois qu'il y a de collectivités rattachées à sa catégorie.

Paramètrage des tiers consultés
===============================

.. image:: a_tiers_parametrage.png

Les champs de ce formulaire sont :

- "Catégorie de tiers consulté" : champs obligatoire qui permet de choisir la catégorie à laquelle le tiers est rattaché. Tout comme pour le listing, ce champs est filtré en fonction de la collectivité d'appartenance de l'utilisateur.
- "Abrégé" : libellé abrégé qui sera utilisé lorsque le tiers est affiché sous la forme *abrégé - libellé*.
- "Libellé" : libellé qui sera utilisé lorsque le tiers est affiché sous la forme *abrégé - libellé*.
- "Adresse" : adresse associée au tiers consulté.
- "Complément" : complément d'adresse.
- "Cp" : code postal.
- "Ville" : permet de saisir la ville du tiers consulté.
- "Liste de diffusion" : liste des courriels permettant de notifier le tiers consulté. Ce champ peut en contenir plusieurs séparés par des sauts de ligne.
- "Accepte la notification par mail" : case à cocher permettant de savoir si la notification par courriel est possible pour le tiers.
- "Identifiant acteur Plat'AU" : identifiants de l'acteur sur Plat'AU. Ce champ peut en contenir plusieurs séparés par des sauts de ligne.

Export des tiers consultés
==========================

Il est possible d'exporter les données relatives aux tiers consultés au format csv en cliquant sur l'icône d'export du listing.

L'export contiens les informations suivantes :

 - "tiers consulté" : identifiant du tiers.
 - "abrégé" : libellé abrégé du tiers.
 - "libellé tiers consulté" : libellé du tiers.
 - "adresse" : adresse du tiers.
 - "complément" : complément d'adresse du tiers.
 - "cp" : code postale du tiers.
 - "ville" : ville du tiers.
 - "liste de diffusion" : liste des courriels du tiers.
 - "accepte la notification par mail" : indique si le tiers peut être notifié par mail ou pas.
 - "identifiant acteur plat'au" : identifiant du tiers (acteur) dans plat'AU.
 - "utilisateur" : logins des utilisateurs de l'application rattachés au tiers.
 - "catégorie de tiers consulté" : catégorie du tiers.
 - "libellé catégorie de tiers consulté" : libellé de la catégorie du tiers.
 - "code" : code associé à la catégorie du tiers.
 - "description" : description de la catégorie du tiers.
 - "date de début de validité catégorie de tiers consulté" : date de début de validité de la catégorie du tiers.
 - "date de fin de validité catégorie de tiers consulté" : date de fin de validité de la catégorie du tiers.
 - "collectivité" : collectivités auxquelles est rattaché la catégorie de tiers.
 - "type d'habilitation de tiers consulté" : identifiant du type d'habilitation du tiers.
 - "libellé type d'habilitation de tiers consulté" : libellé du type d'habilitation du tiers.
 - "texte d'agrément" : texte d'agrément associé à l'habilitation du tiers.
 - "identifiant de la commune" : identifiant de la division territoriale d’intervention commune de l'habilitation du tiers.
 - "commune" : libellé de la division territoriale d’intervention commune de l'habilitation du tiers.
 - "identifiant du département" : identifiant de la division territoriale d’intervention département de l'habilitation du tiers.
 - "département" : libellé de la division territoriale d’intervention département de l'habilitation du tiers.
 - "division territoriales" : division territoriale de l'habilitation du tiers.
 - "date de début de validité habilitation de tiers consulté" : date de début de validité de l'habilitation du tiers.
 - "date de fin de validité habilitation de tiers consulté" : date de fin de validité de l'habilitation du tiers.
 - "spécialité de tiers consulté" : spécialité de l'habilitation du tiers.

.. note::

  Si le listing est filtré, l'export l'est aussi. Par exemple, si un filtrage est réalisé pour n'afficher que les tiers d'une
  collectivité A, l'export ne contiendra que les tiers de la collectivité A.

.. _parametrage_habilitation_tiers_consultes:

=====================================
Les habilitations des tiers consultés
=====================================

(:menuselection:`Paramétrage --> Gestion des consultations --> Habilitation Des Tiers Consultés`)

Éditions des habilitations des tiers consultés.

.. _parametrage_type_habilitation_tiers_consultes:

=============================================
Les types d'habilitations des tiers consultés
=============================================

(:menuselection:`Paramétrage --> Gestion des consultations --> Type D'habilitation Des Tiers Consultés`)

Éditions des types d'habilitations des tiers consultés.

.. _parametrage_specialite_tiers_consultes:

===================================
Les spécialités des tiers consultés
===================================

(:menuselection:`Paramétrage --> Gestion des consultations --> Spécialité Des Tiers Consultés`)

Éditions des spécialités des tiers consultés.

.. _parametrage_motif_consultation:

===========================
Les motifs de consultations
===========================

(:menuselection:`Paramétrage --> Gestion des consultations --> Motif De Consultation`)

Éditions des motifs de consultations.

Gestion des documents
#####################

.. _parametrage_document:

===========================
Les catégories de documents
===========================

(:menuselection:`Paramétrage --> Gestion des documents --> Catégorie De Documents`)

Paramétrage des catégories de documents possibles.

Liste des catégories de type de document

.. image:: a_document_type_categorie_parametrage.png

Les champs du formulaire lors de l'ajout :

  * **Libellé** : Libellé de la catégorie du type de document, champ obligatoire utilisé dans la liste à choix lors de l’ajout d’un type de document ;
  * **Code** : Code de la catégorie du type de document, le code n’est pas obligatoire mais il doit être unique ;

.. image:: a_document_type_categorie_form_ajout.png

.. note::

  On ne peux pas ajouter de catégorie avec le code **PLATAU**. Le code de cette catégorie est réservé.
  Elle ne peut être ni modifiée ni supprimée.
  De même pour un type de document en lien avec la catégorie **PLATAU**.
  On ne peut pas en ajouter, ni en modifier/supprimer.

Pour le reste, toute autre catégorie de document peut être ajoutée, modifiée ou supprimée. (Ex: une catégorie avec le code "Autre")
Ainsi que son type de document associé.

.. _parametrage_categorie_document:

======================
Les types de documents
======================

(:menuselection:`Paramétrage --> Gestion des documents --> Type De Documents`)

Paramétrage des types de documents possibles.

Liste des types de document

.. image:: a_document_type_parametrage.png

Les champs du formulaire lors de l'ajout :

  * **Libellé** : Libellé du type de document, champ obligatoire utilisé dans la liste à choix lors de l’ajout d’un type de document dans le paramétrage d'un évènement (paramétrage dossiers > workflows > événement) ;
  * **Code** : Code du type de document, champ obligatoire ;
  * **Catégorie de documents** : Type de document, champ obligatoire utilisé pour organiser les types de document sur tous les affichages ;
  * **Date de début de validité** : Date de début de validité du type de document ;
  * **Date de fin de validité** : Date de fin de validité du type de document ;

.. image:: a_document_type_form_ajout.png

.. note::

  On ne peut pas ajouter un type de document de "catégorie de documents" **Plat'AU**. Les types de document de cette catégorie sont réservés.
  Ils ne peuvent être ni modifiés ni supprimés.

Toute autre **catégorie de documents** peut être ajoutée, modifiée ou supprimée. 
(Ex: un type de document avec la **catégorie de documents** "Autre")

.. _parametrage_type_document:

Gestion des dossiers
####################

.. _parametrage_etat_dossier_autorisation:

=====================================
Les états des dossiers d'autorisation
=====================================

(:menuselection:`Paramétrage --> Gestion des dossiers --> États Des Dossiers D'autorisation`)

Liste des états de dossiers d'autorisation possibles.

.. _parametrage_lien_evenement_da:

======================================================================
Les liens entre les évènements et les types de dossiers d'autorisation
======================================================================

(:menuselection:`Paramétrage --> Gestion des dossiers --> Lien Événement Dossier Autorisation Type`)

Liens entre les événements et les types de dossiers d'autorisation.

.. _parametrage_affectation_automatique:

=============================
Les affectations automatiques
=============================

(:menuselection:`Paramétrage --> Gestion des dossiers --> Affectation Automatique`)

Configuration de l'affectation automatique des instructeurs (primaire et secondaire) aux dossiers d'instruction par communes (seulement pour le mode service consulté), le type détaillé de dossier d'autorisation, le type de dossier d'instruction, l'arrondissement, le quartier et/ou la section.

L'affectation affecte à la fois l'instructeur primaire et l'instructeur secondaire.
Il peut y avoir des affectations avec seulement un instructeurs primaire, mais il ne peut pas y avoir d'affectation avec seulement un instructeur secondaire.

Les critères de filtre de l'affectation automatique respectent un ordre de priorités précis, dans l'ordre :

* par communes
* par type de dossier d'instruction
* par type détaillé de dossier d'autorisation
* par quartier
* par section
* et enfin par arrondissement

Exemple d'utilisation du paramétrage :

* paramétrage d'une affectation automatique de l'instructeur primaire X et l'instructeur secondaire Y sur les dossiers d'instruction de type DECLARATION PREALABLE ;
* paramétrage d'une affectation automatique de l'instructeur primaire A et l'instructeur secondaire B sur les dossiers d'instruction de type DECLARATION PREALABLE avec le quartier 801 ;
* paramétrage d'une affectation automatique de l'instructeur primaire Z sur les dossiers d'instruction avec le quartier 811.

En cas de dépôt d'un dossier d'instruction :

* de type DECLARATION PREALABLE sans localisation, alors affectation des instructeurs X et Y ;
* de type DECLARATION PREALABLE avec le quartier 801, alors affectation des instructeurs A et B ;
* de type DECLARATION PREALABLE avec le quartier 811 (ou autre différent de 801), alors affectation des instructeurs X et Y ;
* de type PERMIS DE CONSTRUIRE (ou autre type différent de DECLARATION PREALABLE) avec le quartier 811, alors affectation de l'instruction Z ;
* de type PERMIS DE CONSTRUIRE avec le quatier 801, alors acune affectation car aucune correspondance.

Spécificitées
=============

Le champ **communes**
*********************

* sa valeur doit être une liste de code INSEE de communes ou de départements, séparés par des virgules (sans espace entre)
* sera utilisé pour filtrer les affectations automatiques possibles, selon que la commune sélectionnée correspondra à la valeur saisie dans ce champ, uniquement si l':ref:`option dossier_commune<administration_liste_options>` est activée

Par exemple, pour que l'affectation automatique ne s'applique que sur les 9ème et 10ème arrondissements de Marseille, alors on saisira la valeur *13209,13210*.

Le champ **affectation manuelle**
*********************************

* fait apparaître l'affectation dans une liste déroulante au moment de la création des dossiers. L'affectation qui sera sélectionnée manuellement par l'utilisateur à la création d'un dossier, prévaudra sur les autres affectations automatiques définies pour ce même type de dossier, et verra donc l'instructeur qui lui est associé être affecté au dossier d'instruction créé
* limite l'effet de cette affectation au cas où l'utilisateur la sélectionne manuellement à la création d'un dossier (ne sera pas utilisée par défaut)
* est incompatible avec la saisie des champs *arrondissement*, *section*, *quartier* (soit l'une soit les autres)

Par exemple, si on souhaite permettre, lors d'une nouvelle demande, d'affecter automatiquement le dossier à Damien Khalifa ou Sadi Prara en sélectionnant l'affectation désirée dans une liste déroulante, alors il faudra créer 2 affectations automatiques, renseignant le champ **affectation manuelle**, l'une avec pour valeur *Damien Khalifa* et l'autre *Sadi Prara*.

*Note: La valeur en elle-même ne sert qu'à l'affichage dans la liste déroulante, peut importe ce qu'elle contient tant qu'elle est non-vide.*
*Si on ne renseigne pas ce champ (valeur vide), alors l'affectation automatique ne sera pas proposée dans le guichet unique lors d'une nouvelle demande, et servira uniquement lors du calcul automatisé de l'affectiation de l'instructeur.*

.. _parametrage_autorite_competente:

=========================
Les autorités compétentes
=========================

(:menuselection:`Paramétrage --> Gestion des dossiers --> Autorité Compétentes`)

Édition des autorités compétentes possibles pour les dossiers de l'application.

.. _parametrage_phase:

==========
Les phases
==========

(:menuselection:`Paramétrage --> Gestion des dossiers --> Phase`)

La phase est un indicateur permettant un pré-aiguillage des courriers lors d'un retour d'avis de réception d'une :ref:`lettre recommandée <suivi_envoi_lettre_rar>`.
Son affichage ne se fera que si elle est paramétrée sur l':ref:`événement <parametrage_dossiers_evenement>` qui génère une édition adressée au demandeur.

Le formulaire est constitué de seulement trois champs :

  * **code** : code de la phase sur 15 caractères, c'est la valeur affichée sur les lettres recommandées ;
  * **date de début de validité** : date de la mise en service de la phase (par défaut la date courante) ;
  * **date de fin de validité** : date de fin de service de la phase, après cette date la phase ne sera plus sélectionnable depuis les événements.

.. image:: m_parametrage_phase.png

.. _parametrage_pec:

===========================
Les prises en compte métier
===========================

(:menuselection:`Paramétrage --> Gestion des dossiers --> Prise en compte métier`)

Notion utilisée seulement dans un openADS paramétré en mode service consulté. Voir l'option :ref:`option_mode_service_consulte<administration_liste_options>`.

La prise en compte métier (PeC) est un indicateur permettant de définir sur un dossier d'instruction, si la consultation est prise en compte ou non.
La modification de sa valeur sur le dossier d'instruction de consultation ne peut être modifiée que par une instruction.
Il faut donc la paramétrer sur un :ref:`événement d'instruction <parametrage_dossiers_evenement>` et positionner une :ref:`action <parametrage_dossiers_action>` qui utilise la règle pour modifier les PeC.

Le formulaire est constitué des champs :

  * **Code** : permet de codifier la prise en compte métier.
  * **Libellé** : texte affiché dans l'interface lors du choix d'une prise en compte métier.
  * **Description** : texte descriptif de la prise en compte métier.
  * **Date de début de validité** : date de début de validité de la prise en compte métier.
  * **Date de fin de validité** :  date de fin de validité de la prise en compte métier.

.. _parametrage_modele_rapport_instruction:

====================================
Les modèles de rapport d'instruction
====================================

Les modèles permettent de pré-remplir la partie *analyse réglementaire* des rapports d'instruction avec le contenu paramétré.

Le formulaire est constitué des champs :

  * **Libellé** : libellé du modèle, champ obligatoire qui servira à identifier les modèles sur les rapports d'instruction.
  * **Code** : code du modèle.
  * **Contenu** : contenu qui sera copié sur le rapport si le modèle est sélectionné.
  * **Date de début de validité** : les modèles pas encore valide ne seront pas utilisables.
  * **Date de fin de validité** : les modèles n'étant plus valide ne seront pas utilisables.

.. _parametrage_gestion_pieces:

Gestion des pièces
##################

.. _parametrage_document_numerise_type_categorie:

====================
Catégorie des pièces
====================

(:menuselection:`Paramétrage --> Gestion des pièces --> Catégorie des pièces`)

Paramétrage des catégories de pièces possibles.

Les champs du formulaire lors de l'ajout :

  * **Libellé** : Libellé de la catégorie de pièce, champ obligatoire utilisé dans la liste à choix lors de l'ajout d'un type de pièce ;
  * **Code** : Code de la catégorie de pièce, le code n'est pas obligatoire mais il doit être unique ;

.. note::

  Le code "PLATAU" est utilisé pour identifier la catégorie des pièces transmissibles à Plat'AU.
  La catégorie ayant ce code ne pourra pas être modifiée, ni supprimée.

.. _parametrage_document_numerise_type:

===============
Type des pièces
===============

(:menuselection:`Paramétrage --> Gestion des pièces --> Type des pièces`)

Paramétrage des types de pièces possibles.

Les champs du formulaire lors de l'ajout :

  * **Code** : Code du type de pièce, champ obligatoire utilisé pour composer le nom des pièces. Les codes en cours de validité sont uniques ;
  * **Libellé** : Libellé du type de pièce, champ obligatoire utilisé dans la liste à choix lors de l'ajout d'une pièce ;
  * **Catégorie de pièces** : Catégorie du type de pièce, champ obligatoire utilisé pour organiser les pièces sur tous les affichages ;
  * **Ajoutable par les instructeurs** : Permet de définir si le type de pièce peut être ajouté par un instructeur, par défaut coché ;
  * **Affiché sur les demandes d'avis** : Permet de définir si les pièces de ce type peuvent être visualisées sur les demandes d'avis des services consultés, par défaut coché ;
  * **Affiché sur les DA** : Permet de définir si les pièces de ce type peuvent être visualisées sur les dossiers d'autorisation, par défaut coché.

.. image:: a_parametrage_document_numerise_type_form.png

Lors de la modification d'un type de pièce, si les champs **Affiché sur les demandes d'avis** et/ou **Affiché sur les DA** sont modifiés, alors les métadonnées correspondantes sur les fichiers de ce type seront mises à jour lors de la prochaine :ref:`mise à jour des métadonnées <parametrage_document_numerise_type_traiter_metadonnees>`.

.. note::

  La catégorie ayant le code "PLATAU" ne pourra pas être sélectionnée dans la liste des catégories.
  Les types de pièce liés à cette catégorie ne sont pas modifiable.

.. _parametrage_document_numerise_nomenclature:

=======================
Nomenclature des pièces
=======================

(:menuselection:`Paramétrage --> Gestion des pièces --> Nomenclature des pièces`)

Paramétrage des nomenclatures d'un type de pièce pour un type de dossier.

Les champs du formulaire lors de l'ajout :

  * **Type de pièce** : Type de pièce à laquelle le code est associé ;
  * **Type de dossier d'instruction** : Type de dossier auquel le code est associé ;
  * **Code** : Code de la pièce  ;

.. image:: a_parametrage_document_numerise_nomenclature_form.png

.. _parametrage_document_numerise_type_traiter_metadonnees:

===========================
Mise à jour des métadonnées
===========================

(:menuselection:`Paramétrage --> Gestion des pièces --> Mise à jour des métadonnées`)

Mise à jour des métadonnées des fichiers stockés dont le type de pièce a été modifié.

Lors de la modification d'un type de pièce, si les champs **Affiché sur les demandes d'avis** et/ou **Affiché sur les DA** sont modifiés, un marqueur identifie le changement, mais les fichiers des pièces ciblées ne sont pas mis à jour.
Ce changement peut être appliqué ensuite à l'intégralité des fichiers des pièces de ce type par deux méthodes :

  * depuis l'interface réservée aux administrateurs ;
  * de manière désynchronisée, en tâche de fond, par un appel à un :ref:`service web de maintenance <web_services_ressource_maintenance_maj_metadonnees_documents_numerises>`.


Depuis l'interface
==================

.. image:: a_parametrage_document_numerise_metadata_treatment.png

Il suffit de cliquer sur le bouton **Mettre à jour** pour lancer le traitement.

.. image:: a_parametrage_document_numerise_metadata_treatment_res.png

Lorsque certaines pièces numérisées n'ont pas pu être mises à jour, le message de validation présente la liste des pièces en erreur ainsi que le dossier d'instruction correspondant.

.. _parametrage_gestion_contentieux:

Gestion des contentieux
#######################

.. _parametrage_objet_recours:

=====================
Les objets de recours
=====================

(:menuselection:`Paramétrage --> Gestion des contentieux --> Objet de recours`)

Paramétrage des objets de recours possibles.

.. _parametrage_moyen_souleve:

===================
Les moyens soulevés
===================

(:menuselection:`Paramétrage --> Gestion des contentieux --> Moyen soulevé`)

Paramétrage des moyens soulevés possibles.

.. _parametrage_moyen_retenu_juge:

================================
Les moyens retenus par les juges
================================

(:menuselection:`Paramétrage --> Gestion des contentieux --> Moyen retenu par le juge`)

Paramétrage des moyens possibles retenus par les juges.

.. _parametrage_suggestion_contrainte:

Paramétrage des suggestions liées aux contraintes
#################################################

(:menuselection:`Paramétrage --> Geolocalisation --> Contraintes De Référence`)

Le paramétrage des suggestions d'événement est fait au niveau des contraintes de référence.

En consultation d'une contrainte de référence, cliquer sur l'onglet **Événements Suggérés** pour consulter la
liste de tous les événements suggérés pour cette contrainte.
Cliquer sur la croix verte, sélectionner un événement et cliquer sur valider pour ajouter une suggestion d'événement
à la contrainte.

Une fois paramétrées, les suggestions seront visibles dans l'onglet **Contrainte(s)** d'un dossier si toutes ces conditions sont remplies :

* Une *contrainte* ayant le même libellé que celle de la *contrainte de référence* est ajoutée au dossier.
* Le type du dossier d'instruction appartient aux *type(s) de DI concerné(s)* de la contrainte de référence.
* La collectivité du dossier appartient aux *collectivité(s) concernée(s)* de la contrainte de référence.
