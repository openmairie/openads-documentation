.. _guichet_unique:

##############
Guichet unique
##############

.. _guichet_unique_menu:

Le menu
#######

.. image:: m_guichet_unique_menu.png

.. _guichet_unique_nouvelle_demande:

Les nouvelles demandes
######################

==================
Saisir une demande
==================

Une demande peut être de deux natures différentes : elle peut concerner soit un
nouveau dossier ou soit un dossier existant.

.. image:: m_guichet_unique_nouvelle_demande_schema.png

.. _guichet_unique_nouvelle_demande_nouveau_dossier:

Pour un nouveau dossier
=======================

(:menuselection:`Guichet Unique --> Nouvelle demande --> Nouveau Dossier`)

La demande va donner lieu à la création d'un nouveau dossier d'autorisation.
La première étape est donc la sélection du type de dossier d'autorisation qui a
été déposé.
On sélectionne ensuite le type de dossier d'autorisation détaillé.
Enfin, on sélectionne le type de demande.

Éventuellement, si des affectations automatiques sont configurées, on peut en sélectionner une.


.. _guichet_unique_nouvelle_demande_dossier_en_cours:

Pour un dossier en cours
========================

(:menuselection:`Guichet Unique --> Nouvelle demande --> Dossier en cours`)

La demande va se rattacher à un dossier d'autorisation en cours de traitement,
les dossiers affichés sont donc acceptés ou en cours d'instruction.
Après avoir sélectionné le dossier existant sur lequel on souhaite créer une nouvelle demande, on sélectionne le type de demande.

Lors du dépôt d'un dossier d'instruction sur existant, le CERFA utilisé est celui paramétré sur le type détaillé de dossier d'autorisation au moment du dépôt, et donc peut-être différent de celui de l'autorisation visée par le dépôt.
Les données techniques du dossier d'autorisation sont récupérées et nettoyées en fonction du CERFA, puis insérées dans le nouveau dossier d'instruction. Par exemple, les champs qui ne sont plus affichés dans la nouvelle version du CERFA, verront leur valeur supprimée sur le nouveau dossier d'instruction.


.. _guichet_unique_liste_type_demande:

Liste des types de demande
==========================

Les types de demande proposés, en plus d'être filtrés sur le type de dossier d'autorisation détaillé (soit sélectionnés dans le cas d'une nouvelle demande, soit récupérés depuis le dossier d'instruction ciblé) sont récupérés de plusieurs façons défini dans le :ref:`paramétrage des types de dossier d'instruction<parametrage_dossiers_dossier_instruction_type>` et dans le :ref:`paramétrage des types de demande<parametrage_dossiers_demande_type>` :

* qui ne créé pas de nouveau dossier d'instruction et dont l'état du dossier d'instruction ciblé fait partie des états autorisés,
* qui créé de nouveau dossier d'instruction, dont l'état du dossier d'instruction ciblé fait partie des états autorisés et dont le dossier d'autorisation est accordé,
* qui créé de nouveau dossier d'instruction, dont les types des dossiers d'instruction en cours sur le dossier d'autorisation ciblé accordé, sont identiques à la liste des types de dossier d'instruction compatibles,
* qui créé de nouveau dossier d'instruction, dont les types des dossiers d'instruction en cours sur le dossier d'autorisation ciblé en cours d'instruction, sont identiques à la liste des types de dossier d'instruction compatibles et dont le dossier d'instruction initial n'est pas de compétence *commune*.

.. note::

    * L'identifiant de l'autorité compétente représentant *la commune* doit obligatoirement être **1**.
    * L'identifiant de l'état de dossier d'autorisation représentant *l'accord* doit obligatoirement être **2**.
    * L'identifiant de l'état de dossier d'autorisation représentant *l'instruction en cours* doit obligatoirement être **1**.


Liste des affectations manuelles
================================

Si des affectations automatiques ont été configurées (voir :ref:`Paramétrage des affectations automatiques<parametrage_affectation_automatique>`) avec le champ **affectation manuelle** alors une liste apparaitra sous le type de demande ou sous le type de dossier d'autorisation détaillé, et celle-ci sera filtrée en fonction des éléments suivants (par ordre de priorité):

* collectivité
* type dossier autorisation détaillé
* type de demande

Si une affectation est sélectionnée manuellement, alors l'instructeur qui lui est associé sera automatiquement affecté au nouveau dossier créé (même si une autre affectation automatique est disponible pour ce type de dossier). C'est à dire que l'affectation **sélectionnée manuellement** prévaut sur l'affectation automatique réalisée par défaut.

Si l'option :ref:`option_filtre_instructeur_DI_par_division<administration_liste_options>` est activée, la liste des instructeurs à affecter comportera seulement les instructeurs partageant la même division que l'utilisateur.

Saisie de la nouvelle demande pour les dossiers nouveaux et existant
====================================================================

Après la sélection du type de la demande,le reste de la saisie consiste en la saisie des informations suivantes :

* la ou les dates de réception (en fonction des options activées sur l'application)
* les demandeurs tels que (dépend du type de dossier en cours de saisie, la liste suivante indique ceux proposés pour un dossier d'urbanisme) :

  * le demandeur principal
  * le délégataire/correspondant
  * le ou les co-demandeurs
  * le propriétaire
  * l'architecte (cadre sur la légilsation connexe du CERFA)
  * le concepteur paysagiste

* la ou les références cadastrales et la superficie du terrain
* l'adresse du terrain
* la nature des travaux du projet (liste à paramétrer)

Dans le cas d'une multi-collectivité, il est également possible de sélectionner la collectivité.

Pour ajouter un demandeur depuis ce formulaire de saisie, il suffit de cliquer sur le bouton du demandeur souhaité, à l'ouverture de la fenêtre modale de saisir les informations concernant le demandeur, puis de cliquer sur le bouton "Ajouter le demandeur".
À la validation du formulaire, la fenêtre modale sera automatiquement fermée et une synthèse du demandeur ajouté sera affichée sur le formulaire de la demande.
Depuis chaque synthèse de demandeur, il est possible de modifier le demandeur ou même de le supprimer directement.

.. image:: a_guichet_unique_nouvelle_demande_saisie_demandeur.png

Toutes les informations ne sont pas nécessairement disponibles pour la saisie,
en effet si la saisie concerne un nouveau dossier alors la saisie du
pétitionnaire est nécessaire alors que si la demande concerne un dossier
existant les informations du pétitionnaire sont déjà préremplies et il n'est
donc pas nécessaire de les saisir.

En fin de formulaire, un bloc affiche la liste des documents obligatoires paramétrés pour le type de demande considéré (voir :ref:`parametrage_dossiers_demande_type` pour paramétrer cette liste de document).


.. _guichet_unique_nouvelle_demande_saisie_numero:

Saisie du numéro de dossier
============================

Lors de la saisie d'une nouvelle demande, si l':ref:`option de saisie du numéro de dossier<administration_liste_options>` est activée, il est possible de saisir la numérotation de l'autorisation depuis le formulaire.

Cinq champs sont alors affichées :

* le **code du type du dossier d'autorisation**, non modifiable par l'utilisateur il se met à jour en fonction du type de dossier d'autorisation détaillé sélectionné
* le **code composé du département et de la commune**, non modifiable par l'utilisateur il se met à jour en fonction de la collectivité sélectionné
* l'**année**, non modifiable par l'utilisateur elle se met à jour en fonction de la date de la demande saisie ou de la date de dépôt en mairie si :ref:`l'option d'affichage de la date de dépôt en mairie<administration_liste_options>` est activée et qu'une date est saisie dans ce champ.
* le **code de la division**, modifiable par l'utilisateur sur un seul caractère alphanumérique, sa valeur par défaut est récupérée de la division de l'instructeur qui sera affecté automatiquement au dossier si :ref:`l'option de récupération de la division<administration_liste_options>` est activée
* le **numéro**, modifiable par l'utilisateur sur quatre caractères maximum, sa valeur par défaut est récupérée de la numérotation suivante automatique

.. image:: a_guichet_unique_nouvelle_demande_saisie_numero.png

Il suffit de cliquer sur la case à cocher **Compléter la numérotation** pour rendre modifiable les deux derniers champs.

.. note::

    Les valeurs par défaut du **code de la division** et du **numéro** sont récalculées à chaque activation de **Compléter la numérotation**.


Si l':ref:`option de saisie complète du numéro de dossier<administration_liste_options>` est activée, il est possible de saisir le numéro complet du dossier avec la syntaxe souhaitée.

.. image:: a_guichet_unique_nouvelle_demande_saisie_num_doss_complet.png

Si vous n'avez pas de numéro de dossier à saisir il suffit de cocher **Je n'ai pas de numéro de dossier**, les 5 champs de numéro de dossier apparaitrons alors avec le numéro de dossier attendus.

.. warning::  Lorsque l'option de saisie complète du numéro de dossier est activée, si la syntaxe utilisée n'est pas réglementaire, l'incrémentation du numéro pour les dossiers suivants n'est pas garantie. 

Si l':ref:`option de saisie complète du numéro de dossier<administration_liste_options>` est activée, et que la valeur saisie respecte le format imposé par le code de l'urbanisme, alors une suggestion des types de dossier d’autorisation détaillé est effectué en fonction du numéro saisie.

.. image:: a_guichet_unique_nouvelle_demande_saisie_datd_num_doss_complet.png

.. _guichet_unique_nouvelle_demande_saisie_commune:

Saisie de la commune associée au dossier
========================================

Lors de la saisie d'une nouvelle demande, si l':ref:`option dossier_commune<administration_liste_options>` est activée, il est possible d'associer une commune au dossier en remplissant le champ dédié.

.. image:: a_guichet_unique_nouvelle_demande_saisie_commune.png

La liste des communes sélectionnables, sera filtrée par la date de la demande (chaque commune ayant une date de validité), et éventuellement par la collectivité si le :ref:`paramètre communes<parametrage_parametre_collectivite_communes>` est configuré pour la collectivité sélectionnée.

Avec l'option activée, la saisie d'une commune devient obligatoire, et cela va avoir un impact sur :

* le numéros de dossier, car la partie du numéro de dossier concernant le code département/commune sera issue de la commune sélectionnée (et plus de la collectivité renseignée)
* l'affectation automatique, car les affectations automatiques manuelles sélectionnables seront filtrées selon la commune saisie.

Ainsi un changement de la date de la demande, bien qu'elle soit située sous le champ 'commune', pourrait remettre à zéro ce dernier.
En effet puisque les communes ont des dates de validités, un changement dans la date de la demande, va impacter la commune sélectionnable.

.. image:: a_guichet_unique_nouvelle_demande_saisie_commune_date_demande.png

Si l':ref:`option de saisie complète du numéro de dossier<administration_liste_options>` est activée, et que la valeur saisie respecte le format imposé par le code de l'urbanisme, alors une suggestion des communes en fonction du numéro saisie est générée.

.. image:: a_guichet_unique_nouvelle_demande_saisie_commune_num_doss_complet.png

.. _guichet_unique_nouvelle_demande_message_di_exist:

Message d'information sur l'existance d'un dossier d'instruction avec le même numéro de dossier
===============================================================================================

Lors de la saisie d'une nouvelle demande, si l'utilisateur saisie la collectivité et le numéro de dossier (en plus du dossier d'autorisation de type détaillé et du type de demande), et qu'un dossier d'instruction avec le même numéro de dossier et la même collectivité existe, alors un message d'information est affiché.
Dans ce message d'information, ont peut y voir les demandeurs principaux existant, avec l'identité (personne physique/morale) et le type de demandeur, ainsi que le numéro du dernier dossier d'instruction lié.
Il y a également une url de redirection **Ouvrir dans un nouvel onglet** permettant d'accéder directement au dernier dossier d'instruction déposé en mode consultation dans un nouvel onglet, et un bouton permettant la copie des données des demandeurs principaux **UTILISER LES INFORMATIONS DE CE DOSSIER**. Les données du/des demandeurs seront automatiquement insérées dans le formulaire correspondant au même type de demandeur, si le contexte permet l'ajout de ce type de demandeur, et qu'un demandeur principale de ce type existe.

.. image:: a_guichet_unique_nouvelle_demande_message_information_di_exist.png

Les options suivantes doivent être activées : 

* :ref:`option_dossier_saisie_numero_complet<administration_liste_options>`
* :ref:`option_mode_service_consulte<administration_liste_options>`


.. _guichet_unique_nouvelle_demande_copie_adresse_demandeur:

Saisie automatique des champs de l'adresse du terrain
=====================================================

Lors de la saisie d'une nouvelle demande, si un pétitionnaire a été ajouté et que son adresse est renseignée, il est possible en cliquant sur le bouton **adresse du demandeur**, de copier automatiquement les informations de l'adresse du pétitionnaire et de les insérer dans les champs correspondants de l'adresse du terrain.

.. image:: a_guichet_unique_nouvelle_demande_fieldset_localisation_apres_copie_adresse_demandeur.png


.. _guichet_unique_nature_travaux:

Sélection des natures de travaux
================================

Lors de la saisie d'une nouvelle demande, si des :ref:`nature de travaux<parametrage_dossiers_nature_travaux>` ont été préalablement paramétrées, elles peuvent être sélectionnées depuis le champs **nature des travaux**.

Dans ce champs, les résultats sont filtrés pour n'afficher que celles :

* compatibles avec le type de dossier de la demande en cours
* en cours de validité


.. _guichet_unique_champ_requis_platau:

Champs requis pour la transmission à Plat'AU
============================================

Si le type de dossier d'autorisation détaillé est transmissible à Plat'AU (voir :ref:`le paramétrage des types de dossier d'autorisation détaillés<parametrage_dossiers_dossier_autorisation_type_detaille>` pour savoir quelle case il faut cocher), des encadrés en pointillés jaune peuvent apparaître autour de certains champs. 
Si un champ est encadrés en jaune cela signifie que le champ est requis pour transmettre le dossier à Plat'AU.


.. _guichet_unique_nouvelle_demande_recepisse:

=====================
Imprimer un récépissé
=====================

(:menuselection:`Guichet Unique --> Nouvelle demande --> Récépissé`)

Lors de la validation de la nouvelle demande, si l'utilisateur n'a pas eu le
temps d'imprimer le récépissé de demande (car il a changé d'écran un peu
rapidement ou pour toute autre raison), cet écran permet de rechercher la
demande qui vient d'être saisie pour en imprimer le récépissé.

Recherche de la demande
=======================

Un listing de toutes les demandes permet de visualiser les informations
principales. Ce listing est trié par défaut par ordre décroissant de date de
demande donc les demandes du jour doivent apparaître en premier.


Visualisation de la demande et édition du récépissé
===================================================

Un écran récapitule les informations saisies lors de la demande et une action
est disponible dans le portlet d'action contextuelle pour permettre d'éditer le
récépissé.


.. _guichet_unique_nouvelle_demande_petitionnaire_frequent:

Le lien du récépissé de la demande ouvre le document depuis le stockage au format PDF.

===================================
Lister les pétitionnaires fréquents
===================================

(:menuselection:`Guichet Unique --> Nouvelle demande --> Pétitionnaire Fréquent`)

Il est possible d'ajouter un pétitionnaire redondant dans la liste des
pétitionnaires fréquents pour éviter de saisir ses informations à chaque nouvelle entrée,
en cochant le champ "Sauvegarder (pétitionnaire fréquent)" avant de valider.

Pour retrouver un pétitionnaire fréquent, il suffit de taper les trois premières 
lettres de son nom ou de son prénom dans les champs adéquats et de cliquer sur
l'îcone de la loupe "Chercher un pétitionnaire".


.. _guichet_unique_affichage_reglementaire:

L'affichage réglementaire
#########################

Dans les conditions prévues par arrêté du ministre chargé de l'urbanisme, un
affichage au public (aussi appelé registre) de tous les dossiers d'instruction
en cours est obligatoire. Le guichet unique doit pouvoir imprimer une
attestation de cet affichage réglementaire pour un dossier d'instruction
particulier à la demande d'un usager.

.. important::

   Pour l'administrateur : l'événement d'instruction créé sur chaque dossier
   qui permet de générer l'attestation d'affichage doit être paramétré, c'est
   l'identifiant de l'événement en question qui doit être paramétré dans
   l'enregistrement 'id_affichage_obligatoire' depuis l'écran 
   :menuselection:`Administration --> Paramètre`. Si le paramétrage ou
   l'événement n'existe pas alors un message prévient l'utilisateur :
   
   .. image:: m_guichet_unique_affichage_reglementaire_message_erreur_parametrage.png

.. _guichet_unique_affichage_reglementaire_registre:

====================================
Traitement et impression du registre
====================================

(:menuselection:`Guichet Unique --> Affichage Réglementaire --> Registre`)

Le traitement de génération du registre d'affichage réglementaire va :

  * générer le registre que vous devez imprimer et afficher en mairie,
  * créer une instruction d'attestation d'affichage suite au dépôt sur chaque dossier d'instruction en cours,
  * mettre à jour la date d'affichage de chaque dossier d'instruction en cours.


.. note::

  * la date d'affichage est mise à jour uniquement sur les dossiers d'instruction pour lesquels elle n'avait pas été renseignée,
  * l'instruction d'attestation d'affichage suite au dépôt est générée uniquement sur les dossiers d'instruction pour lesquels elle n'existe pas déjà.

.. image:: a_guichet_unique_affichage_reglementaire_registre_formulaire.png


.. _guichet_unique_affichage_reglementaire_attestation:

======================
Imprimer l'attestation
======================

(:menuselection:`Guichet Unique --> Affichage Réglementaire --> Attestation`)

Cet écran permet d'imprimer l'attestation d'affichage réglementaire d'un dossier
d'instruction. Pour le faire, il suffit de saisir le numéro du dossier
d'instruction dans le formulaire puis de cliquer sur le bouton valider.

.. image:: m_guichet_unique_affichage_reglementaire_attestation_formulaire.png

Une fois le formulaire validé, trois cas de figure sont possibles :

* soit l'identifiant saisi ne correspond à aucun dossier d'instruction existant :
  
  .. image:: m_guichet_unique_affichage_reglementaire_attestation_message_dossier_inexistant.png

* soit le dossier d'instruction existe mais ne possède pas d'attestation
  d'affichage :
  
  .. image:: m_guichet_unique_affichage_reglementaire_attestation_message_dossier_jamais_affiche.png

* soit le dossier d'instruction existe et possède une attestation d'affichage,
  on obtient alors un lien vers le fichier pdf de l'attestation permettant de
  l'imprimer :
  
  .. image:: m_guichet_unique_affichage_reglementaire_attestation_message_lien_attestation.png

Le lien de l'attestation d'affichage réglementaire d'un dossier d'instruction ouvre le document depuis le stockage au format PDF.

.. _affichage_date_depot_mairie:

=======================
Date de dépôt en mairie
=======================

Il est possible d'afficher le champ date de dépôt en mairie dans le formulaire d'ajout de la demande
pour cela il faut activer :ref:`l'option d'affichage de la date de dépôt en mairie<administration_liste_options>`. 
Cette date est utilisable dans les règles et est modifiable lors de la modification du dossier d'instruction associé.

