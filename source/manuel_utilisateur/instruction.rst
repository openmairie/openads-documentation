.. _instruction:

###########
Instruction
###########


======================
Dossiers d'instruction
======================

(:menuselection:`Instruction --> Dossiers D'instruction`)

Cette rubrique propose des sous-menus qui ouvrent des tableaux permettant d'accéder
à tout ou une partie (filtre par statut) des dossiers d'instruction :

* les encours de l'utilisateur connecté (liste triée par défaut sur la date limite) ;
* tous les encours (liste triée par défaut sur la date limite) ;
* les clôturés de l'utilisateur connecté ;
* tous les clôturés ;
* tous les dossiers via *Recherche*.

Liste des dossiers d'instruction
================================

(:menuselection:`Instruction --> Recherche`)

La liste de tous les dossiers d'instruction est accessible depuis le menu *Instruction* en cliquant sur l'entrée *Recherche*.

Depuis ce listing, il est possible de rechercher en dossier en utilisant les critères suivant :

  * **Dossier** : libellé ou numéro du dossier.
  * **Type** : type de dossier d'instruction.
  * **Demandeur** : Recherche dans les champs : nom, prénom, raison sociale, dénomination.
    La chaîne recherchée doit figurer dans l'un de ces champs.
    Par exemple, dans le cas d'un demandeur avec le nom 'DUPONT' et le prénom 'JEAN' :

    * la recherche de 'JEAN' donne des résultats car le champ prénom contient 'JEAN',
    * la recherche de 'DUPONT' donne des résultats car le champ nom contient 'DUPONT',
    * la recherche de 'DUPONT JEAN' ne donne aucun résultat car ni le nom ni le prénom ni la raison sociale ni la dénomination ne contient 'DUPONT JEAN'.

  * **Localisation** : Recherche dans les champs numéro, voie, lieu-dit, code postal, localité, boite postale, cedex et dans l'adresse normalisée.
    La chaîne recherchée doit figurer dans l'un de ces champs.
    Par exemple, dans le cas d'une adresse avec la voie 'RUE DU ROUET' et la localité 'MARSEILLE' :

    * la recherche de 'RUE DU ROUET' donne des résultats car le champ voie contient 'RUE DU ROUET',
    * la recherche de 'MARSEILLE' donne des résultats car le champ localité contient 'MARSEILLE',
    * la recherche de 'RUE DU ROUET MARSEILLE' ne donne aucun résultat car ni le numéro ni la voie ni le lieu-dit ni le code postal ni la localité ni la boite postale ni le cedex ne contient 'RUE DU ROUET MARSEILLE'.

    Dans le cas de l'adresse normalisée, la recherche se fait sur la chaîne complète telle que retournée par la BAN. Il est donc conseillé d'utiliser le signe de remplacement * en début de votre recherche.
  * **Parcelle** : identifiant de la parcelle.
  * **État** : état du dossier (ex : notifier commune).
  * **Statut** : statut du dossier (ex : cloturé).
  * **Tacite** : Affiche uniquement les dossiers tacites (Oui) ou non tacite (Non)
  * **Division** : Filtre les dossiers selon la division à laquelle ils sont rattachés.
  * **Source du dépôt** : Filtre les dossiers selon leur source de dépôt (ex : Plat'AU)
  * **Instructeur** : Filtre les dossiers selon l'instructeur choisi.
  * **Instructeur Secondaire** : Filtre les dossiers selon l'instructeur secondaire choisi.
  * **Date de dépôt** : Permet d'afficher les dossiers dont la date de dépôt est comprise dans l'intervalle saisi.
  * **Date de rejet** : Permet d'afficher les dossiers dont la date de rejet est comprise dans l'intervalle saisi.
  * **Date de validité** : Permet d'afficher les dossiers dont la date de validité est comprise dans l'intervalle saisi.
  * **Date de complétude** : Permet d'afficher les dossiers dont la date de complétude est comprise dans l'intervalle saisi.
  * **Date de décision** : Permet d'afficher les dossiers dont la date de décision est comprise dans l'intervalle saisi.
  * **Date limite** : Permet d'afficher les dossiers dont la date limite est comprise dans l'intervalle saisi.
  * **Date chantier** : Permet d'afficher les dossiers dont la date de chantier est comprise dans l'intervalle saisi.
  * **Date achèvement** : Permet d'afficher les dossiers dont la date d'achèvement est comprise dans l'intervalle saisi.
  * **Date conformité** : Permet d'afficher les dossiers dont la date de conformité est comprise dans l'intervalle saisi.
  * **Collectivité** : Permet d'afficher les dossiers de la collectivité choisie.

.. note::
    En fonction de l'activation du mode service consulté certain critère de recherche seront
    accessibles et d'autres non.


En cliquant sur le bouton d'export, il est possible d'exporter la liste des dossiers.
Les données affichées dans l'export sont les suivantes :

 * **dossier** : identifiant du dossier
 * **pétitionnaire**
 * **correspondant**
 * **architecte (nom)**
 * **architecte (cabinet)**
 * **localisation**
 * **nature**
 * **nombre de logements créés**
 * **surface créée**
 * **nature des travaux**
 * **date de dépôt**
 * **date de complétude**
 * **date limite** : date limite d'instruction du dossier.
 * **instructeur** : nom et prénom de l'instructeur affecté au dossier.
 * **division** : division d'affectation du dossier.
 * **état** : état du dossier (ex : notifier commune).
 * **enjeu**
 * **collectivité** : collectivité à laquelle le dossier est rattaché.
 * **dossier plat'au**
 * **consultation plat'au**
 * **pièce(s) plat'au**
 * **autres objets plat'au**
 * **service consultant : identifiant** : (en mode service consulté) identifiant du service consulté pour le dossier.
 * **service consultant : libellé** : (en mode service consulté) libellé du service consulté pour le dossier.


Visualisation des dossiers d'instruction
========================================

Le champ **Délai d'instruction** contiendra toutes les majorations de délai qui ont été ajoutées au dossier d'instruction courant. Ce qui permettra
de déduire la date du champ **Limite d'instruction**. Les unités de temps sont jours et/ou mois.
Exemple : Le dossier d'instruction est déposé le 01/01/2024.
Le délai d'instruction est de 1 mois, la **Limite d'instruction** sera donc le 01/02/2024, ce qui correspond bien à la **Date de dépôt** + 1 mois.
Si pour une quelconque raison, le dossier nécessite d'être complété, et qu'une nouvelle majoration est ajoutée, la valeur du champ **Délai d'instruction** pourra ressembler à ça : **1 mois + 10 jours**. Ainsi, la **Limite d'instruction** sera recalculée et sa valeur sera 11/02/2024, ce qui correspond bien à 01/01/2024 + 1 mois, et 01/02/2024 + 10 jours, on à donc bien la nouvelle date de **Limite d'instruction** au 11/02/2024.

Précision sur le champ "Au terme du délai" dans le fieldset "Suivi" :

.. image:: a_instruction_terme_delai.png

* Si il n'y a pas d'évènement d'accord tacite alors la valeur de ce champ est "N/A" et une infobulle précise qu'aucune instruction ne sera appliquée au terme du délai. Si le dossier est clôturé alors le champ n'est plus affiché. 

Depuis le fieldset des pétitionnaires :

.. image:: m_instruction_dossier_instruction_form_petitionnaires_fieldset.png

* si l'option d'accès au portail citoyen détaillée dans :ref:`cette rubrique <parametrage_parametre>` est activée, le champ **clé d'accès au portail citoyen** affiche le code d'accès nécessaire au pétitionnaire pour accèder à la visualisation de son autorisation depuis le portail citoyen.

.. note::

    Le type d'affichage des dossiers d'instruction est paramétrable depuis le menu **Paramétrage Dossiers > Type DA**.
    L'impact du type d'affichage sur l'affichage des dossiers est décris :ref:`ici<description_type_affichage_dossiers>`.


Représentation des contentieux
==============================

Dans le fieldset **Enjeu** :

.. image:: a_instruction_dossier_instruction_form_enjeu_fieldset.png

* Le champ **contentieux** indique si au moins un dossier en cours **Infraction** ou **Recours** concerne une ou plusieurs parcelles du dossier.
  Le cas échéant le champ contiendra respectivement **RE** en orange et **IN** en rouge.

.. image:: a_instruction_dossier_instruction_form_enjeu_fieldset_tous_cloture.png

* Si tous les **Recours** liés au dossier sont clôturés, la pastille **RE** sera de couleur verte.

* De même si toutes les **Infractions** liées au dossier sont clôturées, la pastille **IN** sera également de couleur verte.


.. _instruction_enjeux:

Représentation des enjeux
=========================

L'ajout d'enjeu se fait en modification depuis le champs enjeu. Il est possible de choisir 1 ou plusieurs enjeux.

.. image:: a_enjeux_add.png

Si un seul enjeu est paramétré sur un dossier l'indicateur suivant apparaît avec la couleur paramétré pour cet enjeu.

.. image:: a_enjeux_triangle.png

Si plusieurs enjeux sont appliqués à un dossier d'instruction, un indicateur montrant la présence de plusieurs enjeu est visible.

.. image:: a_enjeux_double_triangle.png


Indicateur de dépôt électronique
================================

Dans le fieldset **Dossier d'instruction** :

.. image:: a_instruction_dossier_depot_electronique.png

* Devant le champ **demandeur**, la présence du pictogramme de dépôt électronique indique que le dossier a été déposé électroniquement.

.. _transmission_a_platau:

Transmission à Plat'AU
======================

.. image:: m_instruction_dossier_instruction_message_champs_requis_platau.png

Lorsque le type de dossier d'autorisation détaillé est transmissible à Plat'AU (voir :ref:`le paramétrage des types de dossier d'autorisation détaillés<parametrage_dossiers_dossier_autorisation_type_detaille>` pour savoir quelle case il faut cocher) et que les champs requis pour transmettre les informations à Plat'AU ne sont pas totalement saisie, un message de couleur jaune est présent dans la fiche du dossier d'instruction.
Ce message contient un lien cliquable nommé 'Champs requis' qui, si on clique dessus, affiche les champs qui ne sont pas encore saisis sous forme de liste.


Lors de la modification du dossier d'instruction les champs requis pour la transmission à Plat'AU sont encadrés en pointillé jaune.

Tous les types de dossier d'instruction liés au type détaillé de dossier d'autorisation coché comme transmissible, seront transmis également à Plat'AU.
Il existe un paramètre global aux types de dossier d'instruction pour filtrer les dossiers à transmettre : :ref:`*dit_code__to_transmit__platau*<parametrage_parametre_identifiants>`. Ce paramètre est spécifique aux demandes sur existant, il ne s'applique pas aux dossiers d'instruction initiaux.

Natures de travaux
==================

.. image:: a_instruction_dossier_instruction_nature_travaux.png

Lorsque des natures de travaux sont paramétrés pour être compatibles avec le type de dossier d'instruction du DI, il est possible de sélectionner ces nature de travaux afin de les lier au DI.
Le champ de sélection est disponible seulement à la modification du dossier d'instruction.
L'affichage des différentes nature de travaux compatibles avec le di se fait de la manière suivante :

* **famille de travaux** / **nature de travaux**

Pour plus d'informations concernant le paramétrage des familles et natures de travaux, vous pouvez consultez :ref:`le paramétrage des familles de travaux<parametrage_dossiers_famille_travaux>` et :ref:`le paramétrage des natures de travaux<parametrage_dossiers_nature_travaux>`

Indicateur de parcelle temporaire
=================================

Dans le fieldset **Localisation** :

.. image:: a_instruction_dossier_parcelle_temporaire.png

* Le champ **Parcelle temporaire**, indique qu'au moins une parcelle temporaire a été renseignée dans les références cadastrales saisies.

.. _instruction_dossier_portal_code_suivi:

Références cadastrales
======================

Dans le fieldset **Localisation** :

.. image:: a_instruction_dossier_refcad.png

Les références cadastrales peuvent être saisies dans une série de champs dédiés. Par défaut, 3 champs sont visibles : le *Préfixe*, la *Section* et le *Numéro*.
Ces champs suivent la nomenclature de saisie des références cadastrales des CERFA: 

* Dans *Préfixe*: 3 chiffres
* Dans *Section* : 2 lettres
* Dans *Numéro*: 4 chiffres

Au cas où le dossier possède plusieurs références cadastrales ayant le même préfixe et la même section, il est possible de faire une saisie multiple en cliquant sur le bouton **Ajouter d'autres numéros**.

Ce bouton fait apparaitre deux nouveaux champs: 

* Un sélecteur d'opérateur (*jusqu'à* et *et*)
* Un champs Numéro supplémentaire

.. image:: a_instruction_dossier_refcad_ajouter_numeros.png

.. note:: 
    **Les opérateurs** 


    * **Jusqu'à** : Cet opérateur est à utiliser dans le cas de références cadastrales concernant des parcelles contiguës

    *Exemple* : Saisir **[000] [AA] [0001] [jusqu'à] [0005]** équivaut à saisir les références *000AA0001*, *000AA0002*, *000AA0003*, *000AA0004* et *000AA0005*
    
    * **Et** : Cet opérateur est à utiliser dans le cas de références cadastrales concernant des parcelles séparées

    *Exemple* : Saisir **[111] [B] [0012] [et] [0015]** équivaut à saisir les références *111BB0012* et *111BB0015*
    
    .. image:: a_instruction_dossier_refcad_operateurs.png

Dans le cas où plusieurs références cadastrales n'ayant pas les mêmes préfixes et/ou sections doivent être saisies, il est possible de faire apparaitre une nouvelle série de champs Préfixe/Section/Numéro afin de saisir de nouvelles références cadastrales en cliquant sur le bouton **Ajouter d'autres sections**.

.. image:: a_instruction_dossier_refcad_ajouter_sections.png

**Superficie du terrain :** 
Seule la superficie totale du terrain est requise. Elle est l'addition de la superficie des références cadastrales saisie au-dessus. La valeur doit être indiquée en m².

iDE'AU - Codes de suivi
=======================

Liste les codes de suivi des demandes liées au dossier d'instruction. Chaque code est un lien pour accéder au suivi de la demande tel que vu par les demandeurs.

Le lien est paramétrable depuis le paramètre :ref:`portal_code_suivi_base_url<parametrage_parametre_portal>`.

Cette liste n'apparaît que si le dossier d'instruction a au moins un code de suivi de demande enregistré.

.. image:: a_instruction_dossier_portal_code_suivi.png

.. _instruction_acteur_service_consultant:

L'affichage de l'acteur en tant que service consultant
======================================================

Si l'option :ref:`option_module_acteur<parametrage_parametre>` est activée, et qu'un des acteurs du dossier d'instruction est identifié comme *service consultant*, il apparaît dans le fieldset **Dossier d'instruction**.

.. _instruction_definition_non_instruit:

Définition d'un dossier d'instruction non instruit
==================================================

Un dossier d'instruction est définit comme **non instruit** lorsque :

* les instructions appliquées au dossier sont de type *affichage* ;
* la seule instruction appliquée qui ne soit pas de type *affichage*, est le recépissé.

.. _instruction_simulation_taxes:

=======================
La simulation des taxes
=======================

Fonctionnement général
======================

La simulation des taxes permet, pour chaque dossier, d'obtenir une estimation :

* du montant de la Taxe d'Aménagement (TA)
* du montant de la Redevance d'Archéologie Préventive (RAP)

Taxe d'Aménagement
##################

La TA est constituée de trois parties : la part communale, la part départementale et la part régionale. Pour chacune d'entre-elles, deux montants sont proposés : 

* le montant de la part liquidé (calcul de base minoré des abattements et des exonérations) ;
* le montant de la part liquidé avant exonération (calcul de base minoré des abattements, exonérations non-appliquées).

Un total des trois parts avec et sans exonération est également proposé.

    .. important:: La part régionale concerne seulement les collectivités se situant en Île-de-France (case à cocher au niveau du :ref:`paramétrage des taxes<parametrage_taxe_amenagement>`).

Redevance d'Archéologie Préventive
##################################

La RAP se fait sur la globalité de l'autorisation et propose seulement deux montants :

* le montant de la part liquidé (calcul de base minoré des abattements et des exonérations) ;
* le montant de la part liquidé avant exonération (calcul de base minoré des abattements, exonérations non-appliquées).

Calcul
######

Le calcul de base et des abattements se fait automatiquement en fonction les valeurs saisies dans les données techniques (CERFA) du dossier d'instruction. 
En renvanche, le calcul des montants des exonérations ne peut pas être automatisé : les montants doivent être saisis manuellement.

Résultats
#########

Les différents montants issus du simulateur sont présentés sous la forme d'une zone *Simulation de taxes* sur la vue de synthèse du DI.

.. image:: a_instruction_simulation_taxes.png

Restrictions
############

La simulation des taxes est possible seulement si les points suivants sont respectés :

* le :ref:`paramétrage des taxes<parametrage_taxe_amenagement>` pour la collectivité du dossier d'instruction doit être complété ;
* le secteur communal du dossier d'instruction doit être sélectionné ;
* le type d'autorisation du dossier d'instruction doit comporter les données techniques (CERFA) nécessaires au calcul des taxes (pour la :ref:`TA<instruction_simulation_taxes_ta>` et pour la :ref:`RAP<instruction_simulation_taxes_rap>`) ;
* l'utilisateur doit avoir la permission de voir ces informations ;
* l'option :ref:`option_simulation_taxes<parametrage_parametre>` doit être activée sur la collectivité du dossier d'instruction ;

Le non-respect de ses points pourra entrainer des résultats vides ou à zéro, l'absence de la zone *Simulation de taxes* sur la vue de synthèse du DI, etc.

Gestion du *Secteur communal*
#############################

Concernant le secteur communal du dossier d'instruction, celui-ci est sélectionné automatiquement à l'ajout d'une demande si la collectivité possède un seul secteur paramétré (il peut y avoir jusqu'à 20 secteurs par collectivité, voir le :ref:`paramétrage des taxes<parametrage_taxe_amenagement>`). Si plusieurs sont paramétrés, chacun d'entre-eux est sélectionnable lors de la modification du dossier d'instruction : il faut alors le choisir manuellement à la modification du DI.

Champs de fusion
################

Des champs de fusion permettant d'utiliser les montants calculés sont disponibles pour être utilisés dans les éditions.

Le détail est disponible dans l'aide à la saise des lettres-type.

    .. important:: Ce sont les montants avec exonération qui sont récupérés comme champ de fusion dans les éditions.

Déclencheurs et mises à jour
############################

Les montants de la simulation de taxe sont calculés automatiquement :

* à la validation des données techniques (CERFA) ;
* lorsque le secteur communal du dossier d'instruction est modifié.

Dans le cas où le calcul ne peut pas se faire, par manque d'information dans les données techniques (CERFA) par exemple, les valeurs des montants seront vides.
Dans le cas où l'option est activée a posteriori, l'existant n'est pas recalculé automatiquement. Une mise à jour par l'un des deux déclencheurs mentionnés précédement devra être effectuée sur les dossiers souhaités.


.. _instruction_simulation_taxes_ta:

La taxe d'aménagement (TA)
==========================

Le simulateur effectue le calcul de base ainsi que les abattements possibles automatiquement. Les exonérations ne sont pas calculées par l'application, il revient à l'utilisateur d'en saisir le montant dans les données techniques (CERFA).

Source de données pour le calcul de base
########################################

La liste ci-dessous présente les données techniques (CERFA) utilisées pour le calcul de base de la TA :

* **tax_surf_tot_cstr** → Surface taxable totale créée de la ou des construction(s), hormis les surfaces de stationnement closes et couvertes (en m²) ;
* **tax_empl_ten_carav_mobil_nb_cr** → Nombre d’emplacements de tentes, de caravanes et de résidences mobiles de loisirs ;
* **tax_empl_hll_nb_cr** → Nombre d’emplacements pour les habitations légères de loisirs ;
* **tax_sup_bass_pisc_cr** → Superficie du bassin de la piscine ;
* **tax_eol_haut_nb_cr** → Nombre d’éoliennes dont la hauteur est supérieure à 12 m ;
* **tax_pann_volt_sup_cr** → Superficie des panneaux photovoltaïques posés au sol ;
* **tax_am_statio_ext_cr** → Nombre de places de stationnement non couvertes ou non closes.

Abattements
###########

Constructions pour lesquelles un abattement de 50% s'applique sur la valeur forfaitaire :

1. les locaux d'habitation et d'hébergement aidés (hors PLAI ou LLTS) ;
2. les 100 premiers mètres carrés des locaux à usage d'habitation principale ;
3. les locaux à usage industriel et leurs annexes ;
4. les locaux à usage artisanal et leurs annexes ;
5. les entrepôts et hangars non ouverts au public faisant l'objet d'une exploitation commerciale ;
6. les parcs de stationnement couverts faisant l'objet d'une exploitation commerciale.

    .. important:: Les abattements 1 et 2 ne sont pas cumulables d'après l'article L331-12 du code de l'urbanisme (depuis le 1 janvier 2011).

La liste ci-dessous présente les données techniques (CERFA) utilisées pour le calcul des abattements de la TA (pour tous les champs issus d'un tableau, la valeur est récupérée de la colonne *Surfaces créées hormis les surfaces de stationnement closes et couvertes*) :

* **tax_su_princ_surf4** → Tableau "Locaux à usage d’habitation principale et leurs annexes" et ligne "Bénéficiant d'un prêt a taux zéro plus (PTZ+)" ;
* **tax_su_princ_surf3** → Tableau "Locaux à usage d’habitation principale et leurs annexes" et ligne "Bénéficiant d'autres prêts aides (PLUS, LES, PSLA, PLS, LLS)" ;
* **tax_su_heber_surf3** → Tableau "Locaux à usage d’hébergement et leurs annexes" et ligne "Bénéficiant d'autres prêts aidés" ;
* **tax_su_princ_surf1** → Tableau "Locaux à usage d’habitation principale et leurs annexes" et ligne "Ne bénéficiant pas de prêt aidé" ;
* **tax_su_princ_surf2** → Tableau "Locaux à usage d’habitation principale et leurs annexes" et ligne "Bénéficiant d'un PLAI ou LLTS" ;
* **tax_su_non_habit_surf2** → Tableau "Création ou extension de locaux non destinés à l'habitation" et ligne "Locaux industriels et leurs annexes" ;
* **tax_su_non_habit_surf3** → Tableau "Création ou extension de locaux non destinés à l'habitation" et ligne "Locaux artisanaux et leurs annexes" ;
* **tax_su_non_habit_surf4** → Tableau "Création ou extension de locaux non destinés à l'habitation" et ligne "Entrepôts et hangars faisant l'objet d'une exploitation commerciale et non ouverts au public" ;
* **tax_su_parc_statio_expl_comm_surf** → Parcs de stationnement couverts faisant l’objet d’une exploitation commerciale.

    .. important:: Si ces champs ne sont pas renseignés, les abattements ne seront pas pris en compte dans le calcul.

Exonérations
############

La liste ci-dessous présente les données techniques (CERFA) utilisées pour le calcul de l'exonération de la TA :

* **mtn_exo_ta_part_commu** → Montant de l'exonération de la part communale ;
* **mtn_exo_ta_part_depart** → Montant de l'exonération de la part départementale ;
* **mtn_exo_ta_part_reg** → Montant de l'exonération de la part régionale.

Les exonérations de plein droit et facultatives peuvent être sélectionnées depuis les données techniques (CERFA), mais le sont seulement à caractère informatif.

Identification visuelle sur les formulaires
###########################################

Ces données sont accessibles depuis la zone de champ *Déclaration des éléments nécessaires au calcul des impositions* des données techniques (CERFA). Les champs identifiés en rouge correspondent aux données présentées ci-dessus.

.. image:: a_instruction_simulation_taxes_dt_ta.png

Formule de calcul
#################

Détail du calcul de base (noté **A**) de la TA (les valeurs forfaitaires sont récupérées depuis le :ref:`paramétrage des taxes<parametrage_taxe_amenagement>`)) ::

	[tax_surf_tot_cstr] * valeur forfaitaire par surface de construction
	+ [tax_empl_ten_carav_mobil_nb_cr] * valeur forfaitaire par emplacement de tente, caravane ou résidence mobile de loisirs
	+ [tax_empl_hll_nb_cr] * valeur forfaitaire par emplacement d'habitation légère de loisirs
	+ [tax_sup_bass_pisc_cr] * valeur forfaitaire par surface de pisicine
	+ [tax_eol_haut_nb_cr] * valeur forfaitaire par éolienne
	+ [tax_pann_volt_sup_cr] * valeur forfaitaire par surface de panneau photovoltaïque
	+ [tax_am_statio_ext_cr] * valeur forfaitaire par parking extérieur

Détail du calcul des abattements à 50% (noté **B**) de la TA ::

	([tax_su_princ_surf4] + [tax_su_princ_surf3] + [tax_su_heber_surf3]) * (valeur forfaitaire par surface de construction / 2)
	+ (SI [tax_su_princ_surf1] + [tax_su_princ_surf2] > 100 ALORS 100 SINON somme des deux champs) * (valeur forfaitaire par surface de construction / 2)
	+ ([tax_su_non_habit_surf2] * (valeur forfaitaire par surface de construction / 2)
	+ [tax_su_non_habit_surf3] * (valeur forfaitaire par surface de construction / 2)
	+ [tax_su_non_habit_surf4] * (valeur forfaitaire par surface de construction / 2)
	+ [tax_su_parc_statio_expl_comm_surf] * (valeur forfaitaire par surface de construction / 2)


Pour chacune des parts (communale, départementale et régionale) deux résultats sont présentés (les taux sont récupérés depuis le :ref:`paramétrage des taxes<parametrage_taxe_amenagement>`)) :

* le premier résultat est ::

	(A - B) * Taux de le part - le montant de l'exonération de la part

* le second résultat affiche le calcul sans l'exonération.


.. _instruction_simulation_taxes_rap:

La redevance d'archéologie préventive (RAP)
===========================================

La même méthode de calcul que celle de la TA est utilisée pour calculer la RAP.

Source de données pour le calcul de base
########################################

La liste ci-dessous présente les données techniques (CERFA) utilisées pour le calcul de base de la RAP :

* **tax_surf_loc_arch** → Profondeur du(des) terrassement(s) au titre des locaux (en mètre) ;
* **tax_surf_tot_cstr** → Surface taxable totale créée de la ou des construction(s), hormis les surfaces de stationnement closes et couvertes (en m²) ;
* **tax_empl_ten_carav_mobil_nb_arch** → Profondeur du(des) terrassement(s) au titre des emplacements de tentes, de caravanes et de résidences mobiles de loisirs (en mètre) ;
* **tax_empl_ten_carav_mobil_nb_cr** → Nombre d’emplacements de tentes, de caravanes et de résidences mobiles de loisirs ;
* **tax_empl_hll_nb_arch** → Profondeur du(des) terrassement(s) au titre des emplacements pour les habitations légères de loisirs (en mètre) ;
* **tax_empl_hll_nb_cr** → Nombre d’emplacements pour les habitations légères de loisirs ;
* **tax_surf_pisc_arch** → Profondeur du(des) terrassement(s) au titre de la piscine (en mètre) ;
* **tax_sup_bass_pisc_cr** → Superficie du bassin de la piscine ;
* **tax_am_statio_ext_arch** → Profondeur du(des) terrassement(s) au titre des emplacements de stationnement (en mètre) ;
* **tax_am_statio_ext_cr** → Nombre de places de stationnement non couvertes ou non closes.

Abattements
###########

Constructions pour lesquelles un abattement à 50% s'applique sur la valeur forfaitaire pour les 100 premiers m² :

1. local d'habitation constituant une résidence principale ;
2. locaux d'habitation et d'hébergement, ainsi que leurs annexes, édifiés à l'aide d'un prêt locatif à usage social (PLUS), un prêt locatif social (PLS) ou un prêt social de location-accession (PSLA) ;
3. constructions abritant des activités économiques.

    .. important:: Les abattements 1 et 2 ne sont pas cumulables.

La liste ci-dessous présente les données techniques (CERFA) utilisées pour le calcul des abattements de la RAP (pour tous les champs issus d'un tableau la valeur est récupérée de la colonne *Surfaces créées hormis les surfaces de stationnement closes et couvertes*) :

* **tax_su_princ_surf4** → Tableau "Locaux à usage d’habitation principale et leurs annexes" et ligne "Bénéficiant d'un prêt a taux zéro plus (PTZ+)" ;
* **tax_su_princ_surf3** → Tableau "Locaux à usage d’habitation principale et leurs annexes" et ligne "Bénéficiant d'autres prêts aides (PLUS, LES, PSLA, PLS, LLS)" ;
* **tax_su_heber_surf3** → Tableau "Locaux à usage d’hébergement et leurs annexes" et ligne "Bénéficiant d'autres prêts aidés" ;
* **tax_su_princ_surf1** → Tableau "Locaux à usage d’habitation principale et leurs annexes" et ligne "Ne bénéficiant pas de prêt aidé" ;
* **tax_su_princ_surf2** → Tableau "Locaux à usage d’habitation principale et leurs annexes" et ligne "Bénéficiant d'un PLAI ou LLTS" ;
* **tax_su_non_habit_surf2** → Tableau "Création ou extension de locaux non destinés à l'habitation" et ligne "Locaux industriels et leurs annexes" ;
* **tax_su_non_habit_surf3** → Tableau "Création ou extension de locaux non destinés à l'habitation" et ligne "Locaux artisanaux et leurs annexes" ;
* **tax_su_non_habit_surf4** → Tableau "Création ou extension de locaux non destinés à l'habitation" et ligne "Entrepôts et hangars faisant l'objet d'une exploitation commerciale et non ouverts au public" ;
* **tax_su_parc_statio_expl_comm_surf** → Parcs de stationnement couverts faisant l’objet d’une exploitation commerciale.

    .. important:: Si ces champs ne sont pas renseignés, les abattements ne seront pas pris en compte dans le calcul.


Exonérations
############

La liste ci-dessous présente les données techniques (CERFA) utilisées pour le calcul de l'exonération de la RAP :

* **mtn_exo_rap** → Montant de l'exonération.

Les exonérations peuvent être sélectionnées depuis les données techniques (CERFA) mais seulement à caractère informatif.

Identification visuelle sur les formulaires
###########################################

Ces données sont accessibles depuis la zone de champ *Déclaration des éléments nécessaires au calcul des impositions* des données techniques (CERFA). Les champs identifiés en rouge correspondent aux données présentées ci-dessus.

.. image:: a_instruction_simulation_taxes_dt_rap.png


Formule de calcul
#################

Détail du calcul de base (noté **A**) de la RAP (les valeurs forfaitaires sont récupérés depuis le :ref:`paramétrage des taxes<parametrage_taxe_amenagement>`) ::

	SI [tax_surf_loc_arch] > 0.5 ALORS [tax_surf_tot_cstr] * valeur forfaitaire par surface de construction SINON 0
	+ SI [tax_empl_ten_carav_mobil_nb_arch] > 0.5 ALORS [tax_empl_ten_carav_mobil_nb_cr] * valeur forfaitaire par emplacement de tente, caravane ou résidence mobile de loisirs SINON 0
	+ SI [tax_empl_hll_nb_arch] > 0.5 ALORS [tax_empl_hll_nb_cr] * valeur forfaitaire par emplacement d'habitation légère de loisirs SINON 0
	+ SI [tax_surf_pisc_arch] > 0.5 ALORS [tax_sup_bass_pisc_cr] * valeur forfaitaire par surface de pisicine SINON 0
	+ SI [tax_am_statio_ext_arch] > 0.5 ALORS [tax_am_statio_ext_cr] * valeur forfaitaire par parking extérieur SINON 0

Détail du calcul des abattements à 50% (noté **B**) de la RAP, qui ne s'applique que si [tax_empl_ten_carav_mobil_nb_arch] > 0 ::

	(SI [tax_su_princ_surf4] + [tax_su_princ_surf3] + tax_su_heber_surf3 > 100 ALORS 100 SINON somme des trois champs) * (valeur forfaitaire par surface de construction / 2)
	+ (SI [tax_su_princ_surf1] + [tax_su_princ_surf2] > 100 ALORS 100 SINON somme des deux champs) * (valeur forfaitaire par surface de construction / 2)
	+ (SI [tax_su_non_habit_surf2] + [tax_su_non_habit_surf3] + [tax_su_non_habit_surf4] + [tax_su_parc_statio_expl_comm_surf] > 100 ALORS 100 SINON somme des quatre champs) * (valeur forfaitaire par surface de construction / 2)


Deux résultats sont générés (le taux est récupéré depuis le :ref:`paramétrage des taxes<parametrage_taxe_amenagement>`)) :

* le premier résultat est ::

	(A - B) * Taux de le RAP - le montant de l'exonération

* le second résultat affiche le calcul sans l'exonération.


=======
Actions
=======

.. _instruction_action_modifier_date:


Modifier la date de dépôt et la date de dépôt en mairie
=======================================================

Dans le contexte de la modification d'un dossier d'instruction on peut modifier la date de dépôt.

.. image:: a_instruction_action_modifier_date_depot.png

* Disponible s'il n'y a qu'un événement d'instruction sur le dossier et s'il s'agit du récépissé de la demande (les événements de type **affichage** ne sont pas pris en compte).
* La modification s'éffectue uniquement si l'année reste inchangée.
* Si avant la modification, la date du dernier dépôt était celle du dépôt alors sa valeur est aussi modifiée.

Lorsque :ref:`l'option d'affichage de la date de dépôt en mairie<administration_liste_options>` est activée, il est aussi possible de modifier cette date dans le dossier d'instruction sous les mêmes conditions que la date de dépôt.

.. _instruction_action_modifier_date_affichage:

Modifier la date d'affichage
============================

Dans le contexte de la modification d'un dossier d'instruction on peut modifier la date d'affichage à condition d'avoir la permission.

.. image:: m_instruction_action_modifier_date_affichage.png

.. note::
    La date d'affichage, une fois renseignée, n'est plus modifiée par le traitement des événements d'instruction possédant la règle pour changer cette date.


Attestation d'affichage
=======================

.. image:: a_instruction_action_attestation_affichage.png

Cette action est un raccourcis qui permet de consulter le document PDF de la dernière édition finalisée d'affichage règlementaire.


Régénérer le récépissé
======================

Cette action du dossier d'instruction permet de regénèrer l'événement d'instruction du récépissé de la demande et affiche un lien pour le télécharger.

L'action est disponible lorsque :

* le profil de l'utilisateur possède la **permission spécifique** d'utiliser cette action ;
* le dossier d'instruction est considéré comme **:ref:`non instruit<instruction_definition_non_instruit>`** ;

.. _instruction_portlet_rapport_instruction:

Rapport d'instruction
=====================

Le rapport d'instruction est utilisé comme un document de travail par l'instructeur.
Il peut être imprimé à plusieurs étapes de la vie du dossier (présenté à une commission
par exemple).

.. image:: a_instruction_portlet_rapport_instruction.png

Ce document est composé d'un en-tête avec des informations générales sur le dossier, puis des blocs
de texte et de l'option suivante :

* L'analyse réglementaire : ce champ contient le corps de l'analyse de l'instructeur, il est fait pour être mis à jour tout au long de l'instruction.
* La description du projet : cette zone de texte qui est pré-remplie avec la valeur de la nature des travaux.
* Le complément : ce champ de texte permet d'apporter des informations complémentaires, sous la forme d'un texte libre.
* La proposition de décision : une liste à choix de propositions.

Une fois le rapport enregistré, il est possible de sortir une édition PDF contenant ces informations à partir du modèle de l'état *Rapport d'instruction*.

La modification du rapport d'instruction est possible, à tout moment et sans limitation.

.. note::
    La valeur par défaut du champ d'analyse réglementaire est définie dans le paramètre **rapport_instruction_analyse_reglementaire**.
    Les options de proposition disponibles sont choisies dans le paramètre **rapport_instruction_proposition_decision**.
    Il est également possible de sélectionner un modèle qui viendra remplir automatiquement ce champ. Le paramétrage des modèles
    est détaillé :ref:`ici <parametrage_modele_rapport_instruction>`


.. _instruction_portlet_generate_citizen_access_key:

Générer la clé d'accès au portail citoyen
=========================================

.. image:: m_instruction_portlet_generate_citizen_access_key.png

Si l'option d'accès au portail citoyen détaillée dans :ref:`cette rubrique <parametrage_parametre>` n'est pas activée lors de la création du dossier, alors celui-ci n'a pas de clé d'accès au portail citoyen.
Cette action permet de générer une clé d'accès, qui permettra au demandeur de suivre l'avancement de sa demande via le portail citoyen.
Pour que l'action soit disponible il faut que le dossier ne soit pas clôturé, et qu'il ait la même division que celle de l'utilisateur si celui-ci est un instructeur.

.. _instruction_portlet_regenerate_citizen_access_key:

Régénérer la clé d'accès au portail citoyen
===========================================

.. image:: m_instruction_portlet_regenerate_citizen_access_key.png

L'action génère une nouvelle clé d'accès qui écrase l'ancienne, ce qui la rend inutilisable. Cette action n'est disponible que pour les administrateurs et demande une confirmation de la part de l'utilisateur.

.. _instruction_portlet_delete:

Supprimer le dossier d'instruction
==================================

L'action de suppression n'est disponible que sous plusieurs conditions :

* l'option :ref:`option_suppression_dossier_instruction<parametrage_parametre>` est activée
* le profil de l'utilisateur possède la **permission** d'utiliser cette action
* le dossier d'instruction est considéré comme :ref:`non instruit<instruction_definition_non_instruit>`
* si l'option :ref:`option_om_collectivite_entity<parametrage_parametre>` est activée, le dossier d'instruction est la **dernière version** de l'entité (du service consulté)
* si l'option :ref:`option_om_collectivite_entity<parametrage_parametre>` n'est pas activée, le dossier d'instruction est la **dernière version** en cours de l'autorisation
* le dossier n'a pas de notification envoyé
* le dossier est un dossier "papier" (ajouté à partir de l'application)
* le dossier n'est pas géolocalisé, ou le connecteur SIG permet la suppression de la géolocalisation
* dans le cas d'un dossier d'instruction ajouté sur existant, il s'agit de la dernière autorisation de sa **numérotation** (la numérotation se base sur le type du dossier d'autorisation, l'année, le code du département et le code de la commune).

Cette action supprimera le dossier d'instruction ainsi que tous les enregistrements liés.

Si le dossier d'instruction est un dossier initial alors le dossier d'autorisation lié est aussi supprimé, s'il s'agit d'une suppression d'un dossier d'instruction ajouté sur existant, alors le dossier d'autorisation est mis à jour.

.. note::

    Lors de la suppression d'un dossier d'instruction lié à un contentieux, il est nécessaire de supprimer le contentieux en premier lieu.
    
.. note::

    La suppression ne prend pas en compte les potentielles services tiers référencent le dossier d'instruction, voici quelques exemples :
    
    * les fichiers stockés des enregistrements liés ;
    * la référence et les liens de redirection au dossier d'instruction dans le SIG connecté ;
    * il n'y a pas de message transmis au référentiel ERP pour prévenir de la suppression

.. _instruction_portlet_normalize_address:

Normaliser l'adresse
====================

.. image:: a_instruction_portlet_normalize_address.png

L'action ouvre une fenêtre modale depuis laquelle il est possible de saisir l'adresse à normaliser.
Les résultats sont récupérés depuis la BAN (Base Adresse Nationale) grâce à l'`API adresse <https://geo.api.gouv.fr/adresse>`_ et sont présentés sous forme de liste au-dessous du champ de l'adresse saisie.

La sélection d'une de ces adresses remplacera l'adresse saisie, il suffit de valider le formulaire en cliquant sur le bouton **Normaliser l'adresse** pour que celle-ci soit associée au dossier.

.. image:: a_instruction_normalize_address_form.png

Dès qu'une adresse normalisée est associée à un dossier alors c'est celle-ci qui est présentée dans tous les écrans de l'application utilisant cette données, tel que la synthèse du dossier d'instruction ou encore les différents listings des dossiers d'instructions.
Les exports et éditions utiliseront toujours l'adresse non normalisée qui a été saisie sur le dossier d'instruction.

En cas de modification de l'adresse sur le dossier d'instruction, l'adresse normalisée sera supprimée.

.. _instruction_portlet_log_instruction:

Journal d'instruction
=====================

.. image:: a_instruction_portlet_log_instructions.png

L'action redirige vers une nouvelle page sur laquelle figure un tableau d'historique de toutes les entrées du journal d'instruction.

.. image:: a_instruction_log_instructions_table.png

À chaque mise à jour dans l'onglet d'instruction (création, mise à jour, supression d'une instruction), une nouvelle entrée sera ajoutée à ce tableau.
Ce tableau recense :

    * La date de création de la nouvelle entrée du tableau
    * L'identifiant de l'instruction
    * Le contexte de l'action
    * L'utilisateur initiateur de la nouvelle entrée du tableau
    * La date d'évènement
    * La date de notification
    * La date de retour signature
    * L'identifiant de l'évènement de l'instruction
    * L'identifiant de l'action appliqué par l'instruction
    * L'état appliqué au dossier par l'instruction

.. _instruction_parapheur:

Envoi en signature au parapheur
===============================

Cette action permet d'envoyer le document en signature au parapheur.

.. image:: a_instruction_portlet_envoi_en_signature_parapheur.png

Elle est disponible seulement si les conditions suivantes sont remplies :

* Il faut qu'un connecteur parapheur soit configuré
* Le signataire de l'instruction doit avoir une adresse email
* Il faut que le document d'instruction soit finalisé

Si le document (lettre type) qui est envoyé en signature contient un marqueur d'emplacement pour l'incrustation de la signature manuscrite, alors celle-ci apparaitra sur le document signé à l'emplacement prévu. Plus d'information :ref:`ici<parametrage_dossiers_marqueur_signature>`.

Lorsque le document a été envoyé, un nouvel encadré nommé "Suivi Parapheur" apparaît sur l'interface de l'instruction. 
Cet encadré contient le champ "Statut" et un dépliant nommé "Historique".

Lorsqu'on déplie "Historique", un tableau apparaît. Ce tableau contient les différentes étapes du circuit de signature effectuées.

.. image:: a_instruction_fieldset_suivi_parapheur.png

Il existe 4 statuts de parapheur : "en cours de signature", "délai de signature expirée", "signature annulée" et "signé".

Lorsque le document est signé ou que la signature a été annulé, il est possible d'avoir un commentaire supplémentaire. Le commentaire apparaît alors dans le champ "Commentaire" sous le champ "Statut".

La mise à jour du statut de signature se fait automatiquement au moyen d'un webservice.

Si le parapheur renvoie un lien vers le document às signer, il est possible d'afficher ce lien sur le message de validation de l'envoi en activant l'option :ref:`option_afficher_lien_parapheur<administration_liste_options>`.

.. _notification_envoi_signature:

Notification de l'envoi en signature de document
================================================

Si le parapheur le permet, ce dernier, lors de l'envoi en signature du document, envoie un mail de notification pour prévenir le signataire qu'il a un document à signer.

Lorsque le parapheur ne gère pas les notifications ces dernières sont faites par l'application.

.. warning::
    Si le parapheur n'indique pas qui est en charge de la notification (parapheur ou application) alors, il est considéré que c'est le parapheur qui s'en charge. Aucun mail de notification ne sera donc envoyé par l'application.

Le sujet et le contenu du mail des notifications sont modifiables en remplissant les paramètres : param_courriel_notification_signataire_type_titre et param_courriel_notification_signataire_type_message. Le détail de ce paramétrage est présenté dans la partie :ref:`Paramétrage de la notification d'envoi de document en signature<administration_parametrage_notification_signataire>`. Il n'y a que les notifications faites par l'application qui sont paramétrables.

.. warning::
    En cas de notification par l'application, si le mail de notification n'a pas pu être envoyé mais que l'envoi en signature a déjà été effectué, alors l'envoi en signature sera immédiatement annulé.

.. _envoi_signature_relecture_parapheur:

Envoi du document en signature avec relecture sur le parapheur
==============================================================

Pour que cette action soit présente dans l'instruction il faut que le parapheur puisse obliger la relecture du document avant signature, que le connecteur soit configuré afin de permettre cette action  et que les autres conditions mentionnées dans l'action d'envoi en signature soient aussi remplies.

Comme l'action précédente, cette action permet d'envoyer le document en signature au parapheur mais en rendant obligatoire la relecture du document avant signature dans le parapheur.

.. _instruction_annulation_parapheur:

Annuler l'envoi en signature au parapheur
=========================================

Cette action permet d'annuler l'envoi du document en signature au parapheur.

.. image:: a_instruction_portlet_annuler_envoi_en_signature_parapheur.png

Elle est disponible seulement si les conditions suivantes sont remplies :

* Il faut qu'un connecteur parapheur soit configuré
* Dans le fichier de configuration du connecteur le paramètre permettant
  l'annulation du parapheur a été activé
* L'instruction est en cours de signature

Lorsqu'on clique sur l'action d'annulation, l'instruction passe en statut "signature annulée".
Un commentaire est ajouté précisant la date d'annulation et que l'annulation a été faite par l'émetteur.
L'annulation est aussi ajoutée dans la tableau "Historique".


.. _instruction_document_numerise:

Envoi au contrôle de légalité
=============================

Cette action transmet la demande de contrôle de légalité au service compétent par Plat'AU.
La date d'envoi au contrôle de légalité n'est donc plus modifiable manuellement et sera automatiquement renseignée dès que la demande sera traitée par Plat'AU.
En attendant que la demande soit traitée par Plat'AU le texte "En cours de traitement." est affiché dans le champ date d'envoi au contrôle de légalité.
La date de retour du contrôle de légalité est à renseignée manuellement.

L'instruction doit respecter certaines conditions pour que cette cette action soit disponible :

* l'instruction génère un document d'instuction,
* l'événement est paramétré comme :ref:`transmissible au contrôle de légalité par Plat'AU<parametrage_dossiers_evenement>`,
* la date de retour signature est renseignée,
* la date d'envoi au contrôle de légalité n'est pas déjà renseigné,
* le dossier d'instruction est transmissible à Plat'AU,
* l'envoi au contrôle de légalité par Plat'AU n'a pas déjà été réalisé.

.. image:: a_instruction_portlet_envoi_controle_legalite.png

Modification d'un document généré par une instruction
=====================================================

Cette action permet de remplacer le document généré automatiquement par l'instruction par un document numérisé de la version papier signée dans le cas où aucun parapheur n'est équipé.

Un formulaire apparait permettant d'y téléverser le document signé numérisé, ainsi que la date de retour signature.

(Note: l'ajout de la date de retour signature n'est pas obligatoire.)

L'instruction doit respecter certaines conditions pour que cette cette action soit disponible :

* Le document doit être finalisé
* La date de retour signature doit être vide

.. image:: a_instruction_portlet_modification_document_signe.png

.. note::

    * Lors de cette action le dernier document lié à l'instruction est conservé. Son nom de fichier aura comme valeur *[libelle initial]_remplace_le_YYYYMMDDHHmmSS*.
    * Les documents historisés sont affichés dans l'onglet des "Pièces & documents" dans le sous-onglet :ref:`Docs. instruction  <instruction_document_numerise_documents_tab>`  


=============================
Gestion des pièces du dossier
=============================

Chaque dossier d'instruction peut avoir plusieurs documents numérisés.

.. _instruction_document_numerise_tab:

Liste des pièces
================

Les pièces sont listés dans l'onglet "Pièces pétitionnaire" et organisés par date et catégorie.

.. image:: a_instruction_document_numerise_tab.png

Si :ref:`l'option correspondante <parametrage_parametre>` est activée, toutes les pièces ajoutées sont automatiquement cachetées de manière numérique par :

* **N° de permis** : Numéro du permis auquel est rattaché la pièce.
* **N° de dossier** : Numéro du dossier auquel est rattaché la pièce.
* **Date de dépôt** : Date de création de la pièce.

On appelle cela le trouillotage numérique.

Lors du clic sur le nom de la pièce, la pièce s'ouvrira soit, en visualisation dans un nouvel onglet du navigateur, soit en téléchargement direct. Cela dépendra de la configuration de votre navigateur.

Il est possible d'ouvrir le document en visualisation dans une fenêtre modale de l'application, c'est-à-dire sans changer l'onglet du navigateur. Pour cela il suffit de cliquer sur l'action à gauche comportant une loupe.
Seul les images et les pdf sont visualisable, pour les autres types de fichier l'action ne sera pas visible.

.. image:: a_instruction_document_numerise_tab_preview.png

Une vignette de la pièce est également visible au survol de l'action de prévisualisation.

.. image:: a_instruction_document_numerise_tab_vignette.png

Pour ouvrir le formulaire de consultation de la pièce, il suffit de cliquer sur la flèche bleue à gauche ou sur le type du document à droite.
Cette action est disponible seulement pour les utilisateurs ayant les droits dans le contexte d'un dossier d'instruction.

.. _instruction_document_numerise_add:

Ajouter une pièce
##################

Pour ajouter une pièce, il faut cliquer sur la mention "+ Ajouter une pièce".
Seuls les documents au format PDF sont autorisés.

.. image:: a_instruction_document_numerise_form_ajouter.png

Dans le formulaire qui apparaît tous les champs sont obligatoires :

* **Fichier** : Document au format PDF a stocker.
* **Date de création** : Date de création de la pièce. Par défaut, c'est la date de dernier dépôt.
* **Type de pièces** : Type de la pièce.
* **Nature de pièce** : Nature de la pièce.

Seulement les types de pièces étant :ref:`paramétrées <parametrage_document_numerise_type>` comme ajoutables par l'instructeur sont visibles dans cette liste pour les profils instructeur.

.. note::

    Une fois la pièce ajoutée, son type ne sera plus modifiable. En cas d'erreur, il faut donc supprimer la pièce et la re-ajouter.

Seulement les types de pièces sans :ref:`nomenclature <parametrage_document_numerise_nomenclature>` ou ayant une nomenclature pour le type de dossier en cours seront visibles dans cette liste.
Dans cette liste les noms des types de pièces sont affichés avec leur nomenclature associée.

Le type de pièce "Autre à préciser" est toujours visible dans cette liste.
Si ce type de pièce est choisi un champ supplémentaire est saisissable dans le formulaire :

* **Description_type** : Description de la pièce ajoutée qui sera affiché dans le listing des pièces.

Si on ajoute plusieurs fois le même type de pièce avec la même date de création, les noms de fichiers seront suffixés par "-1", "-2", etc.
Exemple : pour 3 ajouts de pièces de type **Arrêté retour préfecture** le 14/09/2016, les noms des pièces seront 20160914ART.pdf, 20160914ART-1.pdf, et 20160914ART-2.pdf.

Si une pièce à une nomenclature associée, c'est ce code de nomenclature qui sera utilisé dans le nom du fichier.
Exemple : Pour l'ajout d'un rescrit fiscal (F2) dans un PC le 14/09/2016, le nom de pièce sera  20160914F2.pdf.
Si il existe plusieurs nomenclature pour un type de dossier et un type de pièce, un seul des codes sera utilisée pour le nom du fichier.

Dans le listing des pièces, les codes des nomenclatures des pièces sont affichés devant leur nom.

.. image:: a_instruction_document_numerise_tab.png

Modifier une pièce
###################

Pour modifier la pièce, il faut cliquer sur l'action "modifier" disponible depuis le formulaire de consultation.
Pour les profils instructeur peuvent requalifier la pièce mais ne peuvent pas modifier le fichier.

La date et le type de la pièce permettant de générer le nom de la pièce, en cas de modification de ceux-ci le nom de la pièce sera régénéré.

Télécharger toutes les pièces
###############################

Il est possible de télécharger l'ensemble des pièces du dossier en cliquant sur le bouton correspondant :

.. image:: a_instruction_document_numerise_btn_telecharger_archive.png

Après avoir cliqué sur le lien un message de confirmation vous demandera de valider votre téléchargement.
Les pièces seront placées dans une archive zip qui sera proposée au téléchargement.

.. image:: a_instruction_document_numerise_lien_telecharger_archive.png

.. note::

    Selon le déploiement de l'application, la création de cette archive peut être longue.
    Si le navigateur et fermé ou que l'utilisateur poursuit la navigation, la génération de l'archve se termine mais l'archive ne sera pas proposée au téléchargement.

.. _instruction_document_numerise_documents_tab:

Liste des documents
===================

Les documents sont listés dans l'onglet "Docs. instruction" et séparé en deux tableaux.

.. image:: a_instruction_documents_instruction_et_travail_tab.png

Le nom des fichiers d'instruction d'évènements auront cette forme :

    * Si dans le paramétrage de l'évènement courant, le type de document est renseigné, alors le nom de fichier ressemblera à ça : **IDDossierInstruction_IDInstruction_Date_LibelleTypeDocument**,
    * sinon il ressemblera plutôt à ça : **IDDossierInstruction_IDInstruction_Date_IDLettreType**.
    * Et si le document est une retour signature, le terme "remplace_le_Date", avec la date comme date de retour du document signé.

    Exemple : **at0130551800001p0_111_20240529_arrete.pdf**, la 1ere valeur étant l'identifiant du dossier, la deuxième, l'identifiant de l'instruction, la 3eme la date d'édition/finalisation, et la dernière soit le libellé du type de document, soit le l'identifiant de la lettre type.
    Ou encore : **at0130551800001p0_111_20240529_arrete_remplace_le_20240529.pdf**, lorsqu'il s'agit d'un document retourné après signature.

Pour la colonne type, on récupère le **type de document** s'il existe, sinon on aura "**N/A**".

Lors du clic sur le nom d'un document d'instruction, le document s'ouvrira soit, en visualisation dans un nouvel onglet du navigateur, soit en téléchargement direct. Cela dépendra de la configuration de votre navigateur.

Il est possible d'ouvrir le document en visualisation dans une fenêtre modale de l'application, c'est-à-dire sans changer l'onglet du navigateur. Pour cela il suffit de cliquer sur l'action à gauche comportant une loupe.

.. image:: a_instruction_document_instruction_tab_preview.png

Les documents de travail sont concernés par le trouillotage numérique comme les :ref:`pièces <instruction_document_numerise_tab>`.

Lors du clic sur le nom d'un document de travail, le document s'ouvrira soit, en visualisation dans un nouvel onglet du navigateur, soit en téléchargement direct. Cela dépendra de la configuration de votre navigateur.

Il est possible d'ouvrir un document de travail en visualisation dans une fenêtre modale de l'application, c'est-à-dire sans changer l'onglet du navigateur. Pour cela il suffit de cliquer sur l'action à gauche comportant une loupe.
Seul les images et les pdf peuvent être visualiser de cette façon.

.. image:: a_instruction_document_travail_tab_preview.png

Pour ouvrir le formulaire de consultation du document de travail, il suffit de cliquer sur la flèche bleue à gauche ou sur la description du document à droite.

.. _instruction_document_travail_add:

Ajouter un document de travail
###############################

Pour ajouter un document de travail, il faut cliquer sur le bouton d'ajout du tableau des documents de travail "+".

.. image:: a_instruction_document_numerise_form_ajouter.png

Dans le formulaire qui apparaît tous les champs sont les suivant :

* **Fichier** : Document à stocker.
* **Date de création** : Date de création du document.
* **Description** : Description du document de travail qui servira à identifier le document.

Modifier un document de travail
###############################

Pour modifier un document de travail, il faut cliquer sur l'action "modifier" disponible depuis le formulaire de consultation.

La date permettant de générer le nom de la pièce, en cas de modification de celle-ci le nom du document sera régénéré.

Télécharger tous les documents d'instruction et de travail
###########################################################

Il est possible de télécharger l'ensemble des documents de travail et d'instruction du dossier en cliquant sur le bouton correspondant :

.. image:: a_instruction_document_numerise_documents_btn_telecharger_archive.png

Après avoir cliqué sur le lien un message de confirmation vous demandera de valider votre téléchargement.
Les documents seront placés dans une archive zip qui sera proposée au téléchargement.

.. image:: a_instruction_document_numerise_documents_lien_telecharger_archive.png

.. note::

    Selon le déploiement de l'application, la création de cette archive peut être longue.
    Si le navigateur et fermé ou que l'utilisateur poursuit la navigation, la génération de l'archve se termine mais l'archive ne sera pas proposée au téléchargement.
    
    Si il n'y a pas de document à ajouter à l'archive ne pourra pas être créée.

.. _instruction_document_numerise_constituer_dossier_final:

Constituer le dossier final
===========================

Un écran dédié permet de manipuler les pièces du dossier afin de rassembler celles qui constituent le dossier final.
Les documents de travail ne sont pas affichés dans cet écran.

Pour y accéder, depuis l'onglet **Pièces & Documents** du dossier d'instruction, cliquer sur le bouton de bascule **Toutes les pièces**.
L'ensemble de toutes les pièces s'affiche, qu'elles aient été jointes ou générées.

.. image:: a_instruction_document_numerise_dossier_final_form.png

Les pièces recommandées sont indiquées en surbrillance. 
Une pièce est identifée comme *recommandée* si elle respecte une des règles suivantes :

* tous les documents déposés via l'onglet **Pièces pétitionnaire** sont recommandés. Dans le cas où il en existe plusieurs du même type, seul le plus récent est recommandé ;
* à l'exception des consultations, tous les documents générés lors de l'instruction du dossier sont recommandés. Dans le cas où il en existe plusieurs du même type, seul le plus récent est recommandé ;
* pour les documents issus des consultations, les pièces des consultations *pour information* ne sont jamais recommandées. Sont recommandés :

    * les pièces jointes aux retours de consultations non tacites ;
    * les documents générés lors d'une demande de consultation tacite.

.. note::

    Le paramètre :ref:`id_avis_consultation_tacite<parametrage_parametre_identifiants>` doit être renseigné pour identifier l'avis de consultation désigant le tacite.

Pour chacune des pièces listées, cliquer sur le nom du fichier permet d'en avoir un aperçu direct.

Il est également possible de visualiser le fichier sans changer l'onglet du navigateur. Pour cela il suffit de cliquer sur l'action à gauche comportant une loupe.

Pour sélectionner les pièces qui vont constituer dossier final, il suffit de les cocher.

Des boutons d'action par lot sont disponibles :

* **Sélectionner les pièces et documents recommandés** ;
* **Tout sélectionner / désélectionner**.

Ces actions influent uniquement sur la sélection des pièces.

Pour enregistrer la sélection, cliquer sur le bouton **Constituer le dossier final**.

Il est possible de télécharger l'intégralité des pièces du dossier final sous la forme d'une archive via le bouton **Télécharger le dossier final**. Ce bouton apparait si le dossier final a déjà été constitué au moins une fois.

Télécharger tous les documents
==============================

Un écran dédié permet de télécharger l'ensemble des documents et pièces du dossier.

.. image:: a_instruction_document_numerise_telechargement.png

Pour y accéder, depuis l’onglet **Pièces & Documents** du dossier d’instruction, cliquer sur le bouton de bascule **Téléchargement**. L’ensemble des documents et pièces s’affichent, qu’elles aient été jointes ou générées.

Le listing est découpé en 3 catégories :

* **Pièces pétitionnaire** : tous les fichiers déposés par le pétitionnaire
* **Documents d'instruction** : tous les fichiers générés ou reçus lors de l’instruction (Lettre du premier mois, PeC, avis, arrêtés, rapport d’instruction, etc).
* **Documents de travail** : tous les fichiers ajoutés depuis le sous onglet Docs. instruction > documents de travail.

Des cases à cocher sont disponibles afin de sélectionner les documents à télécharger.

Il est possible de télécharger l'ensemble des documents et pièces du dossier sélectionnés en cliquant sur le bouton correspondant :

.. image:: a_instruction_document_numerise_telechargement_button.png

Il est également possible de ne télécharger que les documents et/ou pièces sélectionnés préalablement, ou de ne télécharger qu'un document en cliquant directement sur la ligne correspondante.

En cliquant sur le bouton **Télécharger les documents sélectionnés**, un message de confirmation vous demandera de valider votre téléchargement.

.. image:: a_instruction_document_numerise_telechargement_lien_telecharger_archive.png

Les documents et pièces sélectionnés sont placés dans une archive zip et un lien de téléchargement est mis à disposition.

========================
Événements d'instruction
========================

Ajout
=====

.. image:: a_instruction_form_ajout.png

Pour ajouter un évènement d'instruction, il suffit de saisir:

* le type d'évènement
* la date de l'évènement
* (\*) un commentaire
* (\*) le signataire
* (\*) le type de rédaction (*libre* ou par *compléments*)
* (\*) le tiers destinataire (parmis la liste des acteurs du DI)

(\*) Affichés ou non selon le paramétrage de l'évènement.

En fonction des contraintes appliquées au dossier, des événements peuvent être suggérés.
Dans la liste de sélection de l'événement, les événements suggérés sont présentés en premier, dans
un groupe nommé **💡 Suggestions**.

.. image:: a_instruction_suggeree.png

Lors de la validation du formulaire d'ajout, si l'événement sélectionné possède une lettre type associée l'utilisateur est automatiquement redirigé vers le formulaire de modification, dans le cas contraire, l'utilisateur est redirigé vers le listing des évènements.


Modification
============

.. image:: a_instruction_form_edition.png

Événement
#########

* **événement** : sélection de l'événement d'instruction
* **date d'événement** : date de l'événement (date du jour par défaut)
* **Type de document** : affiche le type de document associé à l'évènement courant s'il à été paramétré. Le champ est non modifiable, on récupère la valeur du type de document associé à l'évènement à un moment "T". Pour plus d'information sur le paramétrage, voir :ref:`ici<parametrage_dossiers_saisir_evenement>`.
* **lettre type** : choix de la lettre type affectée à cet événement d'instruction


.. _instruction_complement:

Rédaction par compléments
#########################

Type de rédaction d'une instruction par défaut.

Les champs de complément permettent l'introduction d'un texte personnalisé dans
l'édition.

Ils sont composés d'un éditeur riche permettant une mise en page complexe.

Il est possible d'ajouter des compléments d'informations pour les événements 
d'instruction depuis les blocs "Complément" et "Complément 2".

La plupart des compléments d'informations sont disponibles depuis la bible.

.. image:: m_instruction_complement_bible.png

Il suffit de choisir l'élément que l'on désire voir apparaître dans le champ 
complément.
En laissant la souris sur le libellé une infobulle affichera le texte qui sera 
affiché.

Si les compléments d'informations issus de la bible ont été paramétrés pour être
pré-chargés, ils seront automatiquement intégrés, aux blocs "Complément", lors
de la création de l'événement d'instruction.

(Pour plus d'information sur la bible voir :ref:`parametrage_dossiers_bible`.)

Si l'option **consultation** de l'événement est activée lors de son
:ref:`paramétrage<parametrage_dossiers_saisir_evenement>` alors l'action
**automatique** disponible en bas du formulaire va ajouter les consultations
avec leurs avis (hormis celles marquées explicitement comme non-visibles).

.. note:: Si le mode :ref:`redaction_libre` est activé, les champs *Compléments* sont remplacés par les champs *Titre* et *Corps*


.. _redaction_libre:

Rédaction libre
###############

Si le :ref:`paramètre<parametrage_parametre>` **option_redaction_libre** est
activé pour la collectivité du dossier d'instruction en contexte, alors apparait
la nouvelle action *Rédaction libre*.

.. image:: a_instruction_redaction_libre_bouton_portlet.png

Lorsque la **Rédaction libre** est cliquée/activée, cela permet de modifier
manuellement totalement la lettre type associée (PDF) en utilisant les champs
*Titre* et *Corps* remplaçant alors les champs :ref:`instruction_complement`
(décris ci-dessus).

.. image:: a_instruction_redaction_libre_bouton_modifier_portlet.png

.. image:: a_instruction_redaction_libre_champs_corps.png

Lors du passage en mode **Rédaction libre**, la lettre type actuelle (générée
grâce au modèle de la :ref:`admin_lettre_type`, des :ref:`instruction_complement` et des
champs de fusion), sera entièrement dupliquée dans les champs *Titre* et *Corps*.
Les champs de fusion continueront à être fonctionnels.

Si l'on ne souhaite plus être en **Redaction libre** et revenir à un mode
*"classique"*, il suffit de cliquer sur l'action *Rédaction par compléments*.

.. image:: a_instruction_redaction_libre_bouton_complements_portlet.png

.. warning:: En revenant à une **Rédaction par compléments**, tout le contenu qui aura été écrit manuellement en mode **Rédaction libre** sera perdu.


.. _previsualisation_edition:

Prévisualisation de l'édition
#############################

Si le :ref:`paramètre<parametrage_parametre>` **option_previsualisation_edition**
est activé pour la collectivité du dossier d'instruction en contexte, alors le
rendu du PDF sur le formulaire de modification des événements d'instruction qui
ont une lettre type associée est affiché. Après avoir modifié un complément, on
peut regénérer l'édition en cliquant sur *Prévisualiser*.

.. image:: a_instruction_previsualisation_edition.png

.. note:: Si la fenêtre du navigateur a une **résolution horizontale inférieure à 1280 pixels**, alors **la prévisualisation ne s'affichera pas "à côté"** des champs de modification. Dans ce cas, pour l'afficher il faudra alors cliquer sur le bouton *Prévisualiser*, puis sur le bouton *Rédaction* pour revenir à la rédaction (écran précédent).


Suppression
===========

Il est possible de supprimer le dernier événement d'instruction créé s'il remplit
ces critères :

 - le dossier d'instruction rattaché n'est pas clôturé
 - l'événement d'instruction n'est pas finalisé
 - les dates suivantes ne sont pas renseignées : envoi pour signature, retour de signature, envoi AR, notification, envoi au contrôle légalité, retour du contrôle légalité
 - l'événement lié n’est pas de type « retour »

Notification des demandeurs
===========================

La notification des demandeurs consiste à l'envoi d'un message de notification avec ou sans documents aux pétitionnaires d'un
dossier. Pour accéder à ces documents le pétitionnaire reçoit un lien qui lui permet d'accéder aux documents envoyés et de les télécharger.

Le paramétrage de la notification des demandeurs est décrit :ref:`ici<administration_parametrage_notification_demandeur>`.

Notification automatique
########################

Avec la notification automatique, tous les pétitionnaires ayant une adresse mail et dont la case "accepte les notifications"
a été cochée seront notifiés.

..  note:: Dans le cas, où les notification sont transmises via le portail citoyen seul le pétitionnaire principal
    sera notifié.

Le déclenchement de l'envoi de la notification dependra de la présence ou pas d'une lettretype et
de la notification choisie lors du paramétrage de :ref:`l'évènement<parametrage_dossiers_saisir_evenement>`.
* notification automatique : si il y une lettretype les notifications seront envoyées lors de la finalisation. Sinon elles seront envoyées à l'ajout de l'instruction.
* notification automatique avec signature requise : les notifications seront envoyés lorsqu'une date de retour de signature aura été saisie.

Lorsque les conditions de déclenchement de la notification automatique sont remplies, l'envoi manuel des notifications deviens possible.
Cela permet de renvoyer les notifications en cas d'erreur lors de l'envoi initial.


En cas d'anomalie sur le paramétrage du demandeur principal du dossier - pas de courriel, courriel erroné ou si la case "accepte les notifications" 
n'est pas cochée - des alertes existent pour informer l'utilisateur des problèmes de paramétrage.

Lorsque l'utilisateur accède à l'instruction un message d'information est affiché. Ce message lui indique que la
notification automatique du demandeur principal n'est pas possible et les paramètres qu'il doit corriger.

.. image:: m_msg_information_parametrage_notification.png

Si les conditions d'envoi de la notification automatique ont été remplies mais que le paramétrage du demandeur principal était en erreur,
suite à l'envoi de la notification un message à destination de l'instructeur sera ajouté dans l'onglet message.
Dans le suivi de notification de l'instruction la notification du demandeur est visible et indique les causes de l'erreur.

.. image:: m_suivi_notification_demandeur_en_erreur.png

..  note:: Pour les dossiers déposés via le portail citoyen le paramétrage du demandeur principal n'est pas nécessaire
    (adresse mail et autorisation d'envoi des notifications). Dans ce cas, l'alerte et le message d'information lié
    au paramétrage du demandeur principal ne sont pas affichées / envoyées. L'envoi de la notification ne sera pas en
    erreur dans ce cas.


Notification manuelle
#####################

Avec la notification manuelle, l'utilisateur va pouvoir sélectionner les demandeurs qu'il souhaite notifier.
Pour cela, il devra utiliser l'action "notifier les pétitionnaires".

L'affichage de cette action est géré de la même manière que l'envoi des notifications automatiques :

 * notification manuelle : si il y une lettretype l'action sera visible après la finalisation. Sinon elle sera affichée à l'ajout de l'instruction.
 * notification manuelle avec signature requise : l'action sera affichée lorsqu'une date de retour de signature aura été saisie.


Cette action entraine l'ouverture d'un overlay dans lequel la liste des pétitionnaires notifiables est
affichée. Il faut ensuite cocher les cases des pétitionnaires à notifier et cliquer sur valider pour
envoyer les notifications.

..  note::
    
    Dans le cas, où les notification sont transmises via le portail citoyen, l'affichage
    de l'overlay est remplacé par un message de confirmation. En cliquant sur "confirmer" une
    notification sera transmise au pétitionnaire principal uniquement.

    Si la date limite d'instruction est dépassée depuis plus d'un jour, le message préviendra
    l'utilisateur que la notification est hors délai. Ce message n'est pas bloquant et n'empêche
    pas les utilisateurs de notifier le pétitionnaire s'ils le souhaitent.

Dans le cas d'une notification avec choix d'une annexe, il sera également possible dans ce formulaire
de choisir jusqu'à 5 documents et/ou pièces parmis :

 - les instructions finalisées et signées
 - les retours de consultation
 - les pièces déposées
 - les documents de travail

..  note::

    Pour l'ajout d'une annexe de type **documents de travail**, il faudra que le profil faisant l'ajout ai le droit **document_travail_notifier** associé à son profil.

..  note::

    Le nombre maximum de documents est par défaut défini à 5. Mais il est possible de redéfinir cette valeur dans le
    paramètres de notification des instructions : :ref:`ici<administration_parametrage_notification_demandeur>`.

Ces documents et pièces seront transmis en plus du document d'instruction lors de la notification.

..  note::

    Si le nombre de documents choisis est supérieur à la valeur maximale autorisée, un message informant l'utilisateur qu'il a sélectionné
    trop de document sera visible et lui indiquera le nombre de documents à retirer avant de pouvoir valider
    le formulaire.

.. image:: a_form_saisie_demandeur_notification.png

Pour chaque notifications envoyées, le suivi est affiché dans la partie "Suivi notification" de l'instruction.

.. image:: a_suivi_notification_demandeur.png

..  note::

    Pour les notifications via le portail citoyen, la "date de premier accès" n'étant pas utilisée, elle ne sera
    pas affichée dans le tableau de suivi.


.. _notification_services_instruction:

Notification des services
=========================

La notification des services consiste à l'envoi d'un message de notification avec ou sans documents aux services consultés notifiables.
Pour accéder a ces documents le service recevra un mail avec des liens qui lui permettront d'accéder aux documents notifiés et de les télécharger.

Le paramétrage de la notification est décrit :ref:`ici<administration_parametrage_notification_demandeur>`.

Pour notifier les services consultés :
* La case **Notification des services** doit être cochée, dans le paramétrage de :ref:`l'événement<parametrage_dossiers_saisir_evenement>`, pour que l'instruction soit notifiable.
* Si l'événement d'instruction a une lettretype associée, il faut que cette lettretype soit finalisée.

La notification des services consultés se fait en cliquant sur l'action **Notifier les services consultés**.

En cliquant, sur cette action le formulaire de sélection des services à notifier et des annexes s'ouvre.
A la validation de ce formulaire, un mail de notification sera envoyé à chaque adresses mails renseignées dans les
listes de diffusions des services sélectionnés.
Ces mails contiendront un lien vers le document de l'événement d'instruction (si il y en a un) ainsi qu'un lien par annexes
sélectionnées.

.. image:: m_form_saisie_notification_service.png

Pour chaque notifications envoyées, le suivi est affiché dans la partie "Suivi notification service" de l'instruction.

.. image:: m_suivi_notification_service.png

.. _notification_tiers_instruction:

Notification des tiers
======================

Notification manuelle
#####################

La notification des tiers consiste à l'envoi d'un message de notification avec ou sans documents aux tiers consultés notifiables.
Pour accéder a ces documents le tiers recevra un mail avec des liens qui lui permettront d'accéder aux documents notifiés et de les télécharger.

Le paramétrage de la notification est décrit :ref:`ici<administration_parametrage_notification_demandeur>`.

Pour notifier les tiers consultés :

* La case **Notification des tiers** doit être cochée, dans le paramétrage de :ref:`l'événement<parametrage_dossiers_saisir_evenement>`, pour que l'instruction soit notifiable.
* Si l'événement d'instruction a une lettretype associée, il faut que cette lettretype soit finalisée.

La notification des tiers consultés se fait en cliquant sur l'action **Notifier les tiers consultés**.

En cliquant, sur cette action le formulaire de sélection des tiers à notifier et des annexes s'ouvre.
Si un tiers notifiable est marqué comme service consultant, la mention *(service consultant)* suivra le nom du tiers dans le sélecteur des tiers.
A la validation de ce formulaire, un mail de notification sera envoyé à chaque adresses mails renseignées dans les
listes de diffusions des tiers sélectionnés.
Ces mails contiendront un lien vers le document de l'événement d'instruction (si il y en a un) ainsi qu'un lien par annexes
sélectionnées.

.. image:: m_form_saisie_notification_tiers.png

Quel que soit le mode de notification, pour chaque notifications envoyées, le suivi est affiché dans la partie
"Suivi notification tiers" de l'instruction.

.. image:: m_suivi_notification_tiers.png


Notification automatique
########################

Paramétrage
-----------

L'événement d'instruction peut également être paramétré pour déclencher la notification automatique des tiers.

Pour être déclenchée, la notification automatique se paramètre de la manière suivante :

* Activer l'option :ref:`option_module_acteur<administration_liste_options>`.
* Depuis le menu :ref:`Paramétrage Dossiers > Type DI<parametrage_dossiers_dossier_instruction_type>`, sélectionner les catégories dont les tiers seront automatiquement associés en tant qu'**Acteur** au type de dossier d'instruction voulu.
* Depuis le menu :ref:`Paramétrage Dossiers > Type DI<parametrage_dossiers_dossier_instruction_type>`, sélectionner les catégories dont les tiers pourront être manuellement associés en tant qu'**Acteur** au type de dossier d'instruction voulu.
* Depuis le menu :ref:`Paramétrage Dossiers > Événement<parametrage_dossiers_saisir_evenement>`, pour les événement voulus, choisir *automatique* comme mode de notification des tiers. Sélectionner ensuite les types d'habilitation pour lesquelles les tiers seront notifiables.

.. note::
    Lors de la création d'un dossier d'instruction si celui-ci possède des données de la consultation entrante notamment sur l'identiant du service consultant, il est alors possible d'identifier si l'un des acteurs ajoutés automatiquement est le service consutlant. Si c'est le cas il sera marqué automatiquement comme service consultant.

Gestion des acteurs
-------------------

Une fois la notification paramétrée, il faut sélectionner les acteurs du dossier. Les acteurs sont des tiers liés
à un dossier. Il n'y a que les tiers "acteur" qui seront automatiquement notifiés.

Pour être ajoutés manuellement ou automatiquement en tant qu'acteur, les tiers doivent respecter plusieurs conditions :

  * Leur catégorie doit être paramétrée en ajout "automatique" ou "par l'utilisateur" sur le type de DI du dossier.
  * Leur catégorie doit être associée à la même collectivité que le dossier.
  * Ils doivent avoir une liste de diffusion non vide et accepter les notifications.
  * Ils ne doivent pas avoir d'habilitation en cours de validité
    **OU** avoir une habilitation en cours de validité sans division territoriale
    **OU** avoir une division territoriale associée à la commmune et/ou au département du dossier
  * Un même tiers ne peut pas être plus d'une fois acteur d'un dossier.

Depuis l'onglet *Acteur(s)* du dossier, il est possible d'ajouter, consulter et supprimer les acteurs du dossier.
Les acteurs y sont classés par catégorie. Ces catégories sont triées par ordre alphabétique. Le nom de la catégorie
est affichée au dessus de la liste des acteurs qui lui sont associés.

.. note::
    Les catégories visibles sont celles qui ont été paramétrées pour le type de DI courant et appartenant à la même collectivité que le dossier.
    Si un acteur d'une catégorie a été ajouté à un dossier et que le paramétrage a été modifié pour que cette
    catégorie ne soit plus associée à ce type de dossier, cette catégorie sera toujours visible sur le dossier.
    Il sera également toujours possible d'ajouter, consulter et supprimer des acteurs depuis ce dossier.
    Si tous les acteurs de cette catégorie sont supprimés, elle ne sera plus accessible sur le dossier.

En cliquant sur la croix verte d'une catégorie, on accède au formulaire d'ajout.
Depuis ce formulaire, il est possible de sélectionner un ou plusieurs tiers de cette catégorie.

A la validation du formulaire tous les tiers sélectionnés seront ajoutés en tant qu'acteur du dossier.

En cliquant, sur la croix rouge d'un acteur, il sera supprimé.

En cliquant, sur un acteur, on accède à sa fiche de consultation. 
Il est alors possible de le marquer/démarquer en tant que *service consultant* : cette action retire l'attribution d'un précédent acteur éventuel du dossier déjà marqué comme *service consultant*.
**Un** seul acteur par dossier, peut être marqué comme *service consultant*.

.. image:: m_listing_acteur.png

Déclenchement de la notification
--------------------------------

Le déclenchement de la notification automatique se fait après finalisation du courrier d'instruction.
Suite à la saisie de la date de retour de signature, un mail de notification sera envoyé à tous les tiers notifiables.
Le suivi des notifications sera visible dans la partie "Suivi notification tiers" de l'instruction.

Un tiers est notifiable si :

* Il est acteur du dossier.
* Il a une habilitation dont le type a été sélectionnée lors du :ref:`paramètrage de l’événement<parametrage_dossiers_saisir_evenement>`.
* Il intervient sur la commune ou le département du dossier.
* Il a au moins une adresse mail valide.
* Il n'a pas d'identifiant Plat'au ou ses identifiants Plat'au ne contiennent pas celui du service consulté du dossier.


============
Finalisation
============

Finalisation des documents de l'instruction
===========================================

Le principe
###########

Pour finaliser l'édition de l'instruction, il faut cliquer sur le lien "Finaliser le document" du portlet d'actions contextuelles de la visualisation.

.. image:: m_portlet_finaliser.png

Au clic sur le lien de l'édition dans le portlet d'actions contextuelles de la visualisation de l'instruction, le document sera ouvert depuis le stockage au format PDF.

L'instruction n'est plus ni modifiable, ni supprimable.

Il est aussi possible de dé-finaliser le document au clic sur le lien "Reprendre la rédaction du document".

.. image:: a_instruction_portlet_reprendre_instruction.png

Lorsque le document est finalisé certaines informations concernant le dossier
lui sont associées lors de l'enregistrement.

Le clic sur le lien de l'édition dans le portlet d'actions contextuelles de la visualisation de l'instruction ouvrira le document généré à la volée au format PDF.

L'instruction est à nouveau modifiable et supprimable.

L'action peut être non disponible en fonction de certaines conditions, par exemple :

* evidemment si le document n'est pas déjà finalisé
* pour les utilisateurs n'ayant pas la permission adéquate
* lorsque le dossier d'instruction est clôturé (excepté pour un utilisateur ayant la permission adéquate)
* lorsque le document est signé (excepté pour un utilisateur ayant la permission adéquate)

.. note::

    Une instruction peut être finalisée automatiquement lors de son ajout si celle-ci est paramétrée ainsi (voir le :ref:`paramétrage des événements<parametrage_dossiers_evenement>`) ou si :ref:`l'option de finalisation automatique des retours et tacites<parametrage_parametre>` est activée.

La mise à jour des dates de suivi depuis l'instruction
######################################################

Les dates de suivi n'étant pas affichées dans le document PDF de l'instruction, elles sont modifiables une fois l'instruction finalisée. Il faut pour cela cliquer sur le bouton du portlet d'actions contextuelles "Suivi des dates".

.. note::
    Le suivi des dates n'est disponible que si l'événement d'instruction a une édition associée.
    Si ce n'est pas le cas, les dates ne seront pas affiché lors de la consultation de l'instruction.

.. image:: a_instruction_portlet_mise_a_jour_des_dates.png

On arrive alors sur la page suivante où seules les dates de suivi sont modifiables.

.. image:: a_instruction_form_mise_a_jour_des_dates.png

* **date de finalisation du courrier**
* **date d'envoi pour signature**
* **date d'envoi AR**
* **date d'envoi au contrôle légalité**
* **signataire** (on peut en sélectionner un par défaut, cf. `Paramétrage --> Organisation --> Signataire Arrêté`)
* **date de retour de signature**
* **date de notification**
* **date de retour du contrôle de légalité**


.. note::

  Pour avoir accès à cette action :
   - si on est instructeur, soit être celui du dossier ou tout au moins de sa division, soit être un instructeur polyvalent de la commune du dossier dont l'instruction a été déléguée à la communauté ;
   - sinon être soit de la communauté (par exemple un administrateur), soit de la commune du dossier (par exemple le profil *GUICHET SUIVI*) ;
   - les instructeurs ont besoin de la permission spécifique *instruction_modification_dates_cloture* pour continuer le suivi des dates sur un dossier clôturé. Cette permission est octroyée par défaut aux profils :ref:`INSTRUCTEUR POLYVALENT<profil_instructeur_polyvalent>` et :ref:`INSTRUCTEUR POLYVALENT COMMUNE<profil_instructeur_polyvalent_commune>`.

Finalisation des documents du rapport d'instruction
===================================================

Pour finaliser l'édition du rapport d'instruction, il faut cliquer sur le lien "Finaliser le document" du portlet d'actions contextuelles de la visualisation.

.. image:: m_portlet_finaliser.png

Lorsque le document est finalisé certaines informations concernant le dossier
lui sont associées lors de l'enregistrement.

Au clic sur le lien de l'édition dans le portlet d'actions contextuelles de la visualisation du rapport d'instruction, le document sera ouvert depuis le stockage au format PDF.

Le rapport d'instruction n'est plus ni modifiable, ni supprimable.

Il est aussi possible de dé-finaliser le document en cliquant sur le lien "Reprendre la rédaction du document".

.. image:: m_portlet_definaliser.png

Le clic sur le lien de l'édition dans le portlet d'actions contextuelles de la visualisation du rapport d'instruction ouvrira le document généré à la volée au format PDF.

Le rapport d'instruction est à nouveau modifiable et supprimable.

Historisation des documents du rapport d'instruction
====================================================

A chaque finalisation du rapport d'instruction, une version est historisée.
A chaque reprise d'édition du rapport, une nouvelle version est créée.

Pour les utilisateurs ayant les permissions nécessaires, les versions historisées sont visibles dans le tableau "Historique des versions".

Depuis ce tableau, il est possible de consulter les rapports historisés et de les télécharger (selon les permissions de l'utilisateur).

.. image:: a_tab_histo_ri.png

Finalisation des documents de la consultation
=============================================

Pour finaliser l'édition de la consultation, il faut cliquer sur le lien "Finaliser le document" du portlet d'actions contextuelles de la visualisation.

.. image:: m_portlet_finaliser_consultation.png

Lorsque le document est finalisé certaines informations concernant le dossier
lui sont associées lors de l'enregistrement.

Au clic sur le lien de l'édition dans le portlet d'actions contextuelles de la visualisation 
de la consultation, le document sera ouvert depuis le stockage au format PDF.

La consultation n'est plus supprimable.

Il est aussi possible de dé-finaliser le document en cliquant sur le lien "Reprendre la rédaction du document".

.. image:: m_portlet_definaliser.png

Le clic sur le lien de l'édition dans le portlet d'actions contextuelles de la visualisation 
de la consultation ouvrira le document généré à la volée au format PDF.

La consultation est à nouveau supprimable.


Notifier la commune par courriel
================================

Un événement d'instruction est notifiable par courriel aux communes.

.. image:: a_notifier_commune.png

Les quatre conditions suivantes doivent être satisfaites pour rendre l'action disponible :

* :ref:`paramétrage <administration_parametrage_notification_commune>` renseigné ;
* événement d'instruction finalisé ;
* être rattaché à la communauté de communes ;
* disposer du profil instructeur polyvalent ou administrateur général.

Une fois la commune notifiée, le suivi des notifications est visible sur l'instruction.

.. image:: a_suivi_notification_commune.png

Notification automatique de dépôt de dossiers dématérialisés
############################################################

Lorsque l'option :ref:`'option_notification_depot_demat' <parametrage_parametre>` est active, le dépôt d'un dossier dématérialisé
entraine l'envoi d'un courriel de notification aux communes.

Une fois la commune notifiée, le suivi de la notification est visible sur l'instruction de recepisse du dossier.

Le paramétrage du courriel de notification est détaillé :ref:`'ici' <administration_parametrage_notification_depot_demat>`

.. _instruction_dossier_contrainte:

=============================
Contraintes liées au dossier
=============================

Des contraintes (ou risques) peuvent être ajoutées à un dossier.

.. _instruction_dossier_contrainte_view:

Visualisation des contraintes liées au dossier
===============================================

Les contraintes affichées dans le tableau de données sont groupées par groupe et
sous-groupe et sont classées par le numéro d'ordre d'affichage.

Chaque contrainte possède un bouton raccourci pour ouvrir le formulaire en 
modification et un autre en mode suppression.
Seulement le champ **texte complété** est modifiable.

Il est également possible de sélectionner les contraintes à conserver et de supprimer les autres
en cliquant sur le bouton "Conserver les contraintes sélectionnées".
Pour sélectionner les contraintes a conserver, il suffit de les cocher. 

Une case à cocher nommée "Tout sélectionner / désélectionner" permet de sélectionner toutes 
les contraintes ou de les déselectionner. Chaque groupe contient aussi une case à cocher nommée
"Tout sélectionner / désélectionner dans le groupe" qui permet de sélectionner ou de désélectionner
toutes les contraintes d'un groupe.

Certaines contraintes ont des suggestions associées (Voir :ref:`Paramétrage des suggestions liées aux contraintes.<parametrage_suggestion_contrainte>`).
Elles sont identifiées par la présence de l'icône 💡 devant leur libellé. Au survol de cette icône la liste des suggestions est affichée.
Si des contraintes ayant des suggestions sont présentes, un message indiquant leur présence est affiché au
dessus du tableau.

.. image:: a_instruction_dossier_contrainte_view.png

.. _instruction_dossier_contrainte_add_man:

Ajouter des contraintes manuellement
====================================

En cliquant sur le bouton **Ajouter des contraintes**, un formulaire présentant
toutes les contraintes de l'application apparaît.

Les contraintes sont triées comme dans le tableau de données, par groupe, sous-groupe et par ordre d'affichage. Par défaut chaque groupe et sous-groupe est
replié.

Il suffit de cliquer sur un contrainte pour la sélectionner et de valider le
formulaire pour que celle-ci soit ajoutée au dossier. Un message de validation 
apparait.

.. image:: a_instruction_dossier_contrainte_form.png

.. image:: a_instruction_dossier_contrainte_form_valide.png

Les contraintes peuvent aussi être récupérées automatiquement à partir d'un SIG si
celui-ci est configuré, (voir :ref:`instruction_geolocalisation` ).

===========
Commissions
===========

L'onglet **Commission(s)** permet de lister et consulter les demandes de passage en commissions.

Si on est instructeur d'une collectivité de niveau mono, seuls les types de commissions rattachés à notre collectivité sont affichés.

Si on est instructeur d'une collectivité de niveau multi, tous les types de commissions sont affichés.

====
Lots
====

L'onglet **Lot(s)** permet de lister et consulter tous les lots du dossier d'instruction. Ces lots sont créés manuellement pas l'instructeur sur un permis valant division.
La gestion des lots permet lors d'une demande de transfert partiel d'affecter le ou les pétitionnaire à un ou plusieurs lots.

L'instructeur du dossier peut :

- ajouter des lots
- modifier des lots
- supprimer des lots
- éditer les données techniques (CERFA) des lots
- tranférer le ou les pétitionnaire à un ou plusieurs lots

.. _instruction_dossier_message:

========
Messages
========

.. image:: a_instruction_dossier_message_tab.png

L'onglet **Message(s)** permet d'ajouter, lister et consulter tous les messages du dossier.

Ajouter un message automatiquement
==================================

Les messages automatiquement ajoutés se font suite à des actions spécifiques, comme par exemple l'ajout de pièce numérisée, à condition que l'option :ref:`'option_notification_piece_numerisee' <parametrage_parametre>` soit activée.
Lorsqu'une action notifiée est réalisée par un utilisateur différent de l'instructeur du dossier, alors le message de notification sera destiné à l'instructeur.
Si cette action est réalisée par l'instructeur du dossier et que celui-ci fait partie de la même collectivité que celle du dossier, alors il n'y a pas besoin de message de notification.
Dernier cas, si l'action est réalisée par l'instructeur du dossier et celui-ci n'est pas de la même collectivité que celle du dossier, alors le message de notification sera destinée à la collectivité du dossier.
Dans certains cas tel que celui de l'ajout des pièces, pour éviter de multiplier les notifications, ne seront pas ajoutés les messages traitant d'une même action à la même date et dont le destinataire est identique à un message déjà existant et non lu.

Ajouter un message manuellement
===============================

L'ajout d'un message manuel peut se faire par un utilisateur en ayant le droit. 
Dans ce cas, la notification est gérée de la même manière que pour un message automatique. 
La présence d'un message manuel sur un dossier est indiquée dans la colonne *message* du listing associé au widget des derniers dossiers déposés, suivant le paramétrage de celui-ci (voir :ref:`ici<administration_widget_derniers_dossiers_deposes>`)

.. image:: m_instruction_dossier_message_form_ajouter.png

Marquer comme lu / non lu
=========================

Une action disponible depuis son formulaire de consultation permet de marquer un message comme lu :

.. image:: a_instruction_dossier_message_form.png

Les messages marqués comme 'non lu' sont listés dans les tableaux du menu *Instruction* > *Messages* :

* *Mes Messages*
* *Messages De Ma Division*
* *Tous Les Messages*

Un clic sur une ligne de résultat redirige directement vers le message non lu dans le contexte du dossier d'instruction.

.. note::

  Certains messages sont susceptibles d'être accompagnés d'une édition. Lorsque c'est le cas une action spécifique est disponible depuis le portlet d'actions contextuelles. Ci-après leur liste avec leur message correspondant :

  * *Accusé de réception* pour :ref:`[213](Échange ERP → ADS) Dossier PC Accusé de réception de consultation officielle<echange_erp_ads_213>`.

=============
Sous dossiers
=============

L'onglet **Sous Dossier** permet d'ajouter, lister et consulter des sous dossiers.
Cet onglet n'est accessible que si le :ref:`mode service consulté<administration_liste_options>` est actif.

Le fonctionnement des sous dossiers est le meme que celui des dossiers. Cependant ils ne
sont pas accessible ni depuis les listings de dossiers ni depuis les widgets. Ils ne sont
également pas visible dans les exports. Le seul moyen d'accéder à un sous dossier est
de passer le dossier parent auquel il est lié.

Si l'option :ref:`option_module_acteur<parametrage_parametre>` est activée et qu'un *service consultant* est défini sur le dossier parent du sous-dossier, il apparaît sur le formulaire du sous-dossier.

.. image:: a_instruction_sous_dossier.png

Ajouter un sous dossier
=======================

La liste des sous dossiers est visible depuis l'onglet **Sous Dossier**.
Pour chaque type de sous dossier compatible avec le dossier, un tableau spécifique
est affiché.

.. note::

    Si il n'existe aucun type de sous dossier compatible avec le type de dossier en cours un
    message d'information est affiché.


Il est possible d'ajouter un sous dossier en cliquant sur la croix verte du tableau de
sous dossier voulu. Cela déclenche automatiquement la création du sous dossier et le traitement
d'affectation automatique sans passer par un formulaire de paramétrage. Les informations
nécessaires à la création du sous dossier sont récupérés de son paramétrage ou issues du
dossier parent.

Les éléments copiés du dossier parent sont :

- la localisation
- la liste des demandeurs
- l'instructeur et la division si l'affectation automatique du sous dossier n'est pas définie

.. note::

    Si un type de sous dossier est compatible mais mal paramétré (Pas de type de demande
    ou alors plusieurs type de demande pour ce type de sous dossier),
    son tableau est visible mais il ne peut pas être ajouté. Dans ce cas, un messaga à
    l'attention de l'utilisateur est visible et lui donne la liste des sous dossiers à
    corriger.


À la création du sous dossier, l'utilisateur est automatiquement redirigé vers la page
de consultation du sous dossier et peut démarrer son intruction.

.. image:: a_instruction_consultation_sous_dossier.png


Consulter un sous dossier
=========================

Les tableaux de sous dossier sont organisés par type de sous dossier. Pour consulter un
sous dossiers, il faut regarder dans le tableau du type voulu et sélectionner le sous
dossier à consulter.

Une fois la consultation terminé, il suffit de cliquer sur le bouton de retour pour se
retrouver sur le dossier parent dans son contexte d’origine.


=============
Dossiers liés
=============

.. image:: a_instruction_dossiers_lies.png

L'onglet **Dossiers liés** permet d'obtenir tous les dossiers liés au dossier d'instruction courant.
Il existe quatre types de liaisons entre les dossiers pour quatre listings différents. Dans l'ordre :

    * dossier d'autorisation ;
    * dossiers d'instruction liés manuellement ou implicitement par le dossier d'autorisation ;
    * dossiers sur lesquels un lien vers le dossier courant a été établi ;
    * dossiers d'autorisation liés géographiquement, c'est-à-dire ayant au moins une parcelle commune.

.. note::
    Des dossiers auxquels vous n'avez pas accès sont susceptibles d'apparaître à titre indicatif dans les différents listings. Si vous essayez de les consulter en cliquant dessus alors vous rencontrerez une erreur de droits insuffisants.

Il est possible depuis l'action d'ajout "+" dans le tableau des dossiers d'instruction liés, d'ajouter des liaisons avec d'autres dossiers d'instruction.
Il n'est pas possible de lier le dossier d'instruction courant deux fois à un même DI ou de le lier manuellement à un DI déjà lié implicitement par le dossier d'autorisation.

.. image:: a_instruction_dossiers_lies_form_ajout.png

Les liaisons manuelles peuvent être supprimées seulement depuis le tableau **Dossiers liés** grâce à l'action de suppression "X" disponible sur chaque ligne. Par la même occasion, le dossier courant n'apparaîtra plus dans le tableau des liaisons retour du dossier pour lequel on a supprimé la liaison.
Les liaisons retours ne peuvent pas être supprimées depuis le dossier cible. Il faut le faire depuis le dossier source.

.. note::
    Dans le cas des recours (contentieux), il existe une notion de liaison principale avec un dossier d'instruction.
    Cette liaison n'est modifiable que par les profils ayant une permission spécifique.


.. _instruction_geolocalisation:

==================
La géolocalisation
==================

L'action Géolocalisation est disponible seulement pour les communes paramétrées. Elle 
permet, pour les dossiers qui ont des références cadastrales renseignées, de récupérer des
données géographiques à partir du SIG paramétré.

Lorsqu'un dossier est géolocalisé, un lien vers le SIG est présent dans le cadre principal du dossier
d'instruction sous forme d'îcone de planète suivie des coordonnées du centroïde dans le SIG.

.. image:: m_instruction_dossier_geolocalisation.png

Si l'option **option_streetview** est spécifiée alors un lien vers *Google Maps Street View* sera affiché dans un champ **streetview**.
Il est possible d'afficher ce lien que le dossier soit géolocalisé ou pas.


Pour géolocaliser un dossier, ouvrir la fenêtre modale de géolocalisation en cliquant sur le bouton
"Géolocalisation" sur l'onglet principal du DI.

.. image:: m_instruction_portlet_geolocalisation.png

La fenêtre modale de géolocalisation se présente comme suit:

.. image:: m_instruction_geolocalisation_view.png


Vérifier les parcelles
======================

Cette action permet de vérifier si les parcelles définies dans le dossier existent au
niveau du SIG. Cette étape est nécessaire a l'exécution des autres traitements.

Calculer l'emprise
==================

L'emprise est le total de la surface des parcelles. Le calcul de l'emprise est requis pour
pouvoir calculer le centroïde des parcelles.

Dessiner l'emprise
==================

Dans le cas où le calcul de l'emprise a échoué du côté du SIG, cette action permet d'être
redirigé sur le SIG, sur lequel il est alors possible de dessiner l'emprise à la main.

Calculer le centroïde
=====================

Le centroïde est le point représentatif de l'emprise calculée précedement. Il est ensuite
récupéré et stocké dans le dossier d'instruction.
Si le SIG fourni l'information, la surface totale de l'emprise est récupérée et stockée dans
le champ 'Superficie calculée' du dossier d'instruction.
Également si le dossier a été géolocalisé manuellement avec succès et qu'aucune référence
cadastrale n'a été saisie dans le dossier, alors celles-ci sont récupérées depuis le SIG,
lorsque celui-ci permet d'obtenir cette information.

Récupérer les contraintes
=========================

Cette action permet de récupérer les contraintes du SIG qui sont applicables aux parcelles
du dossier. Ces contraintes peuvent appartenir à la communauté de communes aussi bien qu'à
la commune.

L'action "J'ai de la chance"
============================

Ce bouton permet un lancement automatique, à la chaine, de toutes les actions de
géolocalisation d'un dossier décrites précedement. Il permet de gagner du temps.


.. _instruction_designation_operateur:

=====================
Désigner un opérateur
=====================

En archéologie préventive, dès lors que la prise en compte positive est effectuée, le SRA a
la possibilité de prendre plusieurs décisions sur un seul dossier chaque décision est une
opération ayant son propre déroulé (workflow). Une personne doit être désigné pour prendre 
en charge ces opérations, cette personne est un opérateur.

Pour avoir accès à l'action désignation d'opérateur intervenant sur le dossier d'instruction, il faut que l'application soit en mode service consulté (*option_mode_service_consulte*), que l'option permettant de lier une commune au dossier soit activée (*option_dossier_commune*), que le paramètre *param_operateur* soit ajouté (voir la partie :ref:`paramétrage de la gestion des opérateurs<parametrage_gestion_des_operateurs>`) et que la désignation de l'opérateur soit paramétré sur l'évènement d'instruction (voir la partie :ref:`paramétrage de l'évènement d'instruction<parametrage_dossiers_saisir_evenement>`).

Lorsqu'on clique sur l'action de désignation des opérateurs, un overlay s'affiche avec l'action "Rechercher opérateur". Cette action permet de rechercher des opérateurs en fonction des cas présents dans *param_operateur* et des tiers consultés présents dans l'application.

Il y a deux grands types d'opérateur :

* l'opérateur INRAP (il ne peut y en avoir qu'un seul)
* les opérateurs "Collectivité Territoriale"

Concernant les opérateurs "Collectivité Territoriale" il y a deux types d'habilitation :

* l'habilitation tout diagnostique
* l'habilitation au cas par cas

Pour qu'un opérateur soit sélectionné, un certain nombre de conditions doit être remplies en fonction des cas paramétrés dans *param_operateur*.

Lors de la recherche, les opérateurs trouvés sont basés sur les identifiants communiqués dans chaque attribut du *param_operateur*. Voici la liste de ces attribut :

"categorie_tiers_inrap", "categorie_tiers_collterr", "categorie_tiers_amenageur_public", "type_habilitations_operateurs_diag_kpark", "type_habilitations_operateurs_diag_toutdiag", "type_habilitations_operateurs_inrap"

Explication des paramètres :

Chaque paramètre présent dans les cas de *param_operateur* correspondent à un champs du formulaire de désignation de l'opérateur.

* P2 : Correspond au champ Opérateur qui est un tableau contenant les opérateurs "Collectivité Territoriale"
* P3 : Si dans le tableau des opérateurs "Collectivité Territoriale" il y a un opérateur au cas par cas alors la valeur est 'kpark' sinon la valeur est 'tout_diag'
* P4 : Correspond au champ "L'aménagement est-il réalisé par ou pour une personne publique ?" (true pour Oui et false pour Non)
* P5 : Correspond au champ "La personne publique est-elle l'aménageur du projet ?" (true pour Oui et false pour Non)
* P6 : Avis rendu par le tiers consulté "Collectivité Territoriale" avec l'habilitation au cas par cas
* P7 : Avis rendu par le tiers consulté Personne Publique

P6 est déterminé en fonction des informations saisies dans le tableau des opérateurs "Collectivité Territoriale". Si il y a des opérateur au cas par cas alors :

* Si tous les avis sont rendus, on sélectionne en priorité l'opérateur qui agit sur la commune, si il n'y en a pas alors on sélectionne l'opérateur qui agit sur le département.
* Si les avis sont rendus partiellement, si un opérateur qui agit sur la commune a délivré un avis "Favorable" alors il est sélectionné.

Deux exemples (un simple et un complexe) :

- Exemple 1

Si dans le *param_operateur* il y a ce cas :

.. code-block:: json

    {
        "libelle" : "CAS A",
        "parametre" : {
            "P2" : null
        },
        "type_operateur" : "inrap",
        "evenement" : 412
    }

Si lors de la recherche on trouve seulement un opérateur inrap et pas d'opérateur "Collectivité Territoriale" (P2), c'est l'opérateur inrap trouvé qui est sélectionné.


- Exemple 2

Si dans le *param_operateur* il y a ce cas :

.. code-block:: json

    {
        "libelle" : "CAS D2",
        "parametre" : {
            "P2" : "collterr",
            "P3" : "tout_diag",
            "P4" : true,
            "P5" : true,
            "P7" : "F"
        },
        "type_operateur" : "collterr",
        "evenement" : 412
    }

Dans ce cas l'opérateur sélectionné sera de type "Collectivité Territoriale" tout diagnostique. L'aménagement est réalisé par ou pour une personne publique. La personne publique est l'aménageur du projet. La personne public a rendu un avis "Favorable".

Une fois la recherche initialisée, une action "Modifier" est disponible pour saisir les différents champs, à chaque validation du formulaire la recherche en fonction des cas est relancée. Si un opérateur est trouvé il est ajouté dans l'encadré "Opérateur Désigné".

L'action "Réinitialiser" permet d'effacer le contenu du formulaire afin de relancer un opérateur.

L'action "Valider" permet de valider l'opérateur, cette action met à jour un tableau permettant d'avoir un historique des opérateurs validés.
Si un évènement est paramétré dans le cas de *param_operateur* qui est sélectionné et que cet évènement existe alors il est ajouté au dossier d'instruction.

=============
Consultations
=============

.. image:: a_instruction_dossier_consultation_tab.png

L'onglet **Consultation(s)** permet de lister et consulter toutes les consultations du dossier
d'instruction. Les consultations de type *pour conformité* sont surlignées en jaune.

Consultation de service par voie dématérialisée
===============================================

Si le dossier est :ref:`transmissible à Plat'AU<transmission_a_platau>`, il est possible de faire des consultations dématérialisées.
Pour cela, dans le formulaire d'ajout d'une consultation, sélectionner un service de type platau, remplir le formulaire et cliquer sur valider.

L'ajout de cette consultation va entrainer la création d'une "tâche" qui servira à faire le transfert vers le service consulté via Plat'AU.
Une fois la consultation transférée, le service consulté pourra rendre son avis ce qui mettra automatiquement à jour la consultation.

Le suivi du transfert de la consultation est visible sur la synthèse du dossier.

.. note::
    Uniquement, les consultations dématérialisées dont le transfert n'a pas encore été fait (tâche à l'état "à traiter") peuvent être supprimées.
    Si une consultation dématérialisées et non transférée est supprimée cela entraîne l'annulation de sa tâche de transfert.


Modifier la visibilité d'une consultation dans les éditions
===========================================================

Il est possible de masquer une consultation dans les éditions qui y font référence,
comme le *Récapitulatif du dossier* ou le *Rapport d'instruction*.

Depuis le portlet d'actions contextuelles
#########################################

Pour masquer une consultation depuis le portlet d'actions contextuelles
il faut cliquer sur l'action *Masquer dans les éditions*.

.. image:: a_portlet_masquer_consultation.png

La consultation n’apparaîtra plus dans les éditions qui affichent leur liste.
Pour l'afficher à nouveau cliquer sur l'action *Afficher dans les éditions*.

.. image:: a_portlet_visible_consultation.png

Ces actions ne sont pas disponibles sur les dossiers d'instruction clôturés ou si l'utilisateur connecté ne fait pas partie de la division du dossier. Ces deux conditions peuvent être exclues grâce à une permission spécifique.

Depuis la liste de consultations
################################

Depuis le listing des consultations, il est possible de modifier la visibilité
des consultations dans les éditions.

Si la consultation est visible, cliquer sur l'icône en forme d'oeil rouge permet de la masquer.

.. image:: a_instruction_tab_masquer_consultation.png

Si la consultation est masquée, cliquer sur l'icône en forme d'oeil vert permet de la rendre visible.

.. image:: a_instruction_tab_visible_consultation.png

Ces actions ne sont pas disponibles sur les dossiers d'instruction clôturés ou si l'utilisateur connecté ne fait pas partie de la division du dossier. Ces deux conditions peuvent être exclues grâce à une permission spécifique.

.. _instruction_qualification:

================
La qualification
================

La qualification d'un dossier d'instruction se fait depuis son formulaire de modification.

La qualification en ERP
=======================

Pour indiquer un dossier d'instruction comme concernant un **ERP**, il faut cocher la case *ERP* dans l'encadré *Qualification* du formulaire.

.. image:: a_instruction_qualification_erp.png

Lors de la validation du formulaire, toutes les pièces numérisées et générées liées au dossier d'instruction auront leur métadonnée *concerneERP* modifiée. À condition que le connecteur du système de stockage soit configuré pour effectuer cette action.

Attention, si l'option_referentiel_erp est active, le comportement de cette case ne sera plus le même. Elle sera non modifiable dans le cas où le dossier ne peut pas être interfacé avec le référentiel ERP.

Lors de l'ajout d'un dossier d'instruction sur existant, si le type de dossier d'instruction est compatible avec la liaison vers le référentiel ERP, la valeur de la case ERP du dossier initial est appliqué au nouveau dossier.

.. _instruction_pec:

=========================
La prise en compte métier
=========================

L'indicateur de la prise en compte métier (PeC) se met à jour depuis l'instruction du dossier d'instruction de consultation. C'est une information nécessaire lorsque l'application openADS est en mode service consulté. Voir l':ref:`option option_mode_service_consulte<administration_liste_options>`.

.. image:: a_instruction_pec.png

Il est possible de paramétrer les différentes PeC depuis la table de référence :ref:`Prise en compte métier <parametrage_pec>` et de paramétrer l'utilisation depuis un :ref:`événement d'instruction<parametrage_dossiers_evenement>`.


======================
Rubrique qualification
======================

(:menuselection:`Instruction --> Qualification`)

Cette rubrique propose deux sous-menus :
 * Dossiers à qualifier
 * Architecte Fréquent

Dossiers à qualifier
====================

Liste les dossiers à qualifier dont je suis l'instructeur ou l'instructeur secondaire.

Depuis ce menu, il est possible de filtrer les dossiers à qualifier par :
 * numéro de dossier
 * libellé du dossier
 * nom du pétitionnaire (particulier)
 * dénomination du pétitionnaire (personne morale)
 * instructeur
 * instructeur secondaire

======================
Rubrique consultations
======================

(:menuselection:`Instruction --> Consultations`)

Cette rubrique propose les sous-menus :
 * Mes retours
 * Retour de ma division
 * Tous les retours

Mes retours
===========

Liste tous les retours de consultation non lu et liés à des dossiers d'instruction dont je suis
l'instructeur ou l'instructeur secondaire.

Depuis ce menu, il est possible de filtrer les consultations par :
 * abrégé du service
 * service
 * date d'envoi de la consultation
 * date de retour de la consultation
 * date limite de la consultation

Retour de ma division
=====================

Liste tous les retours de consultation non lu et liés à des dossiers d'instruction affectés à ma division.

Depuis ce menu, il est possible de filtrer les consultations par :
 * abrégé du service
 * service
 * instructeur du dossier auquel est rattaché la consultation
 * instructeur secondaire du dossier auquel est rattaché la consultation
 * date d'envoi de la consultation
 * date de retour de la consultation
 * date limite de la consultation

Tous les retours
================

Liste tous les retours de consultation non lu et liés à des dossiers d'instruction affectés à ma collectivité.
Pour un utilisateur appartenant à une collectivité de niveau 2 tous les retours de consultation
non lu seront visible.

Depuis ce menu, il est possible de filtrer les consultations par :
 * abrégé du service
 * service
 * instructeur du dossier auquel est rattaché la consultation
 * instructeur secondaire du dossier auquel est rattaché la consultation
 * division d'appartenance du dossier auquel est rattaché la consultation
 * collectivité d'appartenance du dossier auquel est rattaché la consultation
 * date d'envoi de la consultation
 * date de retour de la consultation
 * date limite de la consultation

=================
Rubrique messages
=================

(:menuselection:`Instruction --> Messages`)

Cette rubrique propose les sous-menus :
 * Mes messages
 * Messages de ma division
 * Tous les messages

Mes messages
============

Pour une collectivité mono : Liste tous les messages non lu liés à des dossiers d'instruction dont je suis l'instructeur ou
l'instructeur secondaire ou étant a destination de la commune.

Pour une collectivité multi : Liste tous les messages non lu liés à des dossiers d'instruction dont je suis l'instructeur ou
l'instructeur secondaire et étant à destination de l'instructeur.

Depuis ce menu, il est possible de filtrer les messages par :
 * dossier
 * type de message
 * émetteur du message
 * date d'émission
 * si le dossier a un enjeu ERP
 * si le dossier a un enjeu Urba

Messages de ma division
=======================

Pour une collectivité mono : Liste tous les messages non lu liés à des dossiers d'instruction associés à
ma division ou étant a destination de la commune.

Pour une collectivité multi : Liste tous les messages non lu liés à des dossiers d'instruction associés à
ma division ou étant a destination de l'instructeur.

Depuis ce menu, il est possible de filtrer les messages par :
 * dossier
 * type de message
 * émetteur du message
 * date d'émission
 * Juriste/Technicien (contentieux) ou instructeur (dossier instruction)
 * instructeur secondaire du dossier sur lequel le message a été déposé
 * si le dossier a un enjeu ERP
 * si le dossier a un enjeu Urba

Tous les messages
=================

Liste tous les messages non lu et liés à des dossiers d'instruction affectés à ma collectivité.
Pour un utilisateur de la collectivité de niveau 2, tous les messages non lu seront affichés.

Depuis ce menu, il est possible de filtrer les messages par :
 * dossier
 * type de message
 * émetteur du message
 * date d'émission
 * Juriste/Technicien (contentieux) ou instructeur (dossier instruction) d'affectation du dossier
 * instructeur secondaire du dossier sur lequel le message a été déposé
 * Division d'appartenance du dossier sur lequel le message a été déposé
 * Collectivité d'appartenance du dossier sur lequel le message a été déposé
 * si le dossier a un enjeu ERP
 * si le dossier a un enjeu Urba

====================
Rubrique Commissions
====================

(:menuselection:`Instruction --> Commissions`)

Cette rubrique propose les sous-menus :
 * Mes Retours
 * Retours de ma division
 * Tous les Retours

Mes Retours
============

Liste des commissions ayant un retour marqué comme "Non vu" et liées à un dossier pour
lequel je suis instructeur ou instructeur secondaire.

Depuis ce menu, il est possible de filtrer les retours de commission par :
 * dossier
 * libellé du dossier d'instruction
 * type de commission
 * libellé de la commission

Retours de ma division
=======================

Liste des commissions ayant un retour marqué comme "Non vu" et liées à un dossier 
affecté à ma division.

Depuis ce menu, il est possible de filtrer les retours de commission par :
 * dossier
 * libellé du dossier d'instruction
 * type de commission
 * libellé de la commission
 * nom de l'instructeur du dossier
 * nom de l'instructeur secondaire du dossier

Tous les Retours
=================

Liste des commissions ayant un retour marqué comme "Non vu" et liées à un dossier
appartenant à ma collectivité.
Pour un utilisateur de la collectivité de niveau 2, tous les retours de commissions
marqués comme "Non Vu" seront affichés.

Depuis ce menu, il est possible de filtrer les retours de commission par :
 * dossier
 * libellé du dossier d'instruction
 * type de commission
 * libellé de la commission
 * nom de l'instructeur du dossier
 * nom de l'instructeur secondaire du dossier
 * division d'appartenance du dossier
 * collectivité d'appartenance du dossier
