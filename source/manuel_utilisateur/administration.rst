.. _administration:

##############
Administration
##############

Libellés
########

.. _administration_collectivite:

=================
Les collectivités
=================

(:menuselection:`Administration --> Collectivité`)

Édition des collectivités présentes dans l'application.

.. _parametrage_parametre:

==============
Les paramètres
==============

(:menuselection:`Administration --> Paramètre`)

Sur le listing des options il est possible de modifier le nombre de résultat à partir de l'URL. Pour cela il faut ajouter *tab_serie* dans l'URL en donnant le nombre de résultat souhaité.

Exemple : *[SITE_WEB]*/app/index.php?module=tab&obj=om_parametre&tab_serie=25

où *[SITE_WEB]* est l'adresse de la racine du logiciel (par exemple https://openads.maville.fr).

Édition des paramètres disponibles dans l'application.

Convention de nommage
=====================

Une convention de nommage existe. Il faut préfixer par :

* **ged\_** les paramètres spécifiques à la GED ;
* **erp\_** les paramètres spécifiques à ERP ;
* **option\_** les paramètres spécifiques aux options ;
* **id\_** les paramètres contenant un numéro d'identifiant ;
* **sig\_** les paramètres spécifiques au systeme d'information géographique ;
* **param_courriel_de_notification_commune\_** les paramètres spécifiques aux notifications par courriel aux communes ;
* **rapport_instruction\_** les paramètres spécifiques au rapport d'instruction.
* **platau\_** les paramètres spécifiques à la connexion avec Plat'AU.

.. _administration_liste_options:

Liste des options
=================

* **option_sig** : la valeur par défaut est *aucun*. Les valeurs possibles sont
  *sig_externe*, *sig_interne* ou *aucun*.
* **option_streetview** : permet d'afficher un lien vers *Google Maps Street View*.
  La valeur par défaut est *aucun*. Les valeurs possibles sont *true* ou *false*.
* **option_referentiel_arrete** : la valeur par défaut est *false*. Les valeurs 
  possibles sont *true* ou *false*.
* **option_localisation** : la valeur par défaut est *false*. Les valeurs possibles 
  sont *true* ou *false*.
* **option_erp** : la valeur par défaut est *false*. Les valeurs possibles sont 
  *true* ou *false*.
* **option_contrainte_di** : la valeur par défaut est *aucun*, exemple 
  d'utilisation "liste_groupe=g1,g2...;liste_ssgroupe=sg1,sg2...;service_consulte=t".
  Toutes les options sont optionnelles.
  Les options liste_groupe et liste_ssgroupe peuvent contenir une valeur unique 
  ou plusieurs valeurs séparées par une virgule, sans espace.
  La dernière option service_consulte permet d'ajouter une condition sur le champ
  du même nom. Il peut prendre t (Oui) ou f (Non) comme valeur.
* **option_afficher_division** : la valeur par défaut est *false*. Les valeurs 
  possibles sont *true* ou *false*.
* **option_arrondissement** : la valeur par défaut est *false*. Les valeurs 
  possibles sont *true* ou *false*.
  Cette option indique si la commune est divisée en arrondissements.
* **option_instructeur_division_numero_dossier** : la valeur par défaut est *false*. Les valeurs possibles sont *true* ou *false*. Cette option indique si le numéro de dossier tient compte de la division de l'instructeur auquel il est affecté.
* **option_portail_acces_citoyen** : permet d'activer ou de désactiver l'accès au :ref:`portail citoyen <portail_citoyen>`.
* **option_notification_piece_numerisee** : permet d'activer ou de désactiver l'ajout de :ref:`message de notification <instruction_dossier_message>`.
* **option_date_depot_demande_defaut** : permet d'afficher (*true*) ou non (*false*) la date du jour dans le champ de la date de dépôt lors de l'ajout d'une demande. La valeur par défaut est *true*.
* **option_simulation_taxes** : permet d'activer (*true*) ou non (*false*) :ref:`la simulation des taxes <instruction_simulation_taxes>` sur les dossiers d'instruction.
* **option_previsualisation_edition** : permet d'activer (*true*) ou non (*false*) :ref:`la prévisualisation des éditions <previsualisation_edition>` sur les événements d'instruction du dossier.
* **option_final_auto_instr_tacite_retour** : permet d'activer (*true*) ou désactiver (*false*) la finalisation automatique des instructions dites tacites (ajoutées automatiquement suite à des délais dépassés) ou dites retours (ajoutées automatiquement suite au suivi des dates).
* **option_ws_synchro_contrainte** : permet d'activer (*true*) ou désactiver (*false*) le :ref:`web service<web_services_ressource_maintenance_synchro_contrainte>` de synchronisation des contraintes depuis le SIG. 
* **option_trouillotage_numerique** : permet d'activer (*true*) ou désactiver (*false*) le trouillotage automatique des :ref:`pièces ajoutées à un dossier d'instruction<instruction_document_numerise_add>`. 
* **option_suppression_dossier_instruction** : permet d'activer (*true*) ou non (*false*) la suppression des dossiers d'instruction non instruits.
* **option_redaction_libre** : permet d'activer (*true*) ou non (*false*) la rédaction libre.
* **option_dossier_saisie_numero** : permet d'activer (*true*) ou de désactiver (*false*) la :ref:`saisie du numéro de dossier lors de l'ajout d'une nouvelle demande<guichet_unique_nouvelle_demande_saisie_numero>`. La valeur par défaut est *false*.
* **option_dossier_saisie_numero_complet** : permet d'activer (*true*) ou de désactiver (*false*) la :ref:`saisie complète du numéro de dossier lors de l'ajout d'une nouvelle demande<guichet_unique_nouvelle_demande_saisie_numero>`. La valeur par défaut est *false*.
* **option_suivi_numerisation** : permet d'activer (*true*) ou de désactiver (*false*) le :ref:`suivi de numérisation<suivi_numerisation>`. la valeur par défaut est *false*.
* **option_cache_piece_num_refuse_da** : permet d'activer (*true*) ou de désactiver (*false*) :ref:`l'affichage des pièce des dossier d'instruction refusé sur le DA <affiche_piece_da>`. La valeur par défaut est *false*.
* **option_dossier_commune** : permet d'activer (*true*) ou de désactiver (*false*) la :ref:`saisie de la commune associée au dossier lors de l'ajout d'une nouvelle demande<guichet_unique_nouvelle_demande_saisie_commune>`. La valeur par défaut est *false*. IMPORTANT : cette option doit être uniquement définie pour une collectivité de niveau 2.
* **option_date_depot_mairie** : permet d'activer (*true*) ou de désactiver (*false*) :ref:`l'affichage de la date de dépôt en mairie<affichage_date_depot_mairie>` dans le dossier d'instruction. la valeur par défaut est *false*.
* **option_renommer_collectivite** : permet d'activer (*true*) ou non (*false*) le renommage dans toutes les interfaces de *Collectivité* en *Service*. La valeur par défaut est *false*.
* **option_mode_service_consulte** : permet de changer (*true*) ou non (*false*) l'openADS pour être utilisé par un service consulté.
* **option_date_evenement_instruction_lecture_seule** : permet de passer en lecture seule (*true*) ou pas (*false*) les dates d'évènements pour toutes les instructions.
* **option_geolocalisation_auto_contrainte** : permet de récupérer les contraintes lors de :ref:`la géolocalisation automatique<administration_geolocalisation_auto>`.
* **option_miniature_fichier** : permet d'activer (*true*) ou de désactiver (*false*) :ref:`l'affichage de la miniature des fichiers dans l'onglet Pièce(s)<instruction_document_numerise_tab>`.
* **option_afficher_couleur_dossier** : permet d'activer (*true*) ou de désactiver (*false*) l'identification par :ref:`couleur du type<parametrage_dossiers_dossier_autorisation_type_detaille>` des dossiers d'instruction dans la plupart des listings.
* **option_renommage_document_numerise_tache** : permet d'activer (*true*) ou de désactiver (*false*) la règle de rennomage des fichiers sur les pièces issues de flux dématérialisés.
* **option_notification_depot_demat** : permet d'activer (*true*) ou de désactiver (*false*) la notification automatique par courriel des communes en cas de dépôt de dossier par voie dématérialisé.
* **option_module_acteur** : permet d'activer (*true*) ou de désactiver (*false*) le module acteur. Lorsque le module est actif les champs de paramétrage de la notification automatique des tiers et de l'ajout des acteurs ainsi que l'onglet *Acteur(s)* des dossiers seront accessibles.
* **option_afficher_lien_parapheur** : permet d'activer (*true*) ou de désactiver (*false*) l'affichage du lien de redirection vers le document à signer sur le parapheur.
* **option_filtre_instructeur_DI_par_division** : permet de filtrer (*true*) ou non (*false*) les instructeurs de la liste des affectations au dossier par rapport à la division de l'utilisateur.
* **affichage_di_listing_colonnes_masquees** : permet de masquer certaines colonnes dans les listing de dossier d'instruction. Valeurs possibles : "date_complet" et "localisation" ou les deux séparées par un ";". La valeur "date_complet" permet de masquer la colonne "complétude" et la valeur "localisation" permet de masquer la colonne "localisation".
* **option_afficher_localisation_colonne_dossier** : permet d'afficher (*true*) ou non (*false*) la localisation au survol du numéro de dossier dans les listing de dossier d'instruction.
* **option_normaliser_nommage_document_numerise** : permet d'avoir la même nomenclature pour chaque document quel que soit sa source d'ajout (interface, tache, numérisation ou autre).
  Les documents nouvellement ajouté prendront la nomenclature suivante : *IDDI_DATE_TYPEPIECE_IDPIECE_LIBTYPEPIECE*

    - IDDI : identifiant du dossier en majuscule
    - DATE : Date de dépôt AAAAMMJJ (A = Année, M = mois, J = jour)
    - TYPEPIECE : nomenclature CERFA si elle existe, sinon code du type de document
    - IDPIECE : identifiant du document
    - LIBTYPEPIECE : libellé du type de pièce limité à 50 caractères normalisé (ou description pour les *autre à préciser*) avec ses majuscules. Les espaces sont remplacés par des tirets.

.. note::

  La suppression d'une option entraîne la désactivation des fonctionnalités liées 
  à l'option.

.. _parametrage_parametre_geolocalisation_auto:

Paramétrage de la géolocalisation automatique
=============================================

Utilisation des paramètres de géolocalisation automatique :

* **param_geolocalisation_auto** : paramètre permettant de définir les dossiers candidats à :ref:`la géolocalisation automatique<administration_geolocalisation_auto>`. Ce paramètre est composé de 3 arguments obligatoires séparés par des points-virgules ";" :

  * La date limite inférieure de traitement au format AAAA-MM-JJ.
    Il est également possible de ne sélectionner que les dossiers du jour en utilisant le mot clé "today".
  * Le ou les codes de type de DA, par exemple : 'PC','AT'.
  * L'avis de décision à exclure, par exemple : retiré.

Exemple de paramétrage : 2015-01-01;'PC','AT';retiré

.. _parametrage_parametre_param_url:

Paramétrage des URLs d'accès à openADS
======================================

Utilisation des paramètres pour les fonctionnalités générant des URLs d'accès à openADS :

* **param_base_path_metadata_url_di** : si renseigné permet de remplacer la construction actuelle de la base de l'URL pour la métadonnées de dossier envoyée au parapheur électronique, sinon la base de l'URL sera générée automatiquement par rapport aux informations du serveur.

La base de l'URL débute du début de l'URL jusqu'à "app/" non inclut.

Exemple si le paramètre n'est pas renseigné : [http://localhost/openads/]app/index.php?module=form&direct_link=true&obj=dossier_instruction&action=3&direct_field=dossier&direct_form=instruction&direct_action=3&direct_idx=29

Exemple si le paramètre est renseigné : [url_renseignée_dans_le param/]app/index.php?module=form&direct_link=true&obj=dossier_instruction&action=3&direct_field=dossier&direct_form=instruction&direct_action=3&direct_idx=29

.. _parametrage_parametre_suivi_numerisation:

Paramétrage du suivi de numérisation
====================================

Utilisation des paramètres du suivi de numérisation :

* **numerisation_type_dossier_autorisation** : définit la liste des codes de type de dossier d'instruction autorisés à être numérisés. Par exemple *PCI;CUb;DP*.
* **numerisation_intervalle_date** : autorise les dossiers déposés dans les *X* derniers jours à être numérisés. *X* étant un entier, par exemple *30*.

.. _parametrage_parametre_identifiants:

Liste des paramètres d'identification
=====================================

Utilisation des paramètres d'identification :

* **id_evenement_bordereau_avis_maire_prefet** : paramètre définissant l'identifiant de l'événement à appliquer aux dossiers d'instruction faisant l'objet d'un bordereau d'envoi des avis du Maire au Préfet, doit être paramétré sur la collectivité de niveau 2 pour une configuration multi-communes.
* **id_datd_filtre_reqmo_dossier_dia** : paramètre définissant l'identifiant du type détaillé de dossier d'autorisation des dossiers d'instruction à retourner dans la requête mémorisée de :ref:`l'exports statistique des DIA <reqmo_export_dia>`. Il est possible d'ajouter plusieurs valeur à condition qu'elles soient séparées par des virgules.
* **id_avis_consultation_tacite** : paramètre définissant l'identifiant de l'avis de consultation pour le caractère tacite. Notamment utilisé pour la récupération et l'identification des pièces recommandées lors de la :ref:`constitution du dossier final <instruction_document_numerise_constituer_dossier_final>`.
* **id_affichage_obligatoire** : paramètre définissant l'identifiant de l'événement d'instruction d'attestation d'affichage suite au dépôt. Cet événement est appliqué automatiquement sur tous les dossiers en cours lors de l'impression du registre d'affichage réglementaire et c'est son édition qui est retourné lors de l'impression d'attestation réglementaire. Il est également utilisé par l'action permettant d'afficher l'attestation d'affichage depuis la fiche d'un dossier d'instruction.
* **dit_code__to_transmit__platau** : paramètre global définissant les codes de type de dossier d'instruction pouvant être transmis à Plat'AU. Ce filtre s'applique seulement lorsque le :ref:`type de dossier d’autorisation détaillé est paramétré comme transmissible à Plat’AU<parametrage_dossiers_dossier_autorisation_type_detaille>`. Si ce paramètre n'est pas renseigné, alors par défaut tous les types de DI sont considérés comme transmissibles. Ce paramètre est spécifique aux demandes sur existant, il ne s'applique pas aux dossiers d'instruction initiaux.

.. _administration_parametrage_notification:

Paramétrage des notifications
=============================

.. _administration_parametrage_notification_commune:

Paramétrage de la notification aux communes
-------------------------------------------

* **param_courriel_de_notification_commune** : paramètre commune listant les adresses mails de notification (une par ligne).
* **param_courriel_de_notification_commune_objet_depuis_instruction** : paramètre communauté spécifiant l'objet du courriel.
* **param_courriel_de_notification_commune_modele_depuis_instruction** : paramètre communauté (écrasable par la commune) spécifiant le modèle du corps du courriel.

.. _administration_parametrage_notification_depot_demat:

Paramétrage de la notification de dépôt de dossier par voie dématérialisé
-------------------------------------------------------------------------

* **param_courriel_de_notification_commune** : paramètre commune listant les adresses mails de notification (une par ligne).
* **param_courriel_de_notification_depot_demat_titre** : paramètre communauté spécifiant l'objet du courriel.
* **param_courriel_de_notification_depot_demat_message** : paramètre communauté (écrasable par la commune) spécifiant le modèle du corps du courriel.

.. _administration_parametrage_service_consulte:

Paramétrage des courriels de notification des services consultés
----------------------------------------------------------------

* **parametre_courriel_service_type_titre** : paramètre communauté spécifiant l'objet du courriel. Dans le titre, il est possible d'ajouter le numéro de Dossier
  avec le champs de fusion [dossier]
* **parametre_courriel_service_type_message** : paramètre spécifiant le modèle du corps du courriel. Dans ce modèle les champs de fusion
  [LIEN_TELECHARGEMENT_DOCUMENT] et [LIEN_TELECHARGEMENT_ANNEXE] doivent être utilisée pour pouvoir insérer les liens vers le document et les annnexes.
  Pareil que pour le titre, [DOSSIER] permet d'insérer le numéro de dossier.

Configuration des mails envoyés automatiquement aux services consultés :

* **services_consultes_lien_interne** : contient un lien d'accès en interne à openADS qui sera affiché dans le mail.
* **services_consultes_lien_externe** : contient un lien d'accès en externe à openADS qui sera affiché dans le mail.

.. note::

  Il est possible de renseigner des variables de remplacement dans l'objet et le corps du courriel :

  * **<DOSSIER_INSTRUCTION>** pour le numéro du dossier (objet et corps) ;
  * **<ID_INSTRUCTION>** pour l'identifiant unique de l'événement d'instruction (corps uniquement) ;
  * **<URL_INSTRUCTION>** pour le lien direct vers l'événement d'instruction (corps uniquement).
  
  Dans certains cas de figure, l'adresse **<URL_INSTRUCTION>** ne fonctionne pas. Si vous ne souhaitez pas faire appel à la génération automatique du lien, il faut écrire manuellement :

  **<a href="** *[LIEN]* **">** *[LIEN]* **</a>**

  en remplaçant *[LIEN]* par :

  *[SITE_WEB]***/app/index.php?module=form&direct_link=true&obj=dossier_instruction&action=3&direct_field=dossier&direct_form=instruction&direct_action=3&direct_idx=<ID_INSTRUCTION>**

  où *[SITE_WEB]* est l'adresse de la racine du logiciel (par exemple https://openads.maville.fr).

.. _administration_parametrage_tiers_consulte:

Paramétrage des courriels de notification de tiers consultés
------------------------------------------------------------

* **parametre_courriel_tiers_type_titre** : paramètre communauté spécifiant l'objet du courriel. Dans le titre, il est possible d'ajouter le numéro de Dossier
  avec le champs de fusion [DOSSIER]
* **parametre_courriel_tiers_type_message** : paramètre spécifiant le modèle du corps du courriel. Dans ce modèle les champs de fusion
  [LIEN_TELECHARGEMENT_DOCUMENT] et [LIEN_TELECHARGEMENT_ANNEXE] doivent être utilisée pour pouvoir insérer les liens vers le document et les annnexes.
  Pareil que pour le titre, [DOSSIER] permet d'insérer le numéro de dossier.

.. _administration_parametrage_notification_demandeur:

Paramétrage de la notification des demandeurs
---------------------------------------------

Utilisation des paramètres de notification des instructions :

* **option_notification** : paramètre permettant de choisir le mode de notification des pétitionnaires. Les valeurs possibles sont *portal* ou *mail*. Si le paramètre n'est pas défini, alors la notification des pétitionnaires ne pourra pas se faire.
* **parametre_notification_url_acces** : paramètre permettant de définir l'url d'accès aux documents notifiés. Obligatoire pour l'envoi de notification par mail.
* **parametre_notification_max_annexes** : paramètre définissant le nombre maximum d'annexe que l'on peut ajouter lors de la notification des demandeurs. Par défaut, si ce paramètre n'est pas renseigné ou mal renseigné, il sera défini à 5.
* **option_bloquer_notif_auto_dln** : paramètre empêchant la notification automatique si la date limite de notification est dépassée. S'active en renseignant les types d'événements dans l'option, séparés par des point-virgules ";", exemple : "incompletude;majoration_delai".

Paramètre de notification des demandeurs :

* **parametre_courriel_type_titre** : paramètre communauté spécifiant l'objet du courriel. Dans le titre, il est possible d'ajouter le numéro de Dossier
  avec le champs de fusion [DOSSIER]
* **parametre_courriel_type_message** : paramètre spécifiant le modèle du corps du courriel. Dans ce modèle les champs de fusion
  [LIEN_TELECHARGEMENT_DOCUMENT] et [LIEN_TELECHARGEMENT_ANNEXE] doivent être utilisée pour pouvoir insérer les liens vers le document et les annnexes.
  Pareil que pour le titre, [DOSSIER] permet d'insérer le numéro de dossier.

.. _administration_parametrage_notification_signataire:

Paramétrage de la notification d'envoi de document en signature
---------------------------------------------------------------

Paramètre de notification des signataires :

* **param_courriel_notification_signataire_type_titre** : paramètre spécifiant l'objet du courriel. Dans le titre, il est possible d'ajouter le numéro de dossier en écrivant [DOSSIER]. Par défaut, c'est à dire si le paramètre n'est pas rempli, le sujet des courriels est ::

  [openADS] Nouveau document à signer pour le dossier [DOSSIER]


* **param_courriel_notification_signataire_type_message** : paramètre spécifiant le modèle du corps du courriel. Dans ce modèle, le champs de fusion [LIEN_PAGE_SIGNATURE] doit être utilisé pour pouvoir insérer le lien vers le document à signer. Par défaut, le contenu des courriels est ::

    Bonjour,

    Un document concernant le dossier <DOSSIER_INSTRUCTION> est disponible à la signature.

    Vous pouvez le signer en cliquant sur le lien suivant :
    [LIEN_PAGE_SIGNATURE]

    Si vous possédez un compte sur openADS, vous pouvez retrouver l'intégralité du dossier en suivant le lien ci-dessous :
    [URL_DOSSIER]


.. note:: Les variables de remplacement [LIEN_TELECHARGEMENT_DOCUMENT] et [LIEN_TELECHARGEMENT_ANNEXE] ne peuvent pas être utilisées avec ces paramètres.

.. _administration_parametrage_variable_remplacement_notification:

Variable de remplacement des messages de notification
-----------------------------------------------------

Pour tous les types de notification, il est possible de renseigner des variables de remplacement dans l'objet et le corps du courriel :

  * **[DOSSIER]** pour le numéro du dossier (objet).
  * **<DOSSIER_INSTRUCTION>** pour le numéro du dossier (corps).
  * **[LIEN_TELECHARGEMENT_DOCUMENT]** pour afficher le lien permettant de télécharger le document de l'instruction.
  * **[LIEN_TELECHARGEMENT_ANNEXE]** pour afficher les liens des annexes sélectionnées.
  * **<URL_INSTRUCTION>** pour afficher le lien vers l'instruction.
  * **<ID_INSTRUCTION>** pour afficher l'identifiant de l'instruction.
  * **[URL_DOSSIER]** pour afficher le lien vers le dossier.

.. note::

  Par défaut, pour tous les types de notification (sauf les notifications aux communes et aux signataires), le titre sera : [openADS] Notification concernant votre dossier [DOSSIER].

  Par défaut, quel que soit le type de notification, le message sera :
  Bonjour, veuillez prendre connaissance du(des) document(s) suivant(s) :<br>[LIEN_TELECHARGEMENT_DOCUMENT]<br>[LIEN_TELECHARGEMENT_ANNEXE].

.. _parametrage_parametre_collectivite_communes:

Paramétrage des communes associées à une collectivité
=====================================================

Lorsque l':ref:`option dossier_commune<administration_liste_options>` est activée, il est possible d'associer une commune à un dossier, lors d'une nouvelle demande. Or la liste des communes peut s'avérer trop fournie.
*Par exemple lorsqu'on entre le nombre '13', on peut autant voir les communes marseillaises qui appartiennent au département '13' que la commune parisienne du 13ème arrondissement '75113'.*
C'est pourquoi ce paramètre de collectivité permet de limiter les communes visibles, en spécifiant, quelles sont les communes associables aux dossiers de cette collectivité.

* **param_communes** : sa valeur doit être une liste de code INSEE de communes ou de départements, séparés par des virgules (sans espace entre)

Par exemple, pour que les instructeurs de la collectivité *Marseille* ne puisse "associer" que les communes Marseillaises et Aixoises et pas les communes Parisiennes, il faut que ce paramètre soit défini avec la valeur suivante (pour la collectivité *Marseille*) : *13201,13202,13203,13204,13205,13206,13207,13208,13209,13210,13211,13212,13213,13214,13215,13216,13055,13001*, soit l'ensemble des arrondissements Marseillais (*13201* à *13216*), la commune de Marseille (*13055*), plus la commune d'Aix-en-Provence (*13001*).

.. _parametrage_parametre_portal:

Paramétrage du lien avec le portail SVE
=======================================

Le paramétre **portal_code_suivi_base_url** permet de reneigner l'url du suivi de demande dans le portail SVE. La variable de remplacement **[PORTAL_CODE_SUIVI]** permet d'indiquer le code de suivi dans le lien vers le portail SVE.

Exemple : https://demarches-ideau-preprod.atreal.fr/code/[PORTAL_CODE_SUIVI]/load

.. _parametrage_communes_departements_regions:

Communes, départements et régions
#################################

Lorsque l':ref:`option dossier_commune<administration_liste_options>` est activée, il est nécessaire d'alimenter la base de données des communes. Cela se fait généralement via l':ref:`import spécifique<administration_import_specific_commune>`, mais une édition manuelle reste possible, et c'est dans ces menus qu'on la trouve.

Les départements et régions, ne sont utilisés (actuellement) que pour fournir des libellés dans les éditions, et donc sont moins critiques, mais leur gestion manuelle reste aussi accessible via ces menus.

.. _parametrage_gestion_des_operateurs:

Gestion des opérateurs
######################

Pour que le paramétrage soit pris en compte dans l'application, il faut que celle-ci soit en mode service consulté (*option_mode_service_consulte*) et que l'option *option_dossier_commune* soit activé.

Afin de paramétrer la gestion des opérateurs, il faut ajouter un paramètre nommé *param_operateur* qui doit comporter seulement du json valide.
Si ce n'est pas le cas alors l'application retourne une erreur.

Pour le champ "etat" on attends la valeur présente dans le champ "État".
Pour le reste ce sont des identifiants soit des catégories de tiers consultés, soit des types d'habilitation de tiers consultés soit l'identifiant d'évènement qui sera ajouté lors de la validation de l'opérateur, il ne faut pas entourer les identifiants de guillemets.

Pour plus d'informations consultez cette partie : :ref:`Désignation de l'opérateur<instruction_designation_operateur>`.

Voici la structure du json :

.. code-block:: json
    
    {
        "etat": ["XXX", "XXX"],
        "categorie_tiers_inrap": ["XXX"],
        "categorie_tiers_collterr": ["XXX"],
        "categorie_tiers_amenageur_public": ["XXX"],
        "type_habilitations_operateurs_diag_kpark": ["XXX"],
        "type_habilitations_operateurs_diag_toutdiag": ["XXX"],
        "type_habilitations_operateurs_inrap": ["XXX"],
        "cas": [
            {
                "libelle" : "CAS A",
                "parametre" : {
                    "P2" : null
                },
                "type_operateur" : "inrap",
                "evenement" : 123
            },
            {
            "libelle" : "CAS D2",
            "parametre" : {
                "P2" : "collterr",
                "P3" : "tout_diag",
                "P4" : true,
                "P5" : true,
                "P7" : "F"
            },
            "type_operateur" : "collterr",
            "evenement" : 123
            }
        ]
    }


Gestion Des Utilisateurs
########################

.. _administration_profil:

===========
Les profils
===========

(:menuselection:`Administration --> Gestion Des Utilisateurs --> Profil`)

Édition des profils présents dans l'application.

.. _administration_droit:

==========
Les droits
==========

(:menuselection:`Administration --> Gestion Des Utilisateurs --> Droit`)

Édition des droits présents dans l'application.

.. _administration_utilisateur:

================
Les utilisateurs
================

(:menuselection:`Administration --> Gestion Des Utilisateurs --> Utilisateur`)

Édition des utilisateurs présents dans l'application.


.. _administration_utilisateur_lies:

=====================
Les utilisateurs liés
=====================

L'utilisateur principal est celui avec lequel un utilisateur peut se connecter à l'application. Il est possible d'ajouter des utilisateurs liés à cet utilisateur principal. Lorsque l'utilisateur se connecte avec son utilisateur principal il peut alors changer d'utilisateur sans passer par la déconnexion.

Pour cela il doit cliquer sur "Utilisateurs liés" à côté du bouton de déconnexion et sélectionner l'utilisateur sur lequel il veut se connecter. À la validation du formulaire, le changement d'utilisateur s'effectue sans nécessité d'authentification. L'utilisateur peut revenir à tout moment sur l'utilisateur principal en effectuant de nouveau la manipulation.

Sous condition d'avoir les bonnes permissions (*lien_om_utilisateur_om_utilisateur* ou les sous permissions *lien_om_utilisateur_om_utilisateur_ajouter*/*supprimer*/*consulter*/*tab*). Il est possible pour un administrateur d'ajouter des utilisateurs liés sur un utilisateur principal.

Pour cela, il doit se rendre dans l'onglet "Utilisateur lié" de l'utilisateur principal. Puis cliquer sur la croix permettant d'ajouter un nouvel enregistrement.

Le formulaire est similaire au formulaire de l'utilisateur principal, seul le mail et le mot de passe ne sont pas disponibles. L'adresse email de l'utilisateur lié sera la même que l'utilisateur principal et le mot de passe et généré automatiquement.

L'action « Ouvrir le formulaire de l'utilisateur lié » permet d'accéder au paramétrage de l'utilisateur lié afin de pouvoir effectuer les opérations habituelles sur un utilisateur.

.. _administration_annuaire:

==========
L'annuaire
==========

(:menuselection:`Administration --> Gestion Des Utilisateurs --> Annuaire`)

Gestion des utilisateurs grâce à un LDAP.


Gestion de la confidentialité des dossiers
##########################################

Par défaut dans openADS, tous les dossiers sont visibles par tous les
utilisateurs, du moment que l'utilisateur est de la même collectivité que le
dossier ou si l'utilisateur est affecté à la collectivité de niveau 2.

Selon le besoin, il est possible d'avoir des dossiers dits confidentiels qui
sont ajoutables, consultables et modifiables seulement par certains utilisateurs
ou groupes d'utilisateurs.

Afin de rendre confidentiels certains dossiers, il faut d'abord paramétrer le 
:ref:`type de dossier de dossier d'autorisation <parametrage_dossiers_dossier_autorisation_type>`
comme confidentiel et choisir en tant que groupe le groupe d'utilisateurs qui
aura accès aux dossiers de ce type.
Chaque profil d'utilisateur est lié à un ou plusieurs groupes, avec un
paramètre définissant l'accès aux dossiers confidentiels de ces groupes. Si un
profil a accès aux dossiers confidentiels d'un groupe, alors tous les
utilisateurs avec ce profil auront accès aux dossiers confidentiels de ce groupe.
Il est également possible de redéfinir ces accès attribués à tous les
utilisateurs d'un profil, en paramétrant par utilisateur les groupes auxquels
il a accès. Cela permet d'ajouter ou de retirer des accès à un utilisateur en
particulier.

.. NOTE:: Le paramétrage de groupe par utilisateur prend entièrement le pas sur le paramétrage du profil. Du moment qu'un groupe a été ajouté à l'utilisateur, les groupes paramétrés sur le profil n'ont plus d'effet pour cet utilisateur. Chaque groupe auquel l'utilisateur a accès doit donc être ajouté à l'utilisateur.

Prenons en exemple le type de dossier d'autorisation "Déclaration d'intention
d'aliéner", lié au groupe ADS. En choisissant le paramètre confidentiel *Non*,
il sera alors visible par tous les utilisateurs.
Passons maintenant le paramètre confidentiel à *Oui*. Le dossier devient alors
seulement visible aux utilisateurs du groupe ADS ayant accès aux dossiers
confidentiels.
Disons que nous avons un profil *Instructeur* lié au groupe ADS (avec accès aux
dossiers confidentiels), et un profil *Visualisation DA et DI* qui est lié
au groupe ADS (sans accès aux confidentiels).
De fait, tous les utilisateurs avec le profil *Instructeur* pourront accéder aux
dossiers déclaration d'intention d'aliéner, mais pas les utilisateurs
*Visualisation DA et DI*.

Paramétrage par défaut du profil *Visualisation DA et DI* :

.. image:: a_administration_om_profil_groupe.png

Si on souhaite faire une exception pour un utilisateur ayant le profil
*Visualisation DA et DI*, on peut donner lui donner l'accès aux dossiers
confidentiels sans impacter les autres utilisateurs avec ce profil. Il suffit,
depuis le paramétrage de l'utilisateur, de le lier une nouvelle fois au groupe
ADS avec cette fois l'accès aux dossiers confidentiels de ce groupe. Ce
paramétrage du groupe directement par utilisateur prenant le pas sur le
paramétrage par profil, cet utilisateur pourra avoir accès aux dossiers DIA,
mais pas les autres utilisateurs avec le profil *Visualisation DA et DI*.

Illustration du paramétrage pour cet utilisateur :

.. image:: a_administration_om_utilisateur_groupe.png

.. NOTE:: Les groupes auxquels l'utilisateur est attaché sont visibles dans le widget :ref:`Infos profil<widget_infos_profil>`.

Tableaux de Bord
################


.. _administration_widget:

===========
Les widgets
===========

(:menuselection:`Administration --> Tableaux De Bord --> Widget`)

Un widget, contraction de window (fenêtre) et gadget, est un composant du
tableau de bord proposant des informations.

Son paramètrage nécessite la saisie de quatre champs :

* **libellé** : le titre du widget
* **type** : *file* lorsqu'il s'agit d'un script ou *web* lorsqu'il s'agit d'un
  appel à un web service
* **script** ou **lien** selon respectivement le type *file* ou *web* : nom du
  script ou URL du web service
* **arguments** ou **texte** selon respectivement le type *file* ou *web* :
  paramètres du script ou texte du widget (iframe, JavaScript, AJAX ...)

Seuls les widgets de type *file* sont utilisés dans openADS.

Les arguments sont déclarés ainsi :

::

  argument1=valeur1
  argument2=valeur2

Les scripts disponibles sont les suivants :

.. _administration_widget_consultation_retours:

consultation_retours
====================

Ce widget permet d'afficher le nombre de retours de consultation marqués comme 'non lu' pour les dossiers de l'utilisateur correspondant au filtre paramétrable. Un lien *Voir +* permet d'accéder au listing complet. Les informations fonctionnelles sont disponibles :ref:`ici<widget_consultation_retours>`.

Un argument facultatif est paramétrable :

* **filtre** [par défaut *instructeur*] - les filtres disponibles sont *aucun*, *division*, *instructeur* et *instructeur_secondaire*

.. _administration_widget_commission_mes_retours:

commission_mes_retours
======================

Ce widget permet d'afficher le nombre de retours de commission marqués comme
'non lu' pour les dossiers de l'utilisateur correspondant au filtre
paramétrable. Un lien *Voir +* permet d'accéder au listing complet. Les
informations fonctionnelles sont disponibles
:ref:`ici<widget_commission_mes_retours>`.

Un argument facultatif est paramétrable :

* **filtre** [par défaut *instructeur*] - les filtres disponibles sont *aucun*,
  *division*, *instructeur* et *instructeur_secondaire*

.. _administration_widget_dossiers_limites:

dossiers_limites
================

Ce widget permet d'afficher les dossiers d'instruction dont la date limite est dans moins de X jours. Seuls les 10 premiers résultats sont affichés. Un lien *Voir +* permet d'accéder au listing complet. Les informations fonctionnelles sont disponibles :ref:`ici<widget_dossiers_limites>`.

Cinq arguments facultatifs sont paramétrables :

* **filtre** [par défaut *instructeur*] - les filtres disponibles sont *aucun*, *division*, *instructeur* et *instructeur_secondaire*
* **nombre_de_jours** [par défaut *15*] - délai en jours avant la date limite à partir de laquelle on souhaite voir apparaître les dossiers
* **codes_datd** [par défaut tous les types sont affichés] - liste des types de dossiers à afficher séparés par un point-virgule. exemple : *PCI;PCA;DPS;CUa;CUb*
* **restreindre_aux_tacites** [par défaut il n'y a pas de restriction concernant le caractère tacite] - prend comme valeur *true* (affiche seulement les dossiers d'instruction dont le caractère tacite est actif) ou *false* (pas de restriction concernant le caractère tacite)
* **affichage** [par défaut c'est le listing qui est affiché] - choix de l'affichage du listing (*liste*) ou du nombre d'élément dans un cercle bleu (*nombre*).

.. _administration_widget_messages_retours:

messages_retours
================

Ce widget permet d'afficher le nombre de messages en attente de lecture pour les dossiers de l'utilisateur correspondant au filtre paramétrable. Un lien *Voir +* permet d'accéder au listing complet. Les informations fonctionnelles sont disponibles :ref:`ici<widget_messages_retours>`.

Quatre arguments facultatifs sont paramétrables :

* **contexte** [par défaut *standard*] - les contextes disponibles sont *standard* et *contentieux*
* **filtre** [par défaut *instructeur*] - les filtres disponibles sont *aucun*, *division*, *instructeur* et *instructeur_secondaire*
* **dossier_cloture** [par défaut *false*] - prend comme valeur *true* ou *false* et permet ou non d'inclure les dossiers d'instruction clôturés dans les résultats

.. note::
    Bien que le widget redirige vers un listing accessible également depuis un menu, l'argument **dossier_cloture** avec la valeur *true* n'est possible que depuis la redirection vers ce listing disponible dans le pied de page du widget.

.. _administration_widget_dossiers_evenement_incomplet_majoration:

dossiers_evenement_incomplet_majoration
=======================================

Ce widget présente les dossiers les plus récents (10 max.) sur lesquels ont été appliqué un événement de majoration ou d'incomplétude avec une date d'envoi de lettre AR renseignée pour cet événement, mais dont la date de notification n'a pas été complétée. Un lien "Voir tous les dossiers d'instruction avec un événement d'incompletude ou de majoration de délai sans date de notification" permet d'accéder au listing complet. Les informations fonctionnelles sont disponibles  :ref:`ici<widget_dossiers_evenement_incomplet_majoration>`.

Un argument facultatif est paramétrable :

* **filtre** [par défaut *instructeur*] - les filtres disponibles sont *aucun*, *division*, *instructeur* et *instructeur_secondaire*


.. _administration_widget_nouvelle_demande_nouveau_dossier:

nouvelle_demande_nouveau_dossier
================================

Les informations fonctionnelles sont disponibles :ref:`ici<widget_nouvelle_demande_nouveau_dossier>`.

Un argument facultatif est paramétrable :

* **contexte** [par défaut *standard*] - les contextes disponibles sont *standard* et *contentieux*.


.. _administration_widget_dossier_contentieux_recours:

dossier_contentieux_recours
===========================

Les informations fonctionnelles sont disponibles :ref:`ici<widget_dossier_contentieux_recours>`.

Deux argument facultatif sont paramétrables :

* **filtre** [par défaut *instructeur*] - les filtres disponibles sont *aucun* et *instructeur*.
* **affichage** [par défaut *liste*] - les affichages disponibles sont *liste* ou *nombre*


.. _administration_widget_dossier_contentieux_infraction:

dossier_contentieux_infraction
==============================

Les informations fonctionnelles sont disponibles :ref:`ici<widget_dossier_contentieux_infraction>`.

Deux argument facultatif sont paramétrables :

* **filtre** [par défaut *instructeur*] - les filtres disponibles sont *aucun* et *instructeur*.
* **affichage** [par défaut *liste*] - les affichages disponibles sont *liste* ou *nombre*


.. _administration_widget_dossier_contentieux_contradictoire:

dossier_contentieux_contradictoire
==================================

Les informations fonctionnelles sont disponibles :ref:`ici<widget_dossier_contentieux_contradictoire>`.

Deux argument facultatif sont paramétrables :

* **filtre** [par défaut *instructeur*] - les filtres disponibles sont *aucun*, *instructeur* et *division*.
* **affichage** [par défaut *liste*] - les affichages disponibles sont *liste* ou *nombre*


.. _administration_widget_dossier_contentieux_ait:

dossier_contentieux_ait
=======================

Les informations fonctionnelles sont disponibles :ref:`ici<widget_dossier_contentieux_ait>`.

Deux argument facultatif sont paramétrables :

* **filtre** [par défaut *instructeur*] - les filtres disponibles sont *aucun*, *instructeur* et *division*.
* **affichage** [par défaut *liste*] - les affichages disponibles sont *liste* ou *nombre*

.. _administration_widget_dossier_contentieux_audience:

dossier_contentieux_audience
============================

Les informations fonctionnelles sont disponibles :ref:`ici<widget_dossier_contentieux_audience>`.

Deux argument facultatif sont paramétrables :

* **filtre** [par défaut *instructeur*] - les filtres disponibles sont *aucun*, *instructeur* et *division*.
* **affichage** [par défaut *liste*] - les affichages disponibles sont *liste* ou *nombre*

.. _administration_widget_dossier_contentieux_clotures:

dossier_contentieux_clotures
============================

Les informations fonctionnelles sont disponibles :ref:`ici<widget_dossier_contentieux_clotures>`.

Deux argument facultatif sont paramétrables :

* **filtre** [par défaut *instructeur*] - les filtres disponibles sont *aucun*, *instructeur* et *division*.
* **affichage** [par défaut *liste*] - les affichages disponibles sont *liste* ou *nombre*

.. _administration_widget_dossier_contentieux_inaffectes:

dossier_contentieux_inaffectes
==============================

Les informations fonctionnelles sont disponibles :ref:`ici<widget_dossier_contentieux_inaffectes>`.

Trois arguments facultatifs sont paramétrables :

* **filtre** [par défaut *aucun*] - les valeurs disponibles sont *aucun* et *division* ;
* **dossier_encours** [par défaut *true*] - les valeurs disponibles sont :

    * *true* affiche seulement les infractions dont l'état est considéré comme en cours d'instruction ;
    * *false* affiche toutes les infractions.

* **affichage** [par défaut *liste*] - les affichages disponibles sont *liste* ou *nombre*

.. _administration_widget_dossier_contentieux_alerte_visite:

dossier_contentieux_alerte_visite
=================================

Les informations fonctionnelles sont disponibles :ref:`ici<widget_dossier_contentieux_alerte_visite>`.

Trois arguments facultatifs sont paramétrables :

* **filtre** [par défaut *instructeur*] - les valeurs disponibles sont *aucun*, *instructeur* et *division* ;
* **dossier_encours** [par défaut *true*] - les valeurs disponibles sont :

    * *true* affiche seulement les infractions dont l'état est considéré comme en cours d'instruction ;
    * *false* affiche toutes les infractions.

* **affichage** [par défaut *liste*] - les affichages disponibles sont *liste* ou *nombre*

.. _administration_widget_dossier_contentieux_alerte_parquet:

dossier_contentieux_alerte_parquet
==================================

Les informations fonctionnelles sont disponibles :ref:`ici<widget_dossier_contentieux_alerte_parquet>`.

Trois arguments facultatifs sont paramétrables :

* **filtre** [par défaut *instructeur*] - les valeurs disponibles sont *aucun*, *instructeur* et *division* ;
* **dossier_encours** [par défaut *true*] - les valeurs disponibles sont :

    * *true* affiche seulement les infractions dont l'état est considéré comme en cours d'instruction ;
    * *false* affiche toutes les infractions.

* **affichage** [par défaut *liste*] - les affichages disponibles sont *liste* ou *nombre*

.. _administration_widget_rss:

rss
===

Ce widget permet d’afficher X informations ayant pour origine X flux RSS 2.0.
Les informations fonctionnelles sont disponibles :ref:`ici<widget_rss>`.

Trois arguments sont paramétrables :

* **urls** - il est possible de renseigner un seul ou plusieurs urls, dans le cas de multiple urls alors les séparés par une virgule
* **mode** [par défaut *server_side*] - prend comme valeur server_side (Récupération du flux à partir du serveur Applicatif) ou client_side (Récupération du flux par le coté Client).
* **max_item** - prend comme valeur un nombre entier, détermine le nombre d'information que le widget affichera.


.. _administration_widget_dossier_consulter:

dossier_consulter
=================

Ce widget permet d’afficher les X derniers dossiers consultés.
Un lien *Voir +* permet d'afficher le listing complet de dossiers visité au sein du widget qui est limité à 20 dossiers.
Les informations fonctionnelles sont disponibles :ref:`ici<widget_dossier_consulter>`.

Un argument est paramétrable :

* **nb_dossiers** - prend comme valeur un nombre entier, determine le nombre de dossier affiché dans le widget. Par défaut il sera égale à 5. Le nombre maximal de dossier consultés visibles dans le widget est de 20.

.. _administration_widget_derniers_dossiers_deposes:

derniers_dossiers_deposes
=========================

Ce widget présente une métrique des derniers dossiers déposés correspondant aux paramètres définis.
Un lien *Voir +* permet d'afficher le listing complet des dossiers déposés, selon les paramètres du widget. 
Les informations fonctionnelles sont disponibles :ref:`ici<widget_derniers_dossiers_deposes>`.

Ce widget est par défaut affiché sur le tableau de bord des profils DIVISIONNAIRE. 

Les arguments suivants sont paramétrables :

* **nombre_de_jours** [par défaut *15*] - délai en jours avant la date du jour. Intervalle dans lequel est comprise la date de dépôt des dossiers qu'on souhaite prendre en compte.

* **codes_datd** [par défaut tous les types sont affichés] - liste des types de dossiers à afficher séparés par un point-virgule. exemple : *PCI;PCA;DPS;CUa;CUb*

* **filtre_depot** [par défaut *aucun*] - indique le type de dépôt dont sont issus les dossiers pris en compte par le widget. Les filtres disponibles sont *depot_electronique*, *guichet* et *aucun*.

* **filtre** [par défaut *division*] - les filtres disponibles sont *aucun*, *division*, *instructeur* et *instructeur_secondaire*.

* **restreindre_msg_non_lus** [par défaut *false*] - paramètre l'apparition de l'indicateur de message dans la colonne *message* du listing. *false*: si au moins un message manuel est présent sur le dossier. *true*: si au moins un message manuel NON LU est présent sur le dossier.

.. _administration_widget_dossiers_pre_instruction:

dossiers_pre_instruction
========================

Ce widget présente les dossiers d'instruction qui sont à qualifier, non clôturés et dont la date limite de notification du délai au pétitionnaire arrive bientôt à échéance.
Un lien *Voir +* permet d'afficher le listing complet des dossiers d'instruction, selon les paramètres du widget.
Ces dossiers d'instruction sont triés par la date limite de notification du délai la moins proche à la plus proche du jour courant.
Les informations fonctionnelles sont disponibles :ref:`ici<widget_dossiers_pre_instruction>`.

Ce widget est par défaut affiché sur le tableau de bord des profils INTRUCTEUR POLYVALENT et INSTRUCTEUR POLYVALENT COMMUNE. 

Les arguments suivants sont paramétrables :

* **codes_datd** [par défaut tous les types sont affichés] - liste des types de dossiers à afficher séparés par un point-virgule. exemple : *PCI;PCA;DPS;CUa;CUb*

* **filtre** [par défaut *instructeur*] - les filtres disponibles sont *aucun*, *division*, *instructeur* et *instructeur_secondaire*.

* **affichage** [par défaut *liste*] - les affichages disponibles sont *liste* ou *nombre*

.. _administration_widget_controle_donnee:

widget_controle_donnee
======================

Ce widget présente les dossiers ayant des informations manquantes pour la transmission vers Plat'AU.
Un lien *Voir +* permet d'afficher le listing complet des dossiers d'instruction, selon les paramètres du widget.
Ces dossiers d'instruction sont triés par le nom du dossier d'instruction.
Les informations fonctionnelles sont disponibles :ref:`ici<widget_controle_donnee>`.

Les arguments suivants sont paramétrables :

* **affichage** [par défaut liste] - permet de choisir entre l'affichage du nombre de dossier (nombre) ou de la liste des dossiers.

* **filtre** [par défaut *instructeur*] - les filtres disponibles sont *aucun*, *division*, *instructeur* et *instructeur_secondaire*.

.. _administration_widget_recherche_parametrable:

widget_recherche_parametrable
=============================

Les informations fonctionnelles sont disponibles :ref:`ici<widget_recherche_parametrable>`.

Arguments obligatoires :

* **etat**  - permet de spécifier sur quel état de dossier il faut filtrer

Trois arguments facultatifs sont paramétrables :

* **filtre** [par défaut *instructeur*] - les valeurs disponibles sont *aucun*, *instructeur*, *instructeur_secondaire* et *division*
* **affichage** [par défaut *nombre*] - les affichages disponibles sont *liste* ou *nombre*
* **tri** [par défaut -6] - spécifie sur quelle colonne on va trier les résultat (-6 correspond à la sixième colonne en triant de manière décroissante)
* **message_help** [par défaut à vide] - permet de personnaliser le message d'aide affiché dans l'info bulle et dans le listing
* **source_depot** [par défaut à vide] - permet de spécifier le source du dépôt des dossiers recherchés

.. _administration_widget_suivi_instruction_parametrable:

widget_suivi_instruction_parametrable
=====================================

Les informations fonctionnelles sont disponibles :ref:`ici<widget_suivi_instruction_parametrable>`.

Arguments paramétrables :

* **statut_signature** [par défaut à vide] - les valeurs disponibles sont *in_progress*, *finished*, *canceled* et *expired*, on peut paramétrer plusieurs valeur séparées par des ";"
* **filtre** [par défaut *instructeur*] - les valeurs disponibles sont *aucun*, *instructeur*, *instructeur_secondaire* et *division*
* **affichage** [par défaut *liste*] - les affichages disponibles sont *liste* ou *nombre*
* **tri** [par défaut à vide] - spécifie sur quelle colonne on va trier les résultat (-6 correspond à la sixième colonne en triant de manière décroissante). Ce tri n'est appliqué que sur le listing du *Voir +*. Il est possible d'identifier le numéro de la colonne en se rendant dans le listing *Voir +* du widget et en cliquant sur la colonne voulu, vous retrouvez alors le numéro de la colonne dans l'url au paramètre *tricol*. Exemple tricol=10 correspond au tri de la colonne *instructeur* en croissant.

  Liste des colonnes disponibles et du numéro correspondant sans les options d'affichage de la date de dépôt en mairie et de la commune :

  - *dossier* : colonne numéro 3
  - *pétitionnaire* : colonne numéro 4
  - *localisation* : colonne numéro 5
  - *nature* : colonne numéro 6
  - *date de dépôt* : colonne numéro 7
  - *date de complétude* : colonne numéro 8
  - *date limite* : colonne numéro 9
  - *instructeur* : colonne numéro 10
  - *division* : colonne numéro 11
  - *état* : colonne numéro 12
  - *enjeu* : colonne numéro 13
  - *collectivite/service* : colonne numéro 14


  Liste des colonnes disponibles et du numéro correspondant avec les options d'affichage de la date de dépôt en mairie et de la commune :

  - *dossier* : colonne numéro 3
  - *commune* : colonne numéro 4
  - *pétitionnaire* : colonne numéro 5
  - *localisation* : colonne numéro 6
  - *nature* : colonne numéro 7
  - *date de dépôt* : colonne numéro 8
  - *date de complétude* : colonne numéro 9
  - *date limite* : colonne numéro 10
  - *date de dépôt en mairie* : colonne numéro 11
  - *instructeur* : colonne numéro 12
  - *division* : colonne numéro 13
  - *état* : colonne numéro 14
  - *enjeu* : colonne numéro 15
  - *collectivite/service* : colonne numéro 16

* **message_help** [par défaut à vide] - permet de personnaliser le message d'aide affiché dans l'info bulle et dans le listing
* **evenement_id** [par défaut à vide]- L'identifiant d'évènement, on peut paramétrer plusieurs valeur séparées par des ";"
* **exclure_evenement_id** [par défaut à vide]- L'identifiant d'évènement à exclure, on peut paramétrer plusieurs valeur séparées par des ";"
* **evenement_type** [par défaut à vide]- Le type d’instruction, on peut paramétrer plusieurs valeur séparées par des ";"
* **instruction_finalisee** [par défaut à vide]- Les instructions finalisées : true / false / (pas de filtre si non remplit)
* **instruction_notifiee** [par défaut à vide]- Les instructions notifiées : oui / non / (pas de filtre si non remplit)
* **signataire_description** [par défaut à vide]- Le signataire : filtre selon le champ 'description' du signataire.
* **etat** [par défaut à vide]- L'état du dossier : filtre selon l’état du dossier, on peut paramétrer plusieurs valeur séparées par des ";"
* **statut_dossier** [par défaut à vide]- Le statut du dossier : "encours" ou "cloture"
* **nb_jours_avant_date_limite** [par défaut à vide]- Le nombre de jours avant la date limite d'instruction : ce filtre ne prend pas en compte les dates limites d’incomplétudes.
* **nb_mois_avant_date_limite** [par défaut à vide]- Le nombre de mois avant la date limite d'instruction : ce filtre ne prend pas en compte les dates limites d’incomplétudes.
* **nb_jours_max_apres_date_evenement** [par défaut à vide]- Le nombre de jour maximum après la date d'évènement d'instruction
* **nb_mois_max_apres_date_evenement** [par défaut à vide]- Le nombre de mois maximum après la date d'évènement d'instruction
* **combinaison_criteres** [par défaut à vide]- syntaxe : *critère1*\|critère2 - Par défaut les critères sont séparé par des "et", cet argument permet de spécifier des critères où l'on veut des "ou" à la place.
* **type_cl** [par défaut à vide]- Le type de contrôle de légalité : Plat'AU ou Papier
* **envoi_cl** [par défaut à vide]- Si l'instruction est envoyée au contrôle de légalité : oui ou non
* **nb_max_resultat** [par défaut à 5] - Permet de modifier le nombre de résultat affiché dans le listing du widget
* **codes_datd** [par défaut tous les types sont affichés] - liste des types de dossiers à afficher séparés par un point-virgule. exemple : *PCI;PCA;DPS;CUa;CUb* 
* **tri_tab_widget** [par défaut date_envoi_signature, date_limite ou date_evenement] - définit une colonne sur laquelle porte le tri (voir celles de 'affichage_colonne' ci-dessous, sauf 'alerte_5_jours'). exemple : *instruction* (*-instruction* correspond à colonne 'instruction' en triant de manière décroissante)

* **affichage_colonne** - La première colonne est obligatoirement le numéro de dossier. Les valeurs possibles sont : 

  - *statut_signature* : Permet d'afficher le statut de signature du document d'instruction
  - *date_limite* : Permet d'afficher la date limite dans le listing
  - *date_envoi_signature* : Permet d'afficher la date d'envoi en signature
  - *date_retour_signature* : Permet d'afficher la date de retour signature
  - *signataire* : Permet d'afficher le nom, prenom et description du signataire
  - *petitionnaire* : Permet d'afficher le pétitionnaire
  - *instruction* : Permet d'afficher le libellé d'évènement d'instruction
  - *alerte_5_jours* : Permet d'afficher la colonne alerte 5 jours qui consiste en l'affichage d'un triangle jaune lorsqu'il ne reste plus que 5 jours avant la date limite d'instruction

Il n'y a que la dernière instruction des dossiers d'instruction qui est contrôlée par ce widget.

.. _administration_widget_suivi_tache:

widget_suivi_tache
==================

Les informations fonctionnelles sont disponibles :ref:`ici<widget_suivi_tache>`.

Arguments paramétrables :

* **type_tache** [par défaut à vide] - les valeurs disponibles sont :
        
    * pour les services instructeurs :
      
      * *creation_DA* : Création du projet
      * *creation_DI* (transfert sortant) : Création du dossier d'instruction
      * *create_DI* (transfert entrant) : Création du dossier d'instruction
      * *depot_DI* : Dépôt du dossier d'instruction
      * *modification_DI* : Modification du dossier d'instruction
      * *qualification_DI* : Permet le changement de compétence en état ou commune pour état
      * *decision_DI* : Décision sur le dossier d'instruction
      * *incompletude_DI* : Incomplétude sur le dossier
      * *completude_DI* : Complétude sur le dossier
      * *ajout_piece* (transfert sortant) : Ajout d'une pièce au dossier
      * *add_piece* (transfert entrant) : Ajout d'une pièce au dossier
      * *creation_consultation* : Création d'une consultation sur le dossier
      * *modification_DA* : Modification du projet
      * *envoi_CL* : Envoi contrôle de légalité

    * pour les services consultés :

      * *create_DI_for_consultation* : Ajour d'un dossier à partir d'une consultation entrante
      * *avis_consultation* : Avis sur une consultation
      * *pec_metier_consultation* : Prise en compte métier sur une consultation
      * *create_message* : Ajout d'un message au dossier d'instruction 
      * *prescription* : Création d'une préscription

    * pour les deux services :
      * *notification_recepisse* : Notification du récépissé
      * *notification_instruction* : Notification de l'instruction
      * *notification_decision* : Notification de la décision
      * *notification_service_consulte* : Notification du service consulté
      * *notification_tiers_consulte* : Notification du tiers consulté

* **etat_tache** [par défaut à vide] - les valeurs disponibles sont : *new*, *done*, *pending*, *draft*, *canceled* et *error* 
* **categorie_tache** [par défaut à vide] - Si vide affiche les transferts quelque soit leur catégorie, les valeurs disponibles sont *portal* et *platau*
* **flux_tache** [par défaut à vide] - les valeurs disponibles sont *input* (transfert entrant) et *output* (transfert sortant)
* **filtre** [par défaut *instructeur*] - les valeurs disponibles sont *aucun*, *instructeur*, *instructeur_secondaire* et *division*
* **affichage** [par défaut *liste*] - les affichages disponibles sont *liste* ou *nombre*
* **message_help** [par défaut à vide] - permet de personnaliser le message d'aide affiché dans l'info bulle et dans le listing
* **nb_max_resultat** [par défaut à *10*] - spécifie le nombre de résultat à afficher dans le widget
* **ordre_tri** [par défaut à *croissant*] - les résultats sont triés par date de dépôt de dossier, on peut choisir de trier soit en *croissant* soit en *décroissant*

.. _administration_widget_compteur_signatures:

widget_compteur_signatures
==========================

Les informations fonctionnelles sont disponibles :ref:`ici<widget_compteur_signatures>`.

Arguments paramétrables :

* **om_collectivite** ou **service** [par défaut à vide]: permet de "forcer" l'affichage
  du compteur de signatures d'une collectivité/d'un service qui ne serait pas celle/celui
  de l'utilisateur actuel. Pour ce faire, renseigner l'identifiant technique de la
  collectivité/du service.

* **url_renouvellement** [par défaut à vide]: spécifie l'URL à afficher à l'utilisateur
  lorsque le quota de signatures est presque atteint voir dépassé.


.. _administration_composition:

===========
Composition
===========

(:menuselection:`Administration --> Tableaux De Bord --> Composition`)

Menu de composition du tableau de bord des utilisateurs.

Options Avancées
################


.. _administration_sousetat:

==============
Les sous-états
==============

(:menuselection:`Administration --> Options Avancées --> Sous États`)

Les sous-états des requêtes SQL.

.. _administration_omrequete:

===============
Les requêtes om
===============

(:menuselection:`Administration --> Options Avancées --> Om Requête`)

Les requêtes SQL des éditions.

.. _administration_moniteur:

=============
Les moniteurs
=============

(:menuselection:`Administration --> Options Avancées --> Moniteur Plat'AU / IDE'AU`)

Moniteurs permettant de surveiller l'état des tâches (Plat'AU ou IDE'AU) entrantes et sortantes.

La recherche avancée est accessible dans ces menus et permet de chercher une tâche avec les critères
suivant :

  * **Tâche** : identifiant de la tâche.
  * **Json payload** : recherche une chaîne de caractère donnée dans la payload des tâches.
    Pour fonctionner le texte saisi doit être entouré par '*' (ex : \*Ma recherche\*)
  * **Type** : filtre les tâches selon le type de tâche sélectionné (ex : Création DI).
    Seul les types de tâches associés à des tâches enregistrées dans l'application peuvent
    être choisis.
  * **État** : filtre selon l'état de la tâche (ex : à traiter).
  * **réf. interne** : identifiant de l'élément concerné par la tâche, par exemple un numéro de dossier.
  * **Dossier** : numéro de dossier associé à la tâche.
  * **Flux** : permet de n'afficher que les tâches entrantes ou sortantes.
  * **Date de création** : n'affiche que les tâches dont la date de création est comprise dans l'intervalle selectionné.
  * **Date de dernière modification** : n'affiche que les tâches dont la date de dernière modification est comprise dans l'intervalle selectionné.
  * **Commentaire** : permet de chercher une tâche à partir d'un commentaire.

En sélectionnant une tâche dans ce listing, il est possible de la consulter.

En consultation, l'historique des modifications d'une tâche ainsi que sa payload
son accessible. Si un numéro de dossier est associé à cette tâche et que le dossier
existe dans l'application, cliquer sur le numéro de dossier permet d'y accéder.

.. note::
  Pour les tâches sortantes la payload affichée est "calculée". Cela signifie qu'on va
  récupérer les informations de l'application lors de la consultation de la payload
  pour afficher son contenu et qu'il n'est pas stocké dans la base de données.
  Ainsi en modifiant les données de l'application, par exemple en changeant les données
  technique d'un dossier, la payload sera impactée.

  Lorsque la tâche est traitée (passage à l'état terminé), la payload est enregistrée
  dans la base de données. Cela permet de garder le contenu de la payload tel qu'elle
  a été traitée.

  Après avoir été traitée, si son état est modifié, la payload sera à nouveau "calculée"
  et son contenu sera supprimé de la base de donnée.


.. _administration_import:

===========
Les imports
===========

(:menuselection:`Administration --> Options Avancées --> Import`)

Import des données au format CSV.

Tiers consulté
==============

Pour que les tiers consulté fonctionne correctement, il est important de suivre l'ordre suivant lors des imports : 

- Importer **tiers consulté**
- Importer **habilitation de tiers consulté**
- Importer **commune(s) liées aux habilitations de tiers consulté**
- Importer **département(s) liés aux habilitations de tiers consulté**
- Importer **spécialité(s) des habilitations de tiers consulté**


Import spécifique
=================

(:menuselection:`Administration --> Options Avancées --> Import spécifique`)

Ce menu permet d'accéder au module d'import des données au format ADS 2007 et des bibles.

Fonctionnement
--------------

Choisissez le type d'import spécifique désiré 'ADS 2007' ou 'Bible'.

Puis, depuis le formulaire :

- importer le fichier csv
- choisir le séparateur (, ou ;)
- valider le formulaire d'import

.. NOTE:: L'encodage du fichier csv à importer doit être ISO-8859-15.
          
          Seuls les séparateurs , ou ; sont admis.
          
          Les références cadastrales doivent être séparées par une virgule.
  
Une fois le chargement terminé un récapitulatif des traitements effectués est affiché, dans celui-ci un fichier de rejet est disponible.

Ce fichier de rejet contient toutes les lignes du csv importées qui sont en erreur. Les erreurs sont ajoutées en fin de ligne dans une nouvelle colonne.

Après correction ce ficher de rejet peut être ré-importé.


ADS 2007
--------

Import CSV spécifique au format ADS 2007.

.. NOTE:: Si dans un dossier une date de decision est définie mais qu'il n'a pas de nature de decision alors le dossier est implicitement accordé.
.. NOTE:: Le suffixe "P0" est ajouté à la fin de chaque numéro de dossier initial seulement si le suffixe est activé pour le type de dossier d'instruction importé.

Exemple d'erreurs typiques :

- Le code INSEE n'est pas paramétré : un code INSEE doit être défini pour chaque commune dans les paramètres.
- Dossiers non clôturés (pas de date d'accord/rejet/refus et de date de décision).
- Mauvais format des références cadastrales.
- Dossier avec date de décision mais pas de nature de décision.

Des dossiers importés peuvent être mis à jour hors d'openADS, lors du prochain import les données du dossiers et des données techniques (CERFA) seront mises à jour. Attention, les demandeurs ne sont pas mis à jour.

Description des colonnes du CSV :

+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Colonne | Nom de la colonne                   | Type    | Obligatoire | Description                                                                                                                                                                                    | Choix possibles                                                                                                                                                                 |
+=========+=====================================+=========+=============+================================================================================================================================================================================================+=================================================================================================================================================================================+
| 1       | Type                                | texte   | Oui         | Code des types de dossiers d'autorisations                                                                                                                                                     | AZ, AT, AC, ST, CH, CX, CS, CA, DF, DT, MH, DP, CO, FA, IN, LT, NR, TP, PA, PC, PI, PD, RE, RD, SC, CI, CUb, CUa, DPS                                                           |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 2       | Numéro                              | texte   | Oui         | Identifiant du dossier                                                                                                                                                                         |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 3       | Initial                             | texte   | Non         | Identifiant du dossier initial                                                                                                                                                                 |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 4       | INSEE                               | entier  | Oui         | Code INSEE de la commune sur 5 caractères                                                                                                                                                      | Le code INSEE doit être paramétré pour chaque commune (Administration → Paramètres)                                                                                             |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 5       | Commune                             | texte   | Non         | Nom de la commune                                                                                                                                                                              | Les commune doivent être créées (Administration → Collectivité)                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 6       | Autonome                            | Oui/Non | Non         |                                                                                                                                                                                                |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 7       | Projet                              | texte   | Non         | Description du projet d'urbanisme /!\ Attention : quelle que soit la nature de la DP/DPS (construction, démolition ou aménagement), ce texte se mettra dans la description de la construction. |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 8       | Destination                         | texte   | Non         | Affectation de la construction (choix multiples)                                                                                                                                               | Habitation, Hébergement hôtelier, Bureaux, Commerce, Artisanat, Industrie, Exploit. agricole ou forestière, Entrepôt, Service public ou d'intérêt général                       |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 9       | Nb logements                        | integer | Non         | Nombre de logements                                                                                                                                                                            |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 10      | Surface terrain                     | décimal | Non         | Surface du terrain                                                                                                                                                                             |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 11      | SHON existante                      | décimal | Non         | SHON existante                                                                                                                                                                                 |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 12      | SHON construite                     | décimal | Non         | SHON construite                                                                                                                                                                                |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 13      | SHON transformation SHOB            | décimal | Non         | SHON transformation SHOB                                                                                                                                                                       |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 14      | SHON changement destination         | décimal | Non         | SHON changement destination                                                                                                                                                                    |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 15      | SHON démolie                        | décimal | Non         | SHON démolie                                                                                                                                                                                   |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 16      | SHON supprimée                      | décimal | Non         | SHON supprimée                                                                                                                                                                                 |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 17      | Architecte                          | Oui/Non | Non         | Soumis à architecte O/N                                                                                                                                                                        |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 18      | Demandeur                           | texte   | Oui         | Nom du demandeur                                                                                                                                                                               |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 19      | Opposition CNIL                     | Oui/Non | Non         |                                                                                                                                                                                                |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 20      | Adresse demandeur                   | texte   | Non         | Adresse principale du demandeur                                                                                                                                                                | L'adresse du demandeur doit être de la forme : [adresse (90 caractères max)] [code postal (5 chiffre)] [commune (30 caractères max)]                                            |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 21      | Localisation                        | texte   | Non         | Adresse de la construction                                                                                                                                                                     | L'adresse doit être de la forme : [adresse (90 caractères max)] [code postal (5 chiffre)] [commune (30 caractères max)]                                                         |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 22      | Références cadastrales              | texte   | Non         | Références cadastrales (séparées par ",")                                                                                                                                                      | Format des références : 0 à 4 chiffres, 1 à 2 lettres (obligatoires), 1 à 4 chiffres (obligatoire). Chaque partie est séparée par un tiret. Exemple : 123-AA-0123, AB-0123, ... |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 23      | Lotissement                         | Oui/Non | Non         | Rattachement à un lotissement                                                                                                                                                                  |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 24      | AFU                                 | Oui/Non | Non         | Statut d'Association foncière urbaine                                                                                                                                                          |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 25      | Détail ZAC AFU                      | texte   | Oui         | Description opération d'aménagement de type AFU                                                                                                                                                |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 26      | Autorité                            | texte   | Oui         | Code de l'autorité référente au dossier                                                                                                                                                        | COM, ETATMAIRE, ETAT                                                                                                                                                            |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 27      | Etat                                | texte   | Non         | État du dossier                                                                                                                                                                                | retire, annule, accepte_tacite, accepter, rejeter, Sursis_a_statuer, terminer, refuse_tacite, refuse                                                                            |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 28      | Centre instructeur                  | texte   | Non         | Centre instructeur                                                                                                                                                                             |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 29      | Instructeur                         | texte   | Non         | Nom de l'intructeur                                                                                                                                                                            |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 30      | Liquidateur                         | texte   | Non         | Nom du liquidateur                                                                                                                                                                             |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 31      | Complexité                          | texte   | Oui         | Niveau d'enjeu du dossier (Forte/Moyenne/Faible)                                                                                                                                               |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 32      | Dépôt en mairie                     | date    | Oui         | Date de dépôt en mairie                                                                                                                                                                        |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 33      | Réception DDE                       | date    | Non         | Date de réception par le service instructeur de la DDE                                                                                                                                         |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 34      | Complétude                          | date    | Non         | Date de réception des pièces complémentaires demandées                                                                                                                                         |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 35      | Notification majoration             | date    | Oui         | Date de notification de la majoration si dossier complet                                                                                                                                       |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 36      | DLI                                 | date    | Non         | Date limite d'instruction                                                                                                                                                                      |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 37      | Date envoi demande de pièces        | date    | Non         | Date envoi demande de pièces                                                                                                                                                                   |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 38      | Date notification demande de pièces | date    | Non         | Date notification demande de pièces                                                                                                                                                            |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 39      | Date envoi délai majoration         | date    | Non         | Date envoi délai majoration                                                                                                                                                                    |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 40      | Date notification délai majoration  | date    | Non         | Date notification délai majoration                                                                                                                                                             |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 41      | Service consulté                    | texte   | Non         | Services extérieurs consultés                                                                                                                                                                  |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 42      | Proposition service                 | texte   | Non         | Proposition du service consulté                                                                                                                                                                |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 43      | Date proposition service            | date    | Non         | Date de proposition du service consulté                                                                                                                                                        |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 44      | Date transmission proposition       | date    | Non         | Date de transmission de l'arrêté du service instructeur                                                                                                                                        |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 45      | Date instruction terminée           | date    | Non         | Date instruction terminée                                                                                                                                                                      |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 46      | Date accord tacite                  | date    | Non         | Date accord tacite                                                                                                                                                                             |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 47      | Date de rejet tacite                | date    | Non         | Date de rejet tacite                                                                                                                                                                           |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 48      | Date de refus tacite                | date    | Non         | Date de refus tacite                                                                                                                                                                           |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 49      | Date de décision                    | date    | Oui         | Date de décision                                                                                                                                                                               |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 50      | Date notification décision          | date    | Non         | Date notification décision                                                                                                                                                                     |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 51      | Nature décision                     | texte   | Oui         | Nature de la décision                                                                                                                                                                          | Defavorable, Favorable, Annulation, Refus tacite, Sursis a statuer, Accord Tacite, Favorable avec Reserves, Rejet tacite, Annulation par tribunal                               |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 52      | Récolement                          | texte   | Non         |                                                                                                                                                                                                |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 53      | DOC                                 | date    | Non         | Date de déclaration d'ouverture de chantier                                                                                                                                                    |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 54      | DAACT                               | date    | Non         | Date d'achèvement et de conformité des travaux                                                                                                                                                 |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 55      | Type Evolution                      | texte   | Non         | Type d'évolution de l'autorisation (Prorogation, Retrait à l'initative du pétitionnaire, Transfert)                                                                                            |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 56      | Statut Evolution                    | texte   | Non         | État de l'évolution en cours (Évolution en cours, Décision notifiée au demandeur)                                                                                                              |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 57      | Type dernières taxes                | texte   | Non         |                                                                                                                                                                                                |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 58      | Statut dernières taxes              | texte   | Non         | Non taxable, Taxe initiale, Dégrèvement, Exonération, Procès verbal                                                                                                                            |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 59      | Type dernière RAP                   | texte   | Non         |                                                                                                                                                                                                |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 60      | Statut dernière RAP                 | texte   | Non         | Non taxable, Taxe initiale, Dégrèvement, Exonération, Procès verbal                                                                                                                            |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 61      | EPCI                                | texte   | Non         |                                                                                                                                                                                                |                                                                                                                                                                                 |
+---------+-------------------------------------+---------+-------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

.. _administration_specific_import_bible:

Bibles
------

Import CSV spécifique des bibles.

Avant chaque import, toutes les bibles existantes seront supprimées.

Un export sous ce même format sera disponibles depuis la liste des requêtes mémorisées dans le menu :ref:`"Export / Import"<reqmo_export_param_bible>`.


Description des colonnes du CSV :

+---------+-------------------+----------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------+
| Colonne | Nom de la colonne | Type           | Obligatoire | Description                                                                                                                                 |
+=========+===================+================+=============+=============================================================================================================================================+
| 1       | libelle           | texte (61 max) | Oui         | Libelle de la bible                                                                                                                         |
+---------+-------------------+----------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------+
| 2       | contenu           | texte          | Oui         | Contenu HTML de la bible                                                                                                                    |
+---------+-------------------+----------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------+
| 3       | codes             | texte          | Oui         | Paramètrage de la bible. Le format est : {{evenement, complement, automatique, precharge, dossier_autorisation_type, om_collectivite}, ...}.|
+---------+-------------------+----------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------+


.. _administration_import_specific_commune:

Communes, départements, région
------------------------------

Lorsque l':ref:`option dossier_commune<administration_liste_options>` est activée, il est nécessaire d'alimenter la base de données des communes. Cela se fait généralement via l':ref:`import spécifique<administration_import_specific_commune>`.

Les départements et régions, ne sont utilisés (actuellement) que pour fournir des libellés dans les éditions, et donc sont moins critiques, mais leur import reste accessible via ces menus.



.. _administration_generateur:

=============
Le générateur
=============

(:menuselection:`Administration --> Options Avancées --> Générateur`)

Le générateur de fichiers de l'application.

.. _administration_synchronisation_contrainte:

===============================
Synchronisation des contraintes
===============================

(:menuselection:`Paramétrage Dossiers --> Dossiers --> Synchronisation Des Contraintes`)

Le principe
===========

Ce menu permet de synchroniser les contraintes du SIG avec celles de l'application.
openADS va récupérer l'ensemble des contraintes du SIG, et les comparer avec les
contraintes déjà présentes dans la base de données de l'application, sur la base de leur
identifiant unique de contrainte. Les contraintes présentes sur le SIG ne seront pas
modifiées par openADS.
Les informations suivantes de la contrainte sont récupérées du SIG : 

* identifiant
* groupe
* sous-groupe
* libelle
* texte

.. image:: m_contrainte_synchronisation.png

* **X contrainte(s) ajoutée(s)** : Nombre de contraintes importées dans openADS à partir du SIG.
* **X contrainte(s) modifiée(s)** : Nombre de contraintes déjà présentes dans openADS, mises à jour avec les dernières informations du SIG.
* **X contrainte(s) archivée(s)** : Nombre de contraintes n'existant plus dans le SIG, archivées dans openADS.

Les contraintes référencées comme venant du SIG
===============================================

Lorsque des contraintes sont importées dans openADS via la synchronisation des
contraintes, elles sont marquées comme ayant été importées à partir du SIG (champ **Référence SIG** à *Oui*).

Quand on effectue une nouvelle synchronisation des contraintes, 3 cas de figure se
présentent :

* La contrainte existe sur le SIG mais pas dans openADS : elle est ajoutée.
* La contrainte existe sur le SIG ET dans openADS : les champs **libellé**, **groupe** et **sous-groupe** seront écrasés avec les valeurs du SIG, le **texte** est écrasé seulement si une valeur est renseignée dans le SIG.
* La contrainte n'existe plus sur le SIG, mais est toujours présente dans openADS : elle est archivée en mettant la date du jour de la synchronisation dans le champ **date de fin de validité**.

Les contraintes n'étant pas référencées comme venant du SIG
===========================================================

Les contraintes créées manuellement dans l'application ne sont pas référencées
comme provenant du SIG.

Quand une synchronisation des contraintes est lancée, ces contraintes sont ignorées et
restent dans le même état, même si elles ont le même groupe, sous-groupe, libellé ou texte 
qu'une contrainte importée du SIG. Des contraintes peuvent donc être en doublon.

.. _administration_geolocalisation_auto:

======================================================
Géolocalisation automatique des dossiers d'instruction
======================================================
(:menuselection:`Administration --> Options Avancées --> Géolocalisation des dossiers`)


Le principe
===========

Ce menu permet de lancer la géolocalisation automatique sur chacun des dossiers d'instruction vérifiant toutes les caractéristiques suivantes:

* il n'y a pas de parcelle temporaire sur le dossier d'instruction.
* aucune valeur n'est affectée au centroïde.

Il est également possible d'affiner le filtre des dossiers sur lesquels appliquer la géolocation automatique, en renseignant le paramètre :ref:`param_geolocalisation_auto<parametrage_parametre_geolocalisation_auto>`.

Dans ce cas là, la géolocalisation effectue un calcul de l'emprise puis un calcul du centroïde.

Il est aussi possible de récupérer les contraintes lors de la géolocalisation automatique. Pour cela, l'option :ref:`option_geolocalisation_auto_contrainte<administration_liste_options>` doit être active.

Ce procédé peut-être déclenché manuellement ou via un :ref:`web-service<web_services_ressource_maintenance_geolocalisation_auto>`.

L'utilisation
=============

Le formulaire affiche le nombre de dossier d'instruction à traiter.

Il suffit de cliquer sur le bouton pour lancer le traitement.

.. image:: a_administration_geolocalisation_auto.png


Les messages de retour
======================

A la fin du traitement, un message est affiché à l'utilisateur.

Il contient les éléments suivants, classés par collectivité:

* le nombre de dossiers traités avec succès ;
* le nombre de dossiers en erreur suite à :

  * une parcelle non-existante ;
  * le calcul de l'emprise impossible ;
  * le calcul du centroïde impossible.
